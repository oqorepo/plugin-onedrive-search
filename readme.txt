=== Share-One-Drive ===
Requires at least: 3.9
Tested up to: 4.9.8
Requires PHP: 5.4

Share-one-Drive is the #1 Ultimate OneDrive plugin for WordPress plugin on the market and part of a series of Cloud Plugins already powering 5.000+ company websites improving their workflow. Join now and start using your OneDrive even more efficiently by integrating it on your website!

== Description ==

This plugin will help you to easily integrate OneDrive (formerly SkyDrive) into your WordPress website or blog. Share-one-Drive allows you to view, download, delete, rename files & folders directly from a WordPress page. You can use Share-one-Drive as a File browser, Gallery, Audio- or Video-Player!

== Changelog ==
You can find the Release Notes in the [Documentation](https://www.wpcloudplugins.com/wp-content/plugins/share-one-drive/_documentation/index.html#releasenotes).
