<?php

namespace TheLion\ShareoneDrive;

class Gallery {

    /**
     *
     * @var \TheLion\ShareoneDrive\Processor 
     */
    private $_processor;
    private $_search = false;
    private $_parentfolders = array();

    public function __construct(Processor $_processor) {
        $this->_processor = $_processor;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor() {
        return $this->_processor;
    }

    public function getImagesList() {

        $this->_folder = $this->get_processor()->get_client()->get_folder();

        if (($this->_folder !== false)) {
            $this->imagesarray = $this->createImageArray();
            $this->renderImagesList();
        }
    }

    public function searchImageFiles() {
        $this->_search = true;
        $input = $_REQUEST['query'];
        $this->_folder = $this->get_processor()->get_client()->search_by_name($input);

        if ((!empty($this->_folder['contents']))) {
            $this->imagesarray = $this->createImageArray();

            $this->renderImagesList();
        }
    }

    public function setFolder($folder) {
        $this->_folder = $folder;
    }

    public function setParentFolder() {
        if ($this->_search === true) {
            return;
        }

        $currentfolder = $this->_folder['folder']->get_entry()->get_id();
        if ($currentfolder !== $this->get_processor()->get_root_folder()) {

            /* Get parent folder from known folder path */
            $cacheparentfolder = $this->get_processor()->get_client()->get_entry($this->get_processor()->get_root_folder());
            $folder_path = $this->get_processor()->get_folder_path();
            $parentid = end($folder_path);
            if ($parentid !== false) {
                $cacheparentfolder = $this->get_processor()->get_client()->get_entry($parentid);
            }

            /* Check if parent folder indeed is direct parent of entry
             * If not, return all known parents */
            $parentfolders = array();
            if ($cacheparentfolder !== false && $cacheparentfolder->has_children() && array_key_exists($currentfolder, $cacheparentfolder->get_children())) {
                $parentfolders[] = $cacheparentfolder->get_entry();
            } else {
                if ($this->_folder['folder']->has_parents()) {
                    foreach ($this->_folder['folder']->get_parents() as $parent) {
                        $parentfolders[] = $parent->get_entry();
                    }
                }
            }
            $this->_parentfolders = $parentfolders;
        }
    }

    public function renderImagesList() {

        // Create HTML Filelist
        $imageslist_html = "";

        if (count($this->imagesarray) > 0) {

            /* Limit the number of files if needed */
            if ($this->get_processor()->get_shortcode_option('max_files') !== '-1') {
                $this->imagesarray = array_slice($this->imagesarray, 0, $this->get_processor()->get_shortcode_option('max_files'));
            }

            $imageslist_html = "<div class='images image-collage'>";
            foreach ($this->imagesarray as $item) {
                // Render folder div
                if ($item->is_dir()) {
                    $imageslist_html .= $this->renderDir($item);
                }
            }
        }

        $imageslist_html .= $this->renderNewFolder();

        if (count($this->imagesarray) > 0) {
            $i = 0;
            foreach ($this->imagesarray as $item) {

                // Render file div
                if (!$item->is_dir()) {
                    $hidden = (($this->get_processor()->get_shortcode_option('maximages') !== '0') && ($i >= $this->get_processor()->get_shortcode_option('maximages')));
                    $imageslist_html .= $this->renderFile($item, $hidden);
                    $i++;
                }
            }

            $imageslist_html .= "</div>";
        } else {
            if ($this->_search === true) {
                $imageslist_html .= '<div class="no_results">' . __('No files or folders found', 'shareonedrive') . '</div>';
            }
        }

        //Create HTML Filelist title
        $filepath = '';

        $userfolder = $this->get_processor()->get_user_folders()->get_auto_linked_folder_for_user();

        if ($this->_search === true) {
            $filepath = __('Results', 'shareonedrive');
            //} elseif ($this->_userFolder !== false) {
            //    $filepath = "<a href='javascript:void(0)' class='folder' data-id='" . $this->get_processor()->get_root_folder() . "'>" . $userfolder->get_name() . "</a>";
        } else {
            if ($this->get_processor()->get_root_folder() === $this->_folder['folder']->get_entry()->get_id()) {
                $filepath = "<a href='javascript:void(0)' class='folder' data-id='" . $this->_folder['folder']->get_entry()->get_id() . "'><strong>" . $this->get_processor()->get_shortcode_option('root_text') . "</strong></a>";
            } else {

                $parentId = $this->get_processor()->get_root_folder();
                $lastparent = end($this->_parentfolders);
                if ($lastparent !== false) {
                    $parentId = $lastparent->get_id();

                    if ($parentId === $this->get_processor()->get_root_folder() && $userfolder === false) {
                        $title = $this->get_processor()->get_shortcode_option('root_text');
                    } else {
                        $title = $lastparent->get_name();
                        $title = apply_filters('shareonedrive_gallery_entry_text', $title, $lastparent, $this);
                    }

                    $filepath = " <a href='javascript:void(0)' class='folder' data-id='" . $parentId . "'>" . $title . "</a> &laquo; ";
                } else {
                    $filepath = " <a href='javascript:void(0)' class='folder' data-id='" . $parentId . "'>" . __('Back', 'shareonedrive') . "</a> &laquo; ";
                }

                $title = $this->_folder['folder']->get_entry()->get_name();
                $title = apply_filters('shareonedrive_gallery_entry_text', $title, $this->_folder['folder']->get_entry(), $this);
                $filepath .= "<a href='javascript:void(0)' class='folder' data-id='" . $this->_folder['folder']->get_entry()->get_id() . "'><strong>" . $title . "</strong>";
            }
        }

        /* lastFolder contains current folder path of the user */
        $folder_path = $this->get_processor()->get_folder_path();
        if ($this->_search !== true && (end($folder_path) !== $this->_folder['folder']->get_entry()->get_id())) {
            $folder_path[] = $this->_folder['folder']->get_entry()->get_id();
        }

        if ($this->_search === true) {
            $lastFolder = $this->get_processor()->get_last_folder();
            $expires = 0;
        } else {
            $lastFolder = $this->_folder['folder']->get_entry()->get_id();
            $expires = $this->_folder['folder']->get_expired();
        }

        $response = json_encode(array(
            'folderPath' => base64_encode(serialize($folder_path)),
            'lastFolder' => $lastFolder,
            'breadcrumb' => $filepath,
            'html' => $imageslist_html,
            'hasChanges' => defined('HAS_CHANGES'),
            'expires' => $expires));

        if (defined('HAS_CHANGES') === false) {
            $cached_request = new CacheRequest($this->get_processor());
            $cached_request->add_cached_response($response);
        }

        echo $response;
        die();
    }

    public function renderDir($item) {
        $return = "";

        $target_height = round($this->get_processor()->get_shortcode_option('targetheight'));

        $classmoveable = ($this->get_processor()->get_user()->can_move_folders()) ? 'moveable' : '';
        $isparent = (isset($this->_folder['folder'])) ? $this->_folder['folder']->is_in_folder($item->get_id()) : false;

        if ($isparent) {
            $return .= "<div class='image-container image-folder pf' data-id='" . $item->get_id() . "' data-name='" . $item->get_basename() . "'>";
        } else {
            $return .= "<div class='image-container image-folder entry $classmoveable' data-id='" . $item->get_id() . "' data-name='" . $item->get_basename() . "'>";

            $return .= "<div class='entry_edit'>";
            $return .= $this->renderEditItem($item);
            $return .= $this->renderDescription($item);


            if ($this->get_processor()->get_user()->can_download_zip() || $this->get_processor()->get_user()->can_delete_folders()) {
                $return .= "<div class='entry_checkbox'><input type='checkbox' name='selected-files[]' class='selected-files' value='" . $item->get_id() . "'/></div>";
            }
            $return .= "</div>";
        }
        $return .= "<a title='" . $item->get_name() . "'>";

        $return .= "<div class='preloading'></div>";

        $return .= "<img class='preloading image-folder-img' src='" . SHAREONEDRIVE_ROOTPATH . "/css/images/transparant.png' data-src='" . plugins_url('css/images/folder.png', dirname(__FILE__)) . "' width='$target_height' height='$target_height' style='width:{$target_height}px !important;height:{$target_height}px !important; '/>";

        $maximages = 3;
        $iimages = 0;

        foreach ($item->get_folder_thumbnails() as $folder_thumbnails_set) {
            /* Only use $maximages for the folder div */
            if ($iimages >= $maximages) {
                break;
            }
            $thumbnail_url = ($folder_thumbnails_set->getC48x48() !== false) ? $folder_thumbnails_set->getC48x48()->getUrl() : $folder_thumbnails_set->getMedium()->getUrl();
            $thumbnail = $item->get_thumbnail_with_size($target_height, $target_height, 'center', $thumbnail_url);

            $return .= "<div class='folder-thumb thumb$iimages' style='width:" . $target_height . "px;height:" . $target_height . "px;background-image: url(" . $thumbnail . ")'></div>";

            $iimages++;
        }


        if (count($item->folder_thumbnails) === 0) {
            $return .= "<div class='folder-thumb thumb0' style='background-image: url(" . $item->get_icon_large() . ")'></div>";
        }


        $text = apply_filters('shareonedrive_gallery_entry_text', $item->get_name(), $item, $this);
        $return .= "<div class='folder-text'>" . ($isparent ? '<strong>' . __('Previous folder', 'shareonedrive') . ' (' . $text . ')</strong>' : $text) . "</div></a>";

        $return .= "</div>\n";

        return $return;
    }

    public function renderFile($item, $hidden = false) {

        $class = ($hidden) ? 'hidden' : '';

        $target_height = $this->get_processor()->get_shortcode_option('targetheight'); //Max thumbnail size

        $classmoveable = ($this->get_processor()->get_user()->can_move_files()) ? 'moveable' : '';

        $return = "<div class='image-container $class entry $classmoveable' data-id='" . $item->get_id() . "' data-name='" . $item->get_name() . "'>";

        $return .= "<div class='entry_edit'>";
        $return .= $this->renderEditItem($item);
        $return .= $this->renderDescription($item);

        if ($this->get_processor()->get_user()->can_download_zip() || $this->get_processor()->get_user()->can_delete_files()) {
            $return .= "<div class='entry_checkbox'><input type='checkbox' name='selected-files[]' class='selected-files' value='" . $item->get_id() . "'/></div>";
        }
        $return .= "</div>";

        $thumbnail = 'data-options="thumbnail: \'' . $item->get_thumbnail_medium() . '\'"';

        $link = SHAREONEDRIVE_ADMIN_URL . "?action=shareonedrive-download&id=" . urlencode($item->get_id()) . "&dl=1&listtoken=" . $this->get_processor()->get_listtoken();
        if ($this->get_processor()->get_setting('loadimages') === 'onedrivethumbnail' || $this->get_processor()->get_user()->can_download() === false) {
            $link = $item->get_thumbnail_original();
        }

        $caption_description = htmlspecialchars(((!empty($item->description)) ? $item->get_description() : $item->get_name()), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES);
        $download_url = SHAREONEDRIVE_ADMIN_URL . "?action=shareonedrive-download&id=" . $item->get_id() . "&dl=1&listtoken=" . $this->get_processor()->get_listtoken();
        $caption = ($this->get_processor()->get_user()->can_download()) ? '<a href="' . $download_url . '" title="' . __('Download', 'shareonedrive') . '"><i class="fas fa-arrow-circle-down" aria-hidden="true"></i></a>&nbsp' : '';
        $caption .= $caption_description;
        $caption = apply_filters('shareonedrive_gallery_lightbox_caption', $caption, $item, $this);

        $return .= "<a href='" . $link . "' title='" . htmlspecialchars($item->get_basename(), ENT_COMPAT | ENT_HTML401 | ENT_QUOTES) . "'  class='ilightbox-group' data-type='image' $thumbnail rel='ilightbox[" . $this->get_processor()->get_listtoken() . "]' data-caption='$caption'><span class='image-rollover'></span>";

        $return .= "<div class='preloading'></div>";

        $width = $height = round($target_height);
        if ($item->get_media('width')) {
            $width = round(($target_height / $item->get_media('height')) * $item->get_media('width'));
        }

        /* Solved? https://github.com/OneDrive/onedrive-api-docs/issues/664#event-1513817664 */
        //$crop = ($this->get_processor()->get_app()->get_account_type() === 'personal' || $width !== $height) ? 'none' : 'center';
        $crop = 'none';

        $return .= "<img referrerPolicy='no-referrer' class='preloading $class'  src='" . SHAREONEDRIVE_ROOTPATH . "/css/images/transparant.png' data-src='" . $item->get_thumbnail_with_size($height, $width, $crop) . "' data-src-retina='" . $item->get_thumbnail_with_size($height * 2, $width * 2, $crop) . "' width='$width' height='$height' style='width:{$width}px !important;height:{$height}px !important;'/>";

        if ($this->get_processor()->get_shortcode_option('show_filenames') === '1') {
            $text = apply_filters('shareonedrive_gallery_entry_text', $item->get_basename(), $item, $this);
            $return .= "<div class='entry-text'>" . $text . "</div>";
        }

        $return .= "</a>";


        $return .= "</div>\n";
        return $return;
    }

    public function renderDescription($item) {
        $html = '';


        if ($this->get_processor()->get_app()->get_account_type() !== 'personal') { // Only avialable for OneDrive Personal accounts
            return $html;
        }

        if (($this->get_processor()->get_shortcode_option('editdescription') === '0') && empty($item->description)) {
            return $html;
        }

        $title = $item->get_basename() . ((($this->get_processor()->get_shortcode_option('show_filesize') === '1') && ($item->get_size() > 0)) ? ' (' . Helpers::bytes_to_size_1024($item->get_size()) . ')' : '&nbsp;');

        $html .= "<a class='entry_description'><i class='fas fa-info-circle fa-lg'></i></a>\n";
        $html .= "<div class='description_textbox'>";

        if ($this->get_processor()->get_user()->can_edit_description()) {
            $html .= "<span class='entry_edit_description'><a class='entry_action_description' data-id='" . $item->get_id() . "'><i class='fas fa-pen-square fa-lg'></i></a></span>";
        }

        $nodescription = ($this->get_processor()->get_user()->can_edit_description()) ? __('Add a description', 'shareonedrive') : __('No description', 'shareonedrive');
        $description = (!empty($item->description)) ? nl2br($item->get_description()) : $nodescription;

        $html .= "<div class='description_title'>$title</div><div class='description_text'>" . $description . "</div>";
        $html .= "</div>";

        return $html;
    }

    public function renderEditItem($item) {
        $html = '';

        $usercanread = $this->get_processor()->get_user()->can_download();
        $usercanshare = $this->get_processor()->get_user()->can_share();
        $usercanrename = ($item->is_dir()) ? $this->get_processor()->get_user()->can_rename_folders() : $this->get_processor()->get_user()->can_rename_files();
        $usercandelete = ($item->is_dir()) ? $this->get_processor()->get_user()->can_delete_folders() : $this->get_processor()->get_user()->can_delete_files();

        $filename = $item->get_basename();
        $filename .= (($this->get_processor()->get_shortcode_option('show_ext') === '1' && !empty($item->extension)) ? '.' . $item->get_extension() : '');

        /* Download */
        if ($usercanread && $item->is_file()) {
            $html .= "<li><a href='" . SHAREONEDRIVE_ADMIN_URL . "?action=shareonedrive-download&id=" . $item->get_id() . "&dl=1&listtoken=" . $this->get_processor()->get_listtoken() . "' download='" . $filename . "' class='entry_action_download' title='" . __('Download', 'shareonedrive') . "'><i class='fas fa-download fa-lg'></i>&nbsp;" . __('Download', 'shareonedrive') . "</a></li>";
        }

        /* Rename */
        if ($usercanrename) {
            $html .= "<li><a class='entry_action_rename' title='" . __('Rename', 'shareonedrive') . "'><i class='fas fa-tag fa-lg'></i>&nbsp;" . __('Rename', 'shareonedrive') . "</a></li>";
        }

        /* Delete */
        if ($usercandelete) {
            $html .= "<li><a class='entry_action_delete' title='" . __('Delete', 'shareonedrive') . "'><i class='fas fa-trash fa-lg'></i>&nbsp;" . __('Delete', 'shareonedrive') . "</a></li>";
        }

        /* Shortlink */
        if ($item->is_file() && $usercanshare) {
            $html .= "<li><a class='entry_action_shortlink' title='" . __('Share', 'shareonedrive') . "'><i class='fas fa-share-alt fa-lg'></i>&nbsp;" . __('Share', 'shareonedrive') . "</a></li>";
        }


        if ($html !== '') {
            return "<a class='entry_edit_menu'><i class='fas fa-chevron-circle-down fa-lg'></i></a><div id='menu-" . $item->get_id() . "' class='sod-dropdown-menu'><ul data-id='" . $item->get_id() . "' data-name='" . $item->get_basename() . "'>" . $html . "</ul></div>\n";
        }

        return $html;
    }

    public function renderNewFolder() {
        $html = '';
        if ($this->_search === false) {

            if ($this->get_processor()->get_user()->can_add_folders()) {
                $height = $this->get_processor()->get_shortcode_option('targetheight');
                $html .= "<div class='image-container image-folder image-add-folder grey newfolder'>";
                $html .= "<a title='" . __('Add folder', 'shareonedrive') . "'>";
                $html .= "<img class='preloading' src='" . SHAREONEDRIVE_ROOTPATH . "/css/images/transparant.png' data-src='" . plugins_url('css/images/folder.png', dirname(__FILE__)) . "' width='$height' height='$height' style='width:" . $height . "px;height:" . $height . "px;'/>";
                $html .= "<div class='folder-text'>" . __('Add folder', 'shareonedrive') . "</div>";
                $html .= "</a>";
                $html .= "</div>\n";
            }
        }
        return $html;
    }

    public function createImageArray() {
        $imagearray = array();

        $this->setParentFolder();

        //Add folders and files to filelist
        if (count($this->_folder['contents']) > 0) {

            foreach ($this->_folder['contents'] as $node) {

                $child = $node->get_entry();

                /* Check if entry is allowed */
                if (!$this->get_processor()->_is_entry_authorized($node)) {
                    continue;
                }

                /* Check if entry has thumbnail */
                if (!$child->has_own_thumbnail() && $child->is_file()) {
                    continue;
                }

                $imagearray[] = $child;
            }

            $imagearray = $this->get_processor()->sort_filelist($imagearray);
        }

        // Add 'back to Previous folder' if needed
        if (isset($this->_folder['folder'])) {
            $folder = $this->_folder['folder']->get_entry();

            $add_parent_folder_item = true;

            if ($this->_search || $folder->get_id() === $this->get_processor()->get_root_folder()) {
                $add_parent_folder_item = false;
            } elseif ($this->get_processor()->get_user()->can_move_files() || $this->get_processor()->get_user()->can_move_folders()) {
                $add_parent_folder_item = true;
            } elseif ($this->get_processor()->get_shortcode_option('show_breadcrumb') === '1') {
                $add_parent_folder_item = false;
            }

            if ($add_parent_folder_item) {

                foreach ($this->_parentfolders as $parentfolder) {
                    array_unshift($filesarray, $parentfolder);
                }
            }
        }

        return $imagearray;
    }

}
