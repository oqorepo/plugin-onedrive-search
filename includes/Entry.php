<?php

namespace TheLion\ShareoneDrive;

abstract class EntryAbstract {

    public $id;
    public $name;
    public $basename;
    public $path;
    public $parents;
    public $extension;
    public $mimetype;
    public $trashed;
    public $is_dir = false;
    public $size;
    public $description;
    public $last_edited;
    public $last_edited_str;
    public $preview_link;
    public $download_link;
    public $direct_download_link;
    public $save_as = array();
    public $can_preview_by_cloud = false;
    public $can_edit_by_cloud = false;
    public $permissions = array(
        'canpreview' => false,
        'candelete' => false,
        'canadd' => false,
        'canrename' => false
    );
    public $has_own_thumbnail = false;
    public $thumbnail_icon = false;
    public $thumbnail_small = false;
    public $thumbnail_small_cropped = false;
    public $thumbnail_medium = false;
    public $thumbnail_large = false;
    public $thumbnail_original;
    public $icon;
    public $backup_icon;
    public $media;
    public $additional_data = array();
    /* Parent folder, only used for displaying the Previous Folder entry */
    public $pf = false;

    /**
     * Folders that only have a structural function and cannot be used to perform any actions (e.g. delete/rename/zip)
     * Groups and Sites Folders are such folders
     */
    public $_special_folder = false;

    public function __construct($api_entry = null) {
        if ($api_entry !== null) {
            $this->convert_api_entry($api_entry);
        }

        $this->backup_icon = $this->get_default_icon();
    }

    public abstract function convert_api_entry($entry);

    public function to_array() {
        $entry = (array) $this;

        /* Remove Unused data */
//unset($entry['id']);
        unset($entry['parents']);
        unset($entry['mimetype']);
        unset($entry['direct_download_link']);
        unset($entry['additional_data']);
        $entry['size'] = ($entry['size'] > 0) ? $entry['size'] : '';
        return $entry;
    }

    public function __toString() {
        return serialize($this);
    }

    public function get_id() {
        return $this->id;
    }

    public function set_id($id) {
        return $this->id = $id;
    }

    public function get_name() {
        return $this->name;
    }

    public function set_name($name) {
        return $this->name = $name;
    }

    public function get_basename() {
        return $this->basename;
    }

    public function set_basename($basename) {
        return $this->basename = $basename;
    }

    public function get_path() {
        return $this->path;
    }

    public function set_path($path) {
        return $this->path = $path;
    }

    public function get_parents() {
        return $this->parents;
    }

    public function set_parents($parents) {
        return $this->parents = $parents;
    }

    public function has_parents() {
        return (!empty($this->parents)) && (count($this->parents) > 0);
    }

    public function get_extension() {
        return $this->extension;
    }

    public function set_extension($extension) {
        return $this->extension = $extension;
    }

    public function get_mimetype() {
        return $this->mimetype;
    }

    public function set_mimetype($mimetype) {
        return $this->mimetype = $mimetype;
    }

    public function get_is_dir() {
        return $this->is_dir;
    }

    public function is_dir() {
        return $this->is_dir;
    }

    public function is_file() {
        return !$this->is_dir;
    }

    public function set_is_dir($is_dir) {
        return $this->is_dir = (bool) $is_dir;
    }

    public function get_size() {
        return $this->size;
    }

    public function set_size($size) {
        return $this->size = (int) $size;
    }

    public function get_description() {
        return $this->description;
    }

    public function set_description($description) {
        return $this->description = $description;
    }

    public function get_last_edited() {
        return $this->last_edited;
    }

    public function get_last_edited_str() {
        $last_edited = $this->get_last_edited();
        if (empty($last_edited)) {
            return '';
        }
        /* Add datetime string for browser that doen't support toLocaleDateString */
        if (!empty($last_edited)) {
            $this->last_edited_str = get_date_from_gmt(date('Y-m-d H:i:s', $this->get_last_edited()), get_option('date_format') . ' ' . get_option('time_format'));
        }


        return $this->last_edited_str;
    }

    public function set_last_edited($last_edited) {
        return $this->last_edited = $last_edited;
    }

    public function get_preview_link() {
        return $this->preview_link;
    }

    public function set_preview_link($preview_link) {
        return $this->preview_link = $preview_link;
    }

    public function get_download_link() {
        return $this->download_link;
    }

    public function set_download_link($download_link) {
        return $this->download_link = $download_link;
    }

    public function get_direct_download_link() {
        return $this->direct_download_link;
    }

    public function set_direct_download_link($direct_download_link) {
        return $this->direct_download_link = $direct_download_link;
    }

    public function get_save_as() {
        return $this->save_as;
    }

    public function set_save_as($save_as) {
        return $this->save_as = $save_as;
    }

    public function get_can_preview_by_cloud() {
        return $this->can_preview_by_cloud;
    }

    public function set_can_preview_by_cloud($can_preview_by_cloud) {
        return $this->can_preview_by_cloud = $can_preview_by_cloud;
    }

    public function get_can_edit_by_cloud() {
        return $this->can_edit_by_cloud;
    }

    public function set_can_edit_by_cloud($can_edit_by_cloud) {
        return $this->can_edit_by_cloud = $can_edit_by_cloud;
    }

    public function get_permission($permission) {
        if (!isset($this->permissions[$permission])) {
            return null;
        }
        return $this->permissions[$permission];
    }

    public function get_permissions() {
        return $this->permissions;
    }

    public function set_permissions($permissions) {
        return $this->permissions = $permissions;
    }

    public function has_own_thumbnail() {
        return $this->has_own_thumbnail;
    }

    public function set_has_own_thumbnail($v) {
        return $this->has_own_thumbnail = (bool) $v;
    }

    public function get_trashed() {
        return $this->trashed;
    }

    public function set_trashed($v) {
        return $this->trashed = (bool) $v;
    }

    public function get_thumbnail_icon() {
        return $this->thumbnail_icon;
    }

    public function set_thumbnail_icon($thumbnail_icon) {
        return $this->thumbnail_icon = $thumbnail_icon;
    }

    public function get_thumbnail_small() {
        return $this->thumbnail_small;
    }

    public function set_thumbnail_small($thumbnail_small) {
        return $this->thumbnail_small = $thumbnail_small;
    }

    public function get_thumbnail_small_cropped() {
        return $this->thumbnail_small_cropped;
    }

    public function set_thumbnail_small_cropped($thumbnail_small_cropped) {
        return $this->thumbnail_small_cropped = $thumbnail_small_cropped;
    }

    public function get_thumbnail_medium() {
        return $this->thumbnail_medium;
    }

    public function set_thumbnail_medium($thumbnail_medium) {
        return $this->thumbnail_medium = $thumbnail_medium;
    }

    public function get_thumbnail_large() {
        return $this->thumbnail_large;
    }

    public function set_thumbnail_large($thumbnail_large) {
        return $this->thumbnail_large = $thumbnail_large;
    }

    public function get_thumbnail_original() {
        return $this->thumbnail_original;
    }

    public function set_thumbnail_original($thumbnail_original) {
        return $this->thumbnail_original = $thumbnail_original;
    }

    public function get_icon() {
        return $this->icon;
    }

    public function set_icon($icon) {
        return $this->icon = $icon;
    }

    public function get_media($setting = null) {
        if (!empty($setting)) {
            if (isset($this->media[$setting])) {
                return $this->media[$setting];
            } else {
                return null;
            }
        }

        return $this->media;
    }

    public function set_media($media) {
        return $this->media = $media;
    }

    public function get_additional_data() {
        return $this->additional_data;
    }

    public function set_additional_data($additional_data) {
        return $this->additional_data = $additional_data;
    }

    public function is_parent_folder() {
        return $this->pf;
    }

    public function set_parent_folder($value) {
        return $this->pf = (bool) $value;
    }

    public function get_default_icon() {
        
    }

    public function get_default_thumbnail_icon() {
        return $this->get_icon_large();
    }

    public function is_special_folder() {
        return $this->_special_folder !== false;
    }

    public function get_special_folder() {
        return $this->_special_folder;
    }

    public function set_special_folder($value) {
        $this->_special_folder = $value;
    }

}

class Entry extends EntryAbstract {

    public $folder_thumbnails = array();

    public function convert_api_entry($api_entry) {

        /* @var $api_entry \SODOneDrive_Service_Drive_Item  */

        if (!$api_entry instanceof \SODOneDrive_Service_Drive_Item) {
            error_log('[Share-one-Drive message]: ' . sprintf('OneDrive response is not a valid Entry.'));
            die();
        }

        /* Normal Meta Data */
        $this->set_id($api_entry->getId());
        $this->set_name($api_entry->getName());

        if ($api_entry->getFolder() !== null) {
            $this->set_is_dir(true);
        }

        $pathinfo = Helpers::get_pathinfo($api_entry->getName());
        if ($this->is_file() && isset($pathinfo['extension'])) {
            $this->set_extension(strtolower($pathinfo['extension']));
        }
        $this->set_mimetype_from_extension();

        if ($this->is_file()) {
            $this->set_basename(str_replace('.' . $this->get_extension(), '', $this->get_name()));
        } else {
            $this->set_basename($this->get_name());
        }

        $parent = $api_entry->getParentReference();
        if (!empty($parent) && (!empty($parent->id))) {
            $this->set_parents(array($parent->getId()));
            $path = $parent->getPath() . '/' . $this->get_name();
            $this->set_path(str_replace('/drive/root:/', '', $path));
        }


        $this->set_trashed(($api_entry->getDeleted() !== null));

        $this->set_size($api_entry->getSize());
        $this->set_description($api_entry->getDescription());

        $last_modified = $api_entry->getLastModifiedDateTime();
        if (is_string($last_modified)) {
            $dtime = \DateTime::createFromFormat("Y-m-d\TH:i:s.u\Z", $last_modified, new \DateTimeZone('UTC'));

            /* API can return two different formats :( */
            if ($dtime === false) {
                $dtime = \DateTime::createFromFormat("Y-m-d\TH:i:s\Z", $last_modified, new \DateTimeZone('UTC'));
            }

            if ($dtime) {
                $this->set_last_edited($dtime->getTimestamp());
            }
        }

        /* Can File be previewed via OneDrive? 
         * https://msdn.microsoft.com/en-us/library/office/dn659731.aspx#get_links_to_files_and_folders
         */
        $previewsupport = array('csv', 'doc', 'docx', 'odp', 'ods', 'odt', 'pot', 'potm', 'potx', 'pps', 'ppsx', 'ppsxm', 'ppt', 'pptm', 'pptx', 'rtf', 'xls', 'xlsx', 'jpg', 'jpeg', 'gif', 'png', 'pdf', 'mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'ogg', 'oga');
        $openwithonedrive = (in_array($this->get_extension(), $previewsupport));
        if ($openwithonedrive) {
            $this->set_can_preview_by_cloud(true);
        }

        /* Can File be edited via OneDrive */
        $editsupport = array('doc', 'docx', 'odp', 'ods', 'odt', 'pot', 'potm', 'potx', 'pps', 'ppsx', 'ppsxm', 'ppt', 'pptm', 'pptx', 'xls', 'xlsx');
        $editwithonedrive = (in_array($this->get_extension(), $editsupport));
        if ($editwithonedrive) {
            $this->set_can_edit_by_cloud(true);
        }

        /* Set the permissions */
        $permissions = array(
            'canpreview' => $openwithonedrive,
            'candownload' => true,
            'candelete' => true,
            'canadd' => true,
            'canrename' => true,
        );
        $this->set_permissions($permissions);


        /* Direct Download URL, not always available. Valid for just 1 hour! */
        if (isset($api_entry['@microsoft.graph.downloadUrl'])) {
            $this->set_direct_download_link($api_entry['@microsoft.graph.downloadUrl']);
        }
        $this->set_save_as($this->create_save_as());

        /* Icon */
        $default_icon = $this->get_default_icon();
        $this->set_icon($default_icon);

        /* If entry has media data available set it here */
        $mediadata = array();
        $imagemetadata = $api_entry->getImage();

        if (!empty($imagemetadata)) {
            $mediadata['width'] = $imagemetadata->getWidth();
            $mediadata['height'] = $imagemetadata->getHeight();
        }

        $photometadata = $api_entry->getPhoto();
        if (!empty($photometadata)) {
            $date_taken = $photometadata->getTakenDateTime();
            $dtime = \DateTime::createFromFormat("Y-m-d\TH:i:s.u\Z", $date_taken, new \DateTimeZone('UTC'));

            /* API can return two different formats :( */
            if ($dtime === false) {
                $dtime = \DateTime::createFromFormat("Y-m-d\TH:i:s\Z", $date_taken, new \DateTimeZone('UTC'));
            }

            if ($dtime) {
                $mediadata['datetaken'] = $dtime->getTimestamp();
            }
        }

        $audiometadata = $api_entry->getAudio();
        if (!empty($audiometadata)) {
            $mediadata['duration'] = $audiometadata->getDuration();
        }

        $videometadata = $api_entry->getVideo();
        if (!empty($videometadata)) {
            $mediadata['width'] = $videometadata->getWidth();
            $mediadata['height'] = $videometadata->getHeight();
            $mediadata['duration'] = $videometadata->getDuration();
        }

        $this->set_media($mediadata);

        /* Thumbnail */
        $this->set_thumbnails($api_entry->getThumbnails());


        /* Add some data specific for OneDrive Service */
        $additional_data = array(
        );

        $this->set_additional_data($additional_data);
    }

    public function set_thumbnails($thumbnails) {
        $thumbnail_icon = $this->get_default_icon();
        $thumbnail_icon_large = $this->get_icon_large();

        $this->set_thumbnail_icon($thumbnail_icon);
        $this->set_thumbnail_small($thumbnail_icon);
        $this->set_thumbnail_small_cropped($thumbnail_icon);
        $this->set_thumbnail_medium($thumbnail_icon_large);
        $this->set_thumbnail_large($thumbnail_icon_large);

        if (empty($thumbnails)) {
            return;
        }

        $thumbnail = reset($thumbnails);

        $this->set_has_own_thumbnail(true);

        if ($thumbnail->getC48x48() !== null) {
            $this->set_thumbnail_small($thumbnail->getC48x48()->getUrl());
            $this->set_thumbnail_small_cropped($thumbnail->getC48x48()->getUrl());
        } elseif ($thumbnail->getMedium() !== null) {
            $url_medium = $thumbnail->getMedium()->getUrl();
            $pattern = '/width=\d*&height=\d*/';
            $url_medium = preg_replace($pattern, 'height=48&width=48', $url_medium);
            $this->set_thumbnail_small($url_medium);
            $this->set_thumbnail_small_cropped($url_medium);
        }

        if ($thumbnail->getMedium() !== null) {
            $this->set_thumbnail_icon($thumbnail->getMedium()->getUrl());
            $this->set_thumbnail_medium($thumbnail->getMedium()->getUrl());
            $this->set_thumbnail_large($thumbnail->getMedium()->getUrl());
            $this->set_thumbnail_original($thumbnail->getMedium()->getUrl());

            /* Also update media if not availabe in the ImageFacet/PhotoFacet (Business Accounts)
             * to get an idea of the dimensions
             */
            if ($this->get_media('width') === null) {
                $this->media['width'] = $thumbnail->getMedium()->getWidth();
            }
            if ($this->get_media('height') === null) {
                $this->media['height'] = $thumbnail->getMedium()->getHeight();
            }
        }
        if ($thumbnail->getLarge() !== null) {
            $this->set_thumbnail_large($thumbnail->getLarge()->getUrl());
            $this->set_thumbnail_original($thumbnail->getLarge()->getUrl());

            /* Also update media if not availabe in the ImageFacet/PhotoFacet (Business Accounts)
             * to get an idea of the dimensions
             */
            if ($this->get_media('width') === null) {
                $this->media['width'] = $thumbnail->getLarge()->getWidth();
            }
            if ($this->get_media('height') === null) {
                $this->media['height'] = $thumbnail->getLarge()->getHeight();
            }
        }
        if ($thumbnail->getC1500x1500() !== null) {
            $this->set_thumbnail_original($thumbnail->getC1500x1500()->getUrl());
        }

        /* Folder images contain multiple thumbnail sets */
        if ($this->is_dir()) {
            $this->set_folder_thumbnails($thumbnails);
        }
    }

    public function set_mimetype_from_extension() {
        if ($this->is_dir()) {
            return null;
        }

        if (empty($this->extension)) {
            return null;
        }
        include_once 'mime-types/mime-types.php';
        $mimetype = ShareoneDrive_getMimeType($this->get_extension());
        $this->set_mimetype($mimetype);
    }

    public function get_default_icon() {
        return Helpers::get_default_icon($this->get_mimetype(), $this->is_dir());
    }

    public function get_icon_large() {
        return str_replace('32x32', '256x256', $this->get_icon());
    }

    public function create_save_as() {

        switch ($this->get_extension()) {
            case 'csv':
            case 'doc':
            case 'docx':
            case 'odp':
            case 'ods':
            case 'odt':
            case 'pot':
            case 'potm':
            case 'potx':
            case 'pps':
            case 'ppsx':
            case 'ppsxm':
            case 'ppt':
            case 'pptm':
            case 'pptx':
            case 'rtf':
            case 'xls':
            case 'xlsx':
                $save_as = array(
                    'PDF' => array('mimetype' => 'application/pdf', 'extension' => 'pdf', 'icon' => 'fa-file-pdf'),
                );
                break;
            default:
                return array();
        }

        return $save_as;
    }

    public function get_date_taken() {
        $date_taken = $this->get_media('datetaken');

        if (empty($date_taken)) {
            $date_taken = $this->get_last_edited();
        }

        return $date_taken;
    }

    public function get_thumbnail_with_size($height, $width, $crop = 'none', $resizeable_url = false) {

        if ($resizeable_url === false) {
            $resizeable_url = $this->get_thumbnail_small_cropped();
        }

        if ($this->has_own_thumbnail() === false) {
            return $this->get_thumbnail_large();
        }


        $pattern = '/width=\d*/';
        $new_url = preg_replace($pattern, 'width=' . $width, $resizeable_url);

        $pattern = '/height=\d*/';
        $new_url = preg_replace($pattern, 'height=' . $height, $new_url);

        $new_url .= (strpos($new_url, 'cropmode') === false) ? '&cropmode=none' : '';
        return str_replace('cropmode=none', 'cropmode=' . $crop, $new_url);
    }

    public function set_folder_thumbnails($folder_thumbnails) {
        return $this->folder_thumbnails = $folder_thumbnails;
    }

    public function get_folder_thumbnails() {
        return $this->folder_thumbnails;
    }

}
