<?php

namespace TheLion\ShareoneDrive;

class Upload {

    /**
     *
     * @var \TheLion\ShareoneDrive\Client 
     */
    private $_client;

    /**
     *
     * @var \TheLion\ShareoneDrive\Processor 
     */
    private $_processor;

    /**
     *
     * @var WPC_UploadHandler 
     */
    private $_upload_handler;

    public function __construct(\TheLion\ShareoneDrive\Processor $_processor = null) {
        $this->_client = $_processor->get_client();
        $this->_processor = $_processor;

        /* Upload File to server */
        if (!class_exists('WPC_UploadHandler')) {
            require('jquery-file-upload/server/UploadHandler.php');
        }
    }

    public function do_upload() {

        if ($this->get_processor()->get_shortcode_option('demo') === '1') {
            /* TO DO LOG + FAIL ERROR */
            die(-1);
        }

        $shortcode_max_file_size = $this->get_processor()->get_shortcode_option('maxfilesize');
        $accept_file_types = '/.(' . $this->get_processor()->get_shortcode_option('upload_ext') . ')$/i';
        $post_max_size_bytes = min(Helpers::return_bytes(ini_get('post_max_size')), Helpers::return_bytes(ini_get('upload_max_filesize')));
        $max_file_size = ($shortcode_max_file_size !== '0') ? Helpers::return_bytes($shortcode_max_file_size) : $post_max_size_bytes;

        $options = array(
            'access_control_allow_methods' => array('POST', 'PUT'),
            'accept_file_types' => $accept_file_types,
            'inline_file_types' => '/\.____$/i',
            'orient_image' => false,
            'image_versions' => array(),
            'max_file_size' => $max_file_size,
            'print_response' => false
        );

        $error_messages = array(
            1 => __('The uploaded file exceeds the upload_max_filesize directive in php.ini', 'shareonedrive'),
            2 => __('The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form', 'shareonedrive'),
            3 => __('The uploaded file was only partially uploaded', 'shareonedrive'),
            4 => __('No file was uploaded', 'shareonedrive'),
            6 => __('Missing a temporary folder', 'shareonedrive'),
            7 => __('Failed to write file to disk', 'shareonedrive'),
            8 => __('A PHP extension stopped the file upload', 'shareonedrive'),
            'post_max_size' => __('The uploaded file exceeds the post_max_size directive in php.ini', 'shareonedrive'),
            'max_file_size' => __('File is too big', 'shareonedrive'),
            'min_file_size' => __('File is too small', 'shareonedrive'),
            'accept_file_types' => __('Filetype not allowed', 'shareonedrive'),
            'max_number_of_files' => __('Maximum number of files exceeded', 'shareonedrive'),
            'max_width' => __('Image exceeds maximum width', 'shareonedrive'),
            'min_width' => __('Image requires a minimum width', 'shareonedrive'),
            'max_height' => __('Image exceeds maximum height', 'shareonedrive'),
            'min_height' => __('Image requires a minimum height', 'shareonedrive')
        );

        $this->upload_handler = new \WPC_UploadHandler($options, false, $error_messages);
        $response = @$this->upload_handler->post(false);

        /* Upload files to OneDrive */
        foreach ($response['files'] as &$file) {
            /* Set return Object */
            $file->listtoken = $this->get_processor()->get_listtoken();
            $file->hash = $_POST['hash'];
            $file->convert = false;

            /* Set Progress */
            $return = array('file' => $file, 'status' => array('bytes_up_so_far' => 0, 'total_bytes_up_expected' => $file->size, 'percentage' => 0, 'progress' => 'starting'));
            self::set_upload_progress($file->hash, $return);

            if (isset($file->error)) {
                $file->error = __('Uploading failed', 'shareonedrive') . ': ' . $file->error;
                $return['file'] = $file;
                $return['status']['progress'] = 'failed';
                self::set_upload_progress($file->hash, $return);
                echo json_encode($return);

                error_log('[Share-one-Drive message]: ' . sprintf('Uploading failed: %s', $file->error));
                die();
            }

            /** Check if the user hasn't reached its usage limit */
            $max_user_folder_size = $this->get_processor()->get_shortcode_option('max_user_folder_size');
            if ($this->get_processor()->get_shortcode_option('user_upload_folders') !== '0' && $max_user_folder_size !== '-1') {
                $disk_usage_after_upload = $this->get_client()->get_entry()->get_entry()->get_size() + $file->size;
                $max_allowed_bytes = Helpers::return_bytes($max_user_folder_size);
                if ($disk_usage_after_upload > $max_allowed_bytes) {
                    $return['status']['progress'] = 'failed';
                    $file->error = __('You have reached your usage limit of', 'shareonedrive') . ' ' . Helpers::bytes_to_size_1024($max_allowed_bytes);
                    self::set_upload_progress($hash, $return);
                    echo json_encode($return);
                    die();
                }
            }

            /* Write file */
            $chunkSizeBytes = 20 * 320 * 1000; // Multiple of 320kb, the recommended fragment size is between 5-10 MB.

            /* Update Mime-type if needed (for IE8 and lower?) */
            include_once 'mime-types/mime-types.php';
            $fileExtension = pathinfo($file->name, PATHINFO_EXTENSION);
            $file->type = ShareoneDrive_getMimeType($fileExtension);

            /* Create new OneDrive File */
            $body = array(
                'item' => array(
                    '@microsoft.graph.conflictBehavior' => ($this->get_processor()->get_shortcode_option('overwrite') === '1') ? 'replace' : 'rename'
                )
            );

            /* Call the API with the media upload, defer so it doesn't immediately return. */
            $this->get_app()->get_client()->setDefer(true);

            try {
                $request = $this->get_app()->get_drive()->items->upload($file->name, $this->get_processor()->get_last_folder(), $body);
            } catch (\Exception $ex) {
                $file->error = __('Not uploaded to OneDrive', 'shareonedrive') . ': ' . $ex->getMessage();
                $return['status']['progress'] = 'failed';
                self::set_upload_progress($file->hash, $return);
                echo json_encode($return);

                error_log('[Share-one-Drive message]: ' . sprintf('Not uploaded to OneDrive: %s', $ex->getMessage()));

                die();
            }

            /* Create a media file upload to represent our upload process. */
            $media = new \SODOneDrive_Http_MediaFileUpload(
                    $this->get_app()->get_client(), $request, null, null, true, $chunkSizeBytes
            );

            $filesize = filesize($file->tmp_path);
            $media->setFileSize($filesize);


            /* Start partialy upload 
              Upload the various chunks. $status will be false until the process is
              complete. */
            try {
                $upload_status = false;
                $bytesup = 0;
                $handle = fopen($file->tmp_path, "rb");
                while (!$upload_status && !feof($handle)) {
                    @set_time_limit(60);
                    $chunk = fread($handle, $chunkSizeBytes);
                    $upload_status = $media->nextChunk($chunk);
                    $bytesup += $chunkSizeBytes;

                    /* Update progress */
                    /* Update the progress */
                    $status = array(
                        'bytes_up_so_far' => $bytesup,
                        'total_bytes_up_expected' => $file->size,
                        'percentage' => ( round(($bytesup / $file->size) * 100) ),
                        'progress' => 'uploading'
                    );

                    $current = self::get_upload_progress($file->hash);
                    $current['status'] = $status;
                    self::set_upload_progress($file->hash, $current);
                }

                fclose($handle);
            } catch (\Exception $ex) {
                $file->error = __('Not uploaded to OneDrive', 'shareonedrive') . ': ' . $ex->getMessage();
                $return['file'] = $file;
                $return['status']['progress'] = 'failed';
                self::set_upload_progress($file->hash, $return);
                echo json_encode($return);

                error_log('[Share-one-Drive message]: ' . sprintf('Not uploaded to OneDrive: %s', $ex->getMessage()));
                die();
            }

            $this->get_app()->get_client()->setDefer(false);

            if (empty($upload_status)) {
                $file->error = __('Not uploaded to OneDrive', 'shareonedrive');
                $return['file'] = $file;
                $return['status']['progress'] = 'failed';
                self::set_upload_progress($file->hash, $return);
                echo json_encode($return);

                error_log('[Share-one-Drive message]: ' . sprintf('Not uploaded to OneDrive'));
                die();
            }

            /* check if uploaded file has size */
            usleep(500000); // wait a 0.5 sec so OneDrive can create a thumbnail.
            $cached_entry = $this->get_client()->get_entry($upload_status->getId());

            if ($cached_entry->get_entry()->get_size() === 0) {
                $file->error = __('Not succesfully uploaded to OneDrive', 'shareonedrive');
                $return['status']['progress'] = 'failed';
                return;
            }

            /* Add new file to our Cache */
            $file->completepath = $cached_entry->get_path($this->get_processor()->get_root_folder());
            $file->fileid = $cached_entry->get_id();
            $file->filesize = Helpers::bytes_to_size_1024($file->size);
            $file->link = urlencode($cached_entry->get_entry()->get_preview_link());
            $file->folderurl = false;
        }

        $return['file'] = $file;
        $return['status']['progress'] = 'finished';
        $return['status']['percentage'] = '100';
        self::set_upload_progress($file->hash, $return);

        /* Create response */
        echo json_encode($return);
        die();
    }

    public function do_upload_direct() {
        if ((!isset($_REQUEST['filename'])) || (!isset($_REQUEST['file_size'])) || (!isset($_REQUEST['mimetype']))) {
            die();
        }

        if ($this->get_processor()->get_shortcode_option('demo') === '1') {
            echo json_encode(array('result' => 0));
            die();
        }

        $name = $_REQUEST['filename'];
        $size = $_REQUEST['file_size'];
        $mimetype = $_REQUEST['mimetype'];

        /** Check if the user hasn't reached its usage limit */
        $max_user_folder_size = $this->get_processor()->get_shortcode_option('max_user_folder_size');
        if ($this->get_processor()->get_shortcode_option('user_upload_folders') !== '0' && $max_user_folder_size !== '-1') {
            $disk_usage_after_upload = $this->get_client()->get_entry()->get_entry()->get_size() + $size;
            $max_allowed_bytes = Helpers::return_bytes($max_user_folder_size);
            if ($disk_usage_after_upload > $max_allowed_bytes) {
                error_log('[Share-one-Drive message]: ' . __('You have reached your usage limit of', 'shareonedrive') . ' ' . Helpers::bytes_to_size_1024($max_allowed_bytes));
                echo json_encode(array('result' => 0));
            }
        }

        /* Call the API with the media upload, defer so it doesn't immediately return. */
        $this->get_app()->get_client()->setDefer(true);

        /* Create new OneDrive File */
        /* Create new OneDrive File */
        $body = array(
            'item' => array(
                '@microsoft.graph.conflictBehavior' => ($this->get_processor()->get_shortcode_option('overwrite') === '1') ? 'replace' : 'rename'
            )
        );

        try {
            $request = $this->get_app()->get_drive()->items->upload($name, $this->get_processor()->get_last_folder(), $body);
        } catch (Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Not uploaded to OneDrive: %s', $ex->getMessage()));
        }

        /* Create a media file upload to represent our upload process. */
        $origin = $_REQUEST['orgin'];
        $request_headers = $request->getRequestHeaders();
        $request_headers['Origin'] = $origin;
        $request->setRequestHeaders($request_headers);

        $chunkSizeBytes = 20 * 320 * 1000; // Multiple of 320kb, the recommended fragment size is between 5-10 MB.
        $media = new \SODOneDrive_Http_MediaFileUpload(
                $this->get_app()->get_client(), $request, null, null, true, $chunkSizeBytes
        );
        $media->setFileSize($size);

        try {
            $url = $media->getResumeUri();
            echo json_encode(array('result' => 1, 'url' => $url, 'convert' => false));
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Not uploaded to OneDrive: %s', $ex->getMessage()));
            echo json_encode(array('result' => 0));
        }

        die();
    }

    static public function get_upload_progress($file_hash) {
        return get_transient('shareonedrive_upload_' . substr($file_hash, 0, 40));
    }

    static public function set_upload_progress($file_hash, $status) {
        /* Update progress */
        return set_transient('shareonedrive_upload_' . substr($file_hash, 0, 40), $status, HOUR_IN_SECONDS);
    }

    public function get_upload_status() {
        $hash = $_REQUEST['hash'];

        /* Try to get the upload status of the file */
        for ($_try = 1; $_try < 6; $_try++) {
            $result = self::get_upload_progress($hash);

            if ($result !== false) {

                if ($result['status']['progress'] === 'failed' || $result['status']['progress'] === 'finished') {
                    delete_transient('shareonedrive_upload_' . substr($hash, 0, 40));
                }

                break;
            }

            /* Wait a moment, perhaps the upload still needs to start */
            usleep(500000 * $_try);
        }

        if ($result === false) {
            $result = array('file' => false, 'status' => array('bytes_up_so_far' => 0, 'total_bytes_up_expected' => 0, 'percentage' => 0, 'progress' => 'failed'));
        }

        echo json_encode($result);
        die();
    }

    public function upload_convert() {
        /* NOT IMPLEMENTED */
    }

    public function upload_post_process() {
        if ((!isset($_REQUEST['files'])) || count($_REQUEST['files']) === 0) {
            echo json_encode(array('result' => 0));
            die();
        }

        /* Update the cache to process all changes */
        $this->get_processor()->get_cache()->pull_for_changes(true);

        $uploaded_files = $_REQUEST['files'];
        $_uploaded_entries = array();

        foreach ($uploaded_files as $file_id) {

            $cachedentry = $this->get_client()->get_entry($file_id, false);

            if ($cachedentry === false) {
                continue;
            }

            /* Upload Hook */
            $cachedentry = apply_filters('shareonedrive_upload', $cachedentry, $this->_processor);
            $_uploaded_entries[] = $cachedentry;

            do_action('shareonedrive_log_event', 'shareonedrive_uploaded_entry', $cachedentry);
        }

        /* Send email if needed */
        if (count($_uploaded_entries) > 0) {
            if ($this->get_processor()->get_shortcode_option('notificationupload') === '1') {
                $this->get_processor()->send_notification_email('upload', $_uploaded_entries);
            }
        }

        /* Return information of the files */
        $files = array();
        foreach ($_uploaded_entries as $cachedentry) {

            $file = array();
            $file['name'] = $cachedentry->get_entry()->get_name();
            $file['type'] = $cachedentry->get_entry()->get_mimetype();
            $file['completepath'] = $cachedentry->get_path($this->get_processor()->get_root_folder());
            $file['fileid'] = $cachedentry->get_id();
            $file['filesize'] = Helpers::bytes_to_size_1024($cachedentry->get_entry()->get_size());
            $file['link'] = urlencode($this->get_client()->get_shared_link($cachedentry, 'view'));
            $file['folderurl'] = false;
            $files[$file['fileid']] = $file;
        }

        do_action('shareonedrive_upload_post_process', $_uploaded_entries, $this->_processor);

        /* Clear Cached Requests */
        CacheRequest::clear_local_cache_for_shortcode($this->get_processor()->get_listtoken());

        echo json_encode(array('result' => 1, 'files' => $files));
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor() {
        return $this->_processor;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Client
     */
    public function get_client() {
        return $this->_client;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\App
     */
    public function get_app() {
        return $this->get_processor()->get_app();
    }

}

?>