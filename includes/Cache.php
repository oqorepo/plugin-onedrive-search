<?php

namespace TheLion\ShareoneDrive;

class Cache {

    /**
     *  @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;

    /**
     * Contains the location of the cache
     * Default is 'files'  as this is faster than via DB.
     * However in some cases, the DB easier to setup for some users....
     * @var string 
     */
    private $_cache_type = 'filesystem';

    /**
     * Contains the location to the cache file
     * @var string 
     */
    private $_cache_location;

    /**
     * Contains the file handle in case the plugin has to work
     * with a file for unlocking/locking
     * @var type 
     */
    private $_cache_file_handle = null;

    /**
     * $_nodes contains all the cached entries that are present
     * in the Cache File or Database
     * @var \TheLion\ShareoneDrive\CacheNode[] 
     */
    private $_nodes = array();

    /**
     * Is set to true when a change has been made in the cache.
     * Forcing the plugin to save the cache when needed
     * @var boolean 
     */
    private $_updated = false;

    /**
     * $_last_update contains a timestamp of the latest check
     * for new updates
     * @var string 
     */
    private $_last_check_for_update = null;

    /**
     * $_last_id contains an ID of the latest update check
     * This can be anything (e.g. a File ID or Change ID), it differs per Cloud Service
     * @var mixed 
     */
    private $_last_check_token = null;

    /**
     * How often do we need to poll for changes? (default: 5 minutes)
     * Each Cloud service has its own optimum setting.
     * WARNING: Please don't lower this setting when you are not using your own Apps!!!
     * @var int 
     */
    private $_max_change_age = 300;

    /**
     * Set after how much time the cached noded should be refreshed.
     * This value can be overwritten by Cloud Service Cache classes
     * Default:  needed for download/thumbnails urls (1 hour?) 
     * @var int 
     */
    protected $_max_entry_age = 1800;

    public function __construct(Processor $processor) {
        $this->_processor = $processor;

        $this->_cache_name = get_current_blog_id() . ($processor->get_listtoken() === '' ? '' : '_' . $processor->get_listtoken()) . '.index';
        $this->_cache_type = $this->_processor->settings['cache'];
        $this->_cache_location = SHAREONEDRIVE_CACHEDIR . '/' . $this->_cache_name;

        /* Load Cache */
        $this->load_cache();
    }

    public function load_cache() {
        $cache = false;

        /* 1: Try to load the Local cache when needed */
        if ($this->get_cache_type() === 'filesystem') {
            $cache = $this->_read_local_cache('close');
        }

        /* 2: Fall Back to database cache if cache file isn't writable */
        if ($this->get_cache_type() === 'database' || $cache === false) {
            $cache = get_option('cache_' . $this->get_cache_name(), false);
        }

        if (function_exists('gzdecode')) {
            $cache = @gzdecode($cache);
        }

        /* 3: Unserialize the Cache, and reset if it became somehow corrupt */
        if (!empty($cache) && !is_array($cache)) {
            $this->_unserialize_cache($cache);
        }

        /* Set all Parent and Children */
        if (count($this->_nodes) > 0) {
            foreach ($this->_nodes as $node) {
                if ($node->has_parents()) {
                    foreach ($node->get_parents() as $key => $parent_id) {
                        if (!($parent_id instanceof CacheNode)) {
                            $node->remove_parent_by_id($key);
                        }
                        $parent_in_tree = $this->get_node_by_id($parent_id);
                        if ($parent_in_tree !== false) {
                            $node->set_parent($parent_in_tree);
                        }
                    }
                }

                if ($node->has_children()) {
                    foreach ($node->get_children() as $key => $child_id) {
                        if (!($child_id instanceof CacheNode)) {
                            $node->remove_child_by_id($key);
                        }
                    }
                }

                /* Remove leftovers */
                if (!$node->has_children() && !$node->has_parents() && $node->is_expired() && !$node->is_hidden()) {
                    $this->remove_from_cache($node->get_id(), 'deleted');
                }

                /* Remove trashed entries */
                if ($node->get_entry() instanceof Entry) {
                    if ($node->get_entry()->get_trashed()) {
                        $this->remove_from_cache($node->get_id(), 'deleted');
                    }
                }
            }
        }
    }

    protected function _read_local_cache($close = false) {

        $handle = $this->_get_cache_file_handle();
        if (empty($handle)) {
            $this->_create_local_lock(LOCK_SH);
        }

        clearstatcache();
        rewind($this->_get_cache_file_handle());

        $data = fread($this->_get_cache_file_handle(), filesize($this->get_cache_location()));

        if ($close !== false) {
            $this->_unlock_local_cache();
        }

        return $data;
    }

    protected function _create_local_lock($type) {
        /*  Check if file exists */
        $file = $this->get_cache_location();

        if (!file_exists($file)) {
            @file_put_contents($file, $this->_serialize_cache());

            if (!is_writable($file)) {
                error_log('[Share-one-Drive message]: ' . sprintf('Cache file (%s) is not writable', $file));
                die(sprintf('Cache file (%s) is not writable', $file));
            }
        }

        /* Check if the file is more than 1 minute old. */
        $requires_unlock = ((filemtime($file) + 60) < (time()));

        /* Check if file is already opened and locked in this process */
        $handle = $this->_get_cache_file_handle();
        if (empty($handle)) {
            $handle = fopen($file, 'c+');
            if (!is_resource($handle)) {
                error_log('[Share-one-Drive message]: ' . sprintf('Cache file (%s) is not writable', $file));
                die(sprintf('Cache file (%s) is not writable', $file));
            }
            $this->_set_cache_file_handle($handle);
        }

        @set_time_limit(60);

        if (!flock($this->_get_cache_file_handle(), $type)) {
            /*
             * If the file cannot be unlocked and the last time
             * it was modified was 1 minute, assume that 
             * the previous process died and unlock the file manually
             */
            if ($requires_unlock) {
                $this->_unlock_local_cache();
            }
            /* Try to lock the file again */
            flock($this->_get_cache_file_handle(), LOCK_EX);
        }
        @set_time_limit(60);

        return true;
    }

    protected function _save_local_cache() {
        if (!$this->_create_local_lock(LOCK_EX)) {
            return false;
        }

        $data = $this->_serialize_cache($this);

        ftruncate($this->_get_cache_file_handle(), 0);
        rewind($this->_get_cache_file_handle());

        $result = fwrite($this->_get_cache_file_handle(), $data);

        $this->_unlock_local_cache();
        $this->set_updated(false);
        return true;
    }

    protected function _save_database_cache() {
        $this->_wait_for_database_lock();

        $data = $this->_serialize_cache($this);

        @update_option('cache_' . $this->get_cache_name(), $data);

        $this->_unlock_database_cache();
        $this->set_updated(false);
        return true;
    }

    protected function _unlock_local_cache() {
        $handle = $this->_get_cache_file_handle();
        if (!empty($handle)) {
            flock($this->_get_cache_file_handle(), LOCK_UN);
            fclose($this->_get_cache_file_handle());
            $this->_set_cache_file_handle(null);
        }

        clearstatcache();
        return true;
    }

    protected function _is_database_locked() {
        return get_option('share_one_drive_cache_locked', false);
    }

    protected function _wait_for_database_lock() {
        $locked = $this->_is_database_locked();
        if (!$locked) {
            /* DB cache isn't locked, set new lock */
            $this->_set_database_lock();
            return false;
        } elseif (((int) $locked + 5) < time()) {
            /* DB Cache is locked, but longer than 5 seconds. Assume the owning process died off and set new lock */
            $this->_set_database_lock();
            return false;
        }

        /* Else wait max 5 seconde */
        $tries = 5;
        $cnt = 0;

        do {
            /* 500 ms is a long time to sleep, but it does stop the server from burning all resources on polling locks.. */
            usleep(500000);
            $this->load_cache();
            $cnt++;
        } while ($cnt <= $tries && $this->_is_database_locked());

        $this->_set_database_lock();
        return false;
    }

    protected function _unlock_database_cache() {
        $this->_set_database_lock(false);
    }

    protected function _set_database_lock($lock = true) {
        $value = ($lock) ? time() : false;
        return update_option('share_one_drive_cache_locked', $value);
    }

    public function reset_cache() {
        $this->_nodes = array();
        $this->set_last_check_for_update();
        $this->set_last_check_token(null);
        $this->update_cache();
    }

    public function update_cache($clear_request_cache = true) {
        if ($this->is_updated()) {

            /* Clear Cached Requests, not needed if we only pulled for updates without receiving any changes */
            if ($clear_request_cache) {
                CacheRequest::clear_local_cache_for_shortcode($this->get_processor()->get_listtoken());
            }

            switch ($this->get_cache_type()) {
                case 'filesystem':
                    $saved = $this->_save_local_cache();
                    break;
                case 'database':
                    $saved = $this->_save_database_cache();
                    break;
            }

            $this->set_updated(false);
        }
    }

    public function is_cached($value, $findby = 'id', $as_parent = false) {

        /* Find the node by ID/NAME */
        $node = null;
        if ($findby === 'id') {
            $node = $this->get_node_by_id($value);
        } elseif ($findby === 'name') {
            $node = $this->get_node_by_name($value);
        }

        /* Return if nothing can be found in the cache */
        if (empty($node)) {
            return false;
        }

        if ($node->get_entry() === null) {
            return false;
        }

        if (!$as_parent && !$node->is_loaded()) {
            return false;
        }

        /* Check if the requested node is expired
         * In that case, unset the node and remove the child nodes
         *  */
        if (!$as_parent && $node->is_expired() && $node->get_id() === $this->get_processor()->get_requested_entry()) {
            if ($node->get_entry()->is_dir()) {
                return $this->get_processor()->get_client()->update_expired_folder($node);
            } else {
                return $this->get_processor()->get_client()->update_expired_entry($node);
            }
        }

        /* Check if the children of the node are loaded. */
        if (!$as_parent && !$node->has_loaded_children()) {
            return false;
        }

        return $node;
    }

    public function build_folder_structure($entries) {

        /* Add all entries in folder to cache */
        /* @var $entry Entry */
        foreach ($entries as $entry) {
            $cached_node = $this->add_node($entry);
            $cached_node->set_entry($entry);
            $cached_node->set_loaded(false);

            if (!$entry->has_parents()) {
                $cached_node->set_hidden(true);
            }
        }

        /* Walk through the cache to linked all entries to the parent */
        foreach ($this->get_nodes() as $cached_node) {
            if (!$cached_node->get_entry()->has_parents()) {
                continue;
            }

            foreach ($cached_node->get_entry()->get_parents() as $parent_id) {
                $parent_in_tree = $this->is_cached($parent_id, 'id', 'as_parent');
                /* Parent does already exists in our cache */
                if ($parent_in_tree !== false) {
                    $cached_node->set_parent($parent_in_tree);
                    $cached_node->set_parents_found();
                } else {
                    $cached_node->set_hidden(true);
                }
            }
        }
    }

    /**
     * 
     * @param \TheLion\ShareoneDrive\Entry $entry
     * @return \TheLion\ShareoneDrive\CacheNode
     */
    public function add_to_cache(Entry $entry) {
        /* Check if entry is present in cache */
        $cached_node = $this->get_node_by_id($entry->get_id());

        /* If entry is not yet present in the cache,
         * create a new Node
         */
        if (($cached_node === false)) {
            $cached_node = $this->add_node($entry);
        } else {
            $cached_node->set_name($entry->get_name());
        }


        $this->set_updated();

        /* Set new Expire date */
        $cached_node->set_expired(time() + $this->get_max_entry_age());

        /* Set new Entry in node */
        $cached_node->set_entry($entry);
        $cached_node->set_loaded(true);

        /* Set Loaded_Children to true if entry isn't a folder */
        if ($entry->is_file()) {
            $cached_node->set_loaded_children(true);
        }

        /* If $entry hasn't parents, it is the root */
        if (!$entry->has_parents()) {
            $cached_node->set_parents_found(true);
            $cached_node->set_root();
            return $cached_node;
        }

        /*
         * If parents of $entry doesn't exist in our cache yet, 
         * We need to get it via the API
         */
        $getparents = array();
        foreach ($entry->get_parents() as $parent_id) {


            $parent_in_tree = $this->is_cached($parent_id, 'id', 'as_parent');

            if ($parent_in_tree === false) {
                $newparent = $this->get_processor()->get_client()->get_folder($parent_id, false);

                if (empty($newparent)) {
                    /* If the parent still can't be found?, set it to root node */
                    $parent_in_tree = $this->get_root_node();
                } else {
                    $parent_in_tree = $newparent['folder'];
                }
            }

            $cached_node->set_parent($parent_in_tree);
        }

        $cached_node->set_parents_found(true);

        $this->set_updated();

        /* Return the cached Node */
        return $cached_node;
    }

    public function remove_from_cache($entry_id, $reason = 'update', $parent_id = false) {
        $node = $this->get_node_by_id($entry_id);

        if ($node === false) {
            return false;
        }

        if ($reason === 'update') {
            /* Just remove the parents */
            $node->remove_parents();
            /* $node->set_entry(null);
              $node->set_expired(null);
              $node->set_loaded(false);
              $node->set_loaded_children(false);
              $node->set_parents_found(false); */

            /* First remove all children starting from this entry */
            /* if ($node->has_children()) {
              foreach ($node->get_children() as $child) {
              $this->remove_from_cache($child->get_id(), 'update', $node->get_id());
              }
              } */
        } else if ($reason === 'moved') {
            $node->remove_parents();
        } elseif ($reason === 'deleted') {

            /* First remove all children starting from this entry */
            /* if ($node->has_children()) {
              foreach ($node->get_children() as $child) {
              $this->remove_from_cache($child->get_id(), 'deleted', $node->get_id());
              }
              } */

            /* Remove the entry from the parent */
            //if ($parent_id !== false) {
            //  $node->remove_parent_by_id($parent_id);
            //} else {
            $node->remove_parents();
            //}

            /* After that, remove the current entry if no parents are left */
            //if (!$node->has_children()) {
            unset($this->_nodes[$entry_id]);
            //}
        }

        $this->set_updated();
        return true;
    }

    /**
     * 
     * @return boolean|\TheLion\ShareoneDrive\CacheNode
     */
    public function get_root_node() {
        if (count($this->get_nodes()) === 0) {
            return false;
        }

        foreach ($this->get_nodes() as $node) {
            if ($node->is_root()) {
                return $node;
            }
        }
        return false;
    }

    public function get_node_by_id($id) {
        if (!isset($this->_nodes[$id])) {
            return false;
        }

        return $this->_nodes[$id];
    }

    public function get_node_by_name($name, $parent = null) {
        if (!$this->has_nodes()) {
            return false;
        }

        $parent_id = ($parent instanceof CacheNode) ? $parent->get_id() : $parent;

        /**
         * @var $node \TheLion\ShareoneDrive\CacheNode
         */
        foreach ($this->_nodes as $node) {
            if ($node->get_name() === $name) {
                if ($parent === null) {
                    return $node;
                }

                if ($node->is_in_folder($parent_id)) {
                    return $node;
                }
            }
        }

        return false;
    }

    public function has_nodes() {
        return (count($this->_nodes) > 0 );
    }

    /**
     * @return \TheLion\ShareoneDrive\CacheNode[]
     */
    public function get_nodes() {
        return $this->_nodes;
    }

    public function add_node(Entry $entry) {
// TODO: Set expire based on Cloud Service
        $cached_node = new CacheNode(
                array(
            '_id' => $entry->get_id(),
            '_name' => $entry->get_name()
                )
        );
        return $this->set_node($cached_node);
    }

    public function set_node(CacheNode $node) {
        $id = $node->get_id();
        $this->_nodes[$id] = $node;
        return $this->_nodes[$id];
    }

    public function pull_for_changes($force_update = false) {
        $force = (defined('FORCE_REFRESH') ? true : $force_update);

        /* Check if we need to check for updates */
        $current_time = time();
        $update_needed = ($this->get_last_check_for_update() + $this->get_max_change_age());
        if (($current_time < $update_needed) && !$force) {
            return false;
        } elseif ($force === true && ($this->get_last_check_for_update() > $current_time - 10)) { // Don't pull again if the request was within 10 seconds
            return false;
        }

        $result = $this->get_processor()->get_client()->get_changes($this->get_last_check_token());

        if (empty($result)) {
            return false;
        }

        list($new_change_token, $changes) = $result;
        $this->set_last_check_token($new_change_token);
        $this->set_last_check_for_update();

        if (is_array($changes) && count($changes) > 0) {
            $result = $this->_process_changes($changes);

            if (!defined('HAS_CHANGES')) {
                define('HAS_CHANGES', true);
            }

            $this->update_cache();

            return true;
        } else {
            $this->update_cache(false);
        }

        return false;
    }

    private function _process_changes($changes = array()) {

        foreach ($changes as $entry_id => $change) {

            if ($change === 'deleted') {
                $this->remove_from_cache($entry_id, 'deleted');
            } else {
                /* Update cache with new entry */
                if ($change instanceof Entry) {

                    /* Keep thumbnails as that isn't yet provided by API */
                    $old_cached_entry = $this->get_node_by_id($entry_id);
                    if (($old_cached_entry !== false)) {
                        $old_entry = $old_cached_entry->get_entry();
                        $thumbnail_icon = $old_entry->thumbnail_icon;
                        $thumbnail_small = $old_entry->thumbnail_small;
                        $thumbnail_small_cropped = $old_entry->thumbnail_small_cropped;
                        $thumbnail_medium = $old_entry->thumbnail_medium;
                        $thumbnail_large = $old_entry->thumbnail_large;
                        $thumbnail_original = $old_entry->thumbnail_original;
                        $folder_thumbnails = $old_entry->folder_thumbnails;
                    }

                    $cached_entry = $this->add_to_cache($change);
                    if (($old_cached_entry !== false)) {
                        $new_entry = $cached_entry->get_entry();
                        $new_entry->thumbnail_icon = $thumbnail_icon;
                        $new_entry->thumbnail_small = $thumbnail_small;
                        $new_entry->thumbnail_small_cropped = $thumbnail_small_cropped;
                        $new_entry->thumbnail_medium = $thumbnail_medium;
                        $new_entry->thumbnail_large = $thumbnail_large;
                        $new_entry->thumbnail_original = $thumbnail_original;
                        $new_entry->folder_thumbnails = $folder_thumbnails;
                    }
                }
            }
        }

        $this->set_updated(true);
    }

    public function is_updated() {
        return $this->_updated;
    }

    public function set_updated($value = true) {
        $this->_updated = (bool) $value;
        return $this->_updated;
    }

    public function get_cache_name() {
        return $this->_cache_name;
    }

    public function get_cache_type() {
        return $this->_cache_type;
    }

    public function get_cache_location() {
        return $this->_cache_location;
    }

    protected function _set_cache_file_handle($handle) {
        return $this->_cache_file_handle = $handle;
    }

    protected function _get_cache_file_handle() {
        return $this->_cache_file_handle;
    }

    public function get_last_check_for_update() {
        return $this->_last_check_for_update;
    }

    public function set_last_check_for_update($time = 'now') {
        $this->_last_check_for_update = ($time === 'now') ? time() : $time;
        $this->set_updated();
        return $this->_last_check_for_update;
    }

    public function get_last_check_token() {
        return $this->_last_check_token;
    }

    public function set_last_check_token($token) {
        $this->_last_check_token = $token;
        return $this->_last_check_token;
    }

    public function get_max_entry_age() {
        return $this->_max_entry_age;
    }

    public function set_max_entry_age($value) {
        return $this->_max_entry_age = $value;
    }

    public function get_max_change_age() {
        return $this->_max_change_age;
    }

    public function set_max_change_age($value) {
        return $this->_max_change_age = $value;
    }

    public function __destruct() {
        $this->update_cache();
    }

    private function _serialize_cache() {
        $data = array(
            '_nodes' => $this->_nodes,
            '_last_check_token' => $this->_last_check_token,
            '_last_check_for_update' => $this->_last_check_for_update
        );

        $data_str = serialize($data);

        if (function_exists('gzencode')) {
            $data_str = gzencode($data_str);
        }

        return $data_str;
    }

    private function _unserialize_cache($data) {
        $values = unserialize($data);
        if ($values !== false) {
            foreach ($values as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor 
     */
    public function get_processor() {
        return $this->_processor;
    }

}
