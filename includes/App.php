<?php

namespace TheLion\ShareoneDrive;

class App {

    /**
     * 
     * @var string 
     */
    private $_account_type = '';

    /**
     *
     * @var bool 
     */
    private $_own_app = false;

    /**
     *
     * @var string 
     */
    private $_app_key = '655f6a98-e3b6-490f-8ebd-e1a6714471d4';

    /**
     *
     * @var string 
     */
    private $_app_secret = 'egj7GeOds1z7a8G8CGXXmek';

    /**
     *
     * @var string 
     */
    private $_identifier;

    /**
     * 
     * @var \SODOneDrive_Service_Drive
     */
    private $_onedrive_drive_service;

    /**
     * 
     * @var \SODOneDrive_Service_User
     */
    private $_onedrive_user_service;

    /**
     * 
     * @var \SODOneDrive_Client
     */
    private $_client;

    /**
     * We don't save your data or share it. 
     * This script just simply creates a redirect with your id and secret to OneDrive and returns the created token.
     * It is exactly the same script as the _authorizeApp.php file in the includes folder of the plugin, 
     * and is used for an easy and one-click authorization process that will always work!
     * @var string 
     */
    private $_auth_url = 'https://www.wpcloudplugins.com/share-one-drive/index.php';

    /**
     *
     * @var string
     */
    private $_redirect_uri;

    /**
     *
     * @var boolean 
     */
    private $_log_api_request = false;

    /**
     * Contains the location to the token file
     * @var string 
     */
    private $_token_location;

    /**
     * Contains the file handle for the token file
     * @var type 
     */
    private $_token_file_handle = null;

    /**
     *
     * @var \TheLion\ShareoneDrive\Processor 
     */
    private $_processor;

    public function __construct(Processor $processor) {
        $this->_processor = $processor;
        $this->_token_location = SHAREONEDRIVE_CACHEDIR . '/' . get_current_blog_id() . '.access_token';

        /* Call back for refresh token function in SDK client */
        add_action('share-one-drive-refresh-token', array(&$this, 'start_client'));

        if (!function_exists('onedrive_api_php_client_autoload')) {
            require_once "API/autoload.php";
        }

        $own_key = $this->get_processor()->get_setting('onedrive_app_client_id');
        $own_secret = $this->get_processor()->get_setting('onedrive_app_client_secret');

        if (
                (!empty($own_key)) &&
                (!empty($own_secret))
        ) {
            $this->_app_key = $this->get_processor()->get_setting('onedrive_app_client_id');
            $this->_app_secret = $this->get_processor()->get_setting('onedrive_app_client_secret');
            $this->_own_app = true;
        }

        /* Remove old authorization (Used before version 1.3) */
        if (strpos($this->get_processor()->get_setting('onedrive_app_current_token'), 'wl.skydrive') !== false) {
            $this->revoke_token();
        }

        /* Set right redirect URL */
        $this->set_redirect_uri();

        /* Process codes/tokens if needed */
        $this->process_authorization();
    }

    public function process_authorization() {
        /* CHECK IF THIS PLUGIN IS DOING THE AUTHORIZATION */
        if (!isset($_REQUEST['action'])) {
            return false;
        }

        if ($_REQUEST['action'] !== 'shareonedrive_authorization') {
            return false;
        }

        $this->get_processor()->reset_complete_cache();

        if (isset($_GET['code'])) {
            $access_token = $this->create_access_token();
            /** Echo To Popup */
            echo '<script type="text/javascript">window.opener.parent.location.href = "' . $this->get_redirect_uri() . '"; window.close();</script>';
            die();
        } elseif (isset($_GET['_token'])) {
            $new_access_token = $_GET['_token'];
            $access_token = $this->set_access_token($new_access_token);

            /** Echo To Popup */
            echo '<script type="text/javascript">window.opener.parent.location.href = "' . $this->get_redirect_uri() . '"; window.close();</script>';
            die();
        }



        return false;
    }

    public function can_do_own_auth() {
        return true;
    }

    public function has_plugin_own_app() {
        return $this->_own_app;
    }

    public function get_auth_url() {

        return $this->get_client()->createAuthUrl();
        return $this->_auth_url;
    }

    /**
     * 
     * @return \SODOneDrive_Client
     */
    public function start_client() {

        try {
            $this->_client = new \SODOneDrive_Client();
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Cannot start OneDrive Client %s', $ex->getMessage()));
            return $ex;
        }

        $this->_client->setClientId($this->get_app_key());
        $this->_client->setClientSecret($this->get_app_secret());

        $this->_client->setRedirectUri($this->_auth_url);

        $this->_client->setApprovalPrompt('none');
        $this->_client->setAccessType('offline');

        $this->_client->setScopes(array(
            'offline_access',
            'files.readwrite',
            'user.read'));

        $state = $this->get_redirect_uri() . '&action=shareonedrive_authorization';
        $this->_client->setState(strtr(base64_encode($state), '+/=', '-_~'));

        $this->set_logger();

        if ($this->has_access_token() === false) {
            return $this->_client;
        }

        $access_token = $this->get_access_token();

        if (empty($access_token)) {
            $this->_unlock_token_file();
            return $this->_client;
        }

        if (!empty($access_token)) {

            $this->_client->setAccessToken($access_token);

            /* Check if the AccessToken is still valid * */
            if ($this->_client->isAccessTokenExpired() === false) {
                $this->_unlock_token_file();
                return $this->_client;
            }
        }


        if (!flock($this->_get_token_file_handle(), LOCK_EX | LOCK_NB)) {
            error_log('[Share-one-Drive message]: ' . sprintf('Wait till another process has renewed the Authorization Token'));

            /*
             * If the file cannot be unlocked and the last time
             * it was modified was 1 minute, assume that 
             * the previous process died and unlock the file manually
             */
            $requires_unlock = ((filemtime($this->get_token_location()) + 60) < (time()));
            if ($requires_unlock) {
                $this->_unlock_token_file();
            }

            if (flock($this->_get_token_file_handle(), LOCK_SH)) {
                clearstatcache();
                rewind($this->_get_token_file_handle());
                $token = fread($this->_get_token_file_handle(), filesize($this->get_token_location()));
                error_log('[Share-one-Drive message]: ' . sprintf('New Authorization Token has been received by another process.'));
                $this->_client->setAccessToken($access_token);
                $this->_unlock_token_file();
                return $this->_client;
            }
        }

        //error_log('[Share-one-Drive message]: ' . sprintf('Start renewing the Authorization Token'));

        /* Stop if we need to get a new AccessToken but somehow ended up without a refreshtoken */
        $refresh_token = $this->_client->getRefreshToken();

        if (empty($refresh_token)) {
            error_log('[Share-one-Drive message]: ' . sprintf('No Refresh Token found during the renewing of the current token. We will stop the authorization completely.'));

            define('SHAREONEDRIVE_NOAUTHORIZATION', true);

            $this->_unlock_token_file();
            $this->revoke_token();
            return false;
        }


        /* Refresh token */
        try {
            $this->_client->refreshToken($refresh_token);

            /* Store the new token */
            $new_accestoken = $this->_client->getAccessToken();
            $this->set_access_token($new_accestoken);

            $this->_unlock_token_file();
            //error_log('[Share-one-Drive message]: ' . sprintf('Received new Authorization Token'));

            if (($timestamp = wp_next_scheduled('shareonedrive_lost_authorisation_notification')) !== false) {
                wp_unschedule_event($timestamp, 'shareonedrive_lost_authorisation_notification');
            }
        } catch (\Exception $ex) {
            $this->_unlock_token_file();
            error_log('[Share-one-Drive message]: ' . sprintf('Cannot refresh Authorization Token: %s', $ex->getMessage()));

            if (!wp_next_scheduled('shareonedrive_lost_authorisation_notification')) {
                wp_schedule_event(time(), 'daily', 'shareonedrive_lost_authorisation_notification');
            }

            define('SHAREONEDRIVE_NOAUTHORIZATION', true);

            throw $ex;
        }

        return $this->_client;
    }

    public function set_approval_prompt($approval_prompt = 'none') {
        $this->get_client()->setApprovalPrompt($approval_prompt);
    }

    public function set_logger() {

        if ($this->_log_api_request) {
            /* Logger */
            $this->get_client()->setClassConfig('SODOneDrive_Logger_File', array(
                'file' => SHAREONEDRIVE_CACHEDIR . '/api.log',
                'mode' => 0640,
                'lock' => true));

            $this->get_client()->setClassConfig('SODOneDrive_Logger_Abstract', array(
                'level' => 'debug', //'warning' or 'debug'
                'log_format' => "[%datetime%] %level%: %message% %context%\n",
                'date_format' => 'd/M/Y:H:i:s O',
                'allow_newlines' => true));

            /* Uncomment the following line to log communcations.
             * The log is located in /cache/log
             */

            $this->get_client()->setLogger(new \SODOneDrive_Logger_File($this->get_client()));
        }
    }

    public function create_access_token() {

        $this->get_processor()->reset_complete_cache();


        try {
            $code = $_REQUEST['code'];
            $state = $_REQUEST['state'];

            //Fetch the AccessToken
            $this->get_client()->authenticate($code);
            $access_token = $this->get_client()->getAccessToken();
            $this->set_access_token($access_token);

            $drive = $this->get_drive()->about->get();
            $this->set_account_type($drive->getDriveType());
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Cannot generate Access Token: %s', $ex->getMessage()));
            return new \WP_Error('broke', __("error communicating with OneDrive API: ", 'shareonedrive') . $ex->getMessage());
        }

        return true;
    }

    public function revoke_token() {
        try {
            /* No Endpoint in the Graph API to revoke tokens */
            /* $this->get_client()->revokeToken(); */
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . $ex->getMessage());
        }

        error_log('[Share-one-Drive message]: ' . 'Lost authorization');

        unlink($this->get_token_location());

        $this->get_processor()->set_setting('userfolder_backend_auto_root', null);
        $this->set_account_type('');

        $this->get_processor()->reset_complete_cache();

        if (($timestamp = wp_next_scheduled('shareonedrive_lost_authorisation_notification')) !== false) {
            wp_unschedule_event($timestamp, 'shareonedrive_lost_authorisation_notification');
        }

        $this->get_processor()->get_main()->send_lost_authorisation_notification();

        return true;
    }

    public function get_app_key() {
        return $this->_app_key;
    }

    public function get_app_secret() {
        return $this->_app_secret;
    }

    public function set_app_key($_app_key) {
        $this->_app_key = $_app_key;
    }

    public function set_app_secret($_app_secret) {
        $this->_app_secret = $_app_secret;
    }

    public function get_access_token() {
        $this->_get_lock();
        clearstatcache();
        rewind($this->_get_token_file_handle());

        $filesize = filesize($this->get_token_location());
        if ($filesize > 0) {
            $token = fread($this->_get_token_file_handle(), filesize($this->get_token_location()));
        } else {
            $token = '';
        }

        $this->_unlock_token_file();
        if (empty($token)) {
            return null;
        }

        return $token;
    }

    public function set_access_token($_access_token) {
        ftruncate($this->_get_token_file_handle(), 0);
        rewind($this->_get_token_file_handle());

        return fwrite($this->_get_token_file_handle(), $_access_token);
    }

    public function has_access_token() {
        if (defined('SHAREONEDRIVE_NOAUTHORIZATION')) {
            return false;
        }

        $access_token = $this->get_access_token();
        return (!empty($access_token));
    }

    public function _get_lock($type = LOCK_SH) {

        if (!flock($this->_get_token_file_handle(), $type)) {
            /*
             * If the file cannot be unlocked and the last time
             * it was modified was 1 minute, assume that 
             * the previous process died and unlock the file manually
             */
            $requires_unlock = ((filemtime($this->get_token_location()) + 60) < (time()));
            if ($requires_unlock) {
                $this->_unlock_token_file();
            }
            /* Try to lock the file again */
            flock($this->_get_token_file_handle(), $type);
        }

        return $this->_get_token_file_handle();
    }

    protected function _unlock_token_file() {
        $handle = $this->_get_token_file_handle();
        if (!empty($handle)) {
            flock($this->_get_token_file_handle(), LOCK_UN);
            fclose($this->_get_token_file_handle());
            $this->_set_token_file_handle(null);
        }

        clearstatcache();
        return true;
    }

    public function get_token_location() {
        return $this->_token_location;
    }

    protected function _set_token_file_handle($handle) {
        return $this->_token_file_handle = $handle;
    }

    protected function _get_token_file_handle() {

        if (empty($this->_token_file_handle)) {

            /* Check if cache dir is writeable */
            /* Moving from DB storage to file storage */
            if (!file_exists($this->get_token_location())) {
                $token = $this->get_processor()->get_setting('onedrive_app_current_token');
                $this->get_processor()->set_setting('onedrive_app_current_token', null);
                $this->get_processor()->set_setting('onedrive_app_refresh_token', null);
                file_put_contents($this->get_token_location(), $token);
            }

            if (!is_writable($this->get_token_location())) {
                @chmod($this->get_token_location(), 0755);

                if (!is_writable($this->get_token_location())) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Cache file (%s) is not writable', $this->get_token_location()));
                    die(sprintf('Cache file (%s) is not writable', $this->get_token_location()));
                }

                file_put_contents($this->get_token_location(), $token);
            }


            $this->_token_file_handle = fopen($this->get_token_location(), 'c+');
            if (!is_resource($this->_token_file_handle)) {
                error_log('[Share-one-Drive message]: ' . sprintf('Cache file (%s) is not writable', $this->get_token_location()));
                die(sprintf('Cache file (%s) is not writable', $this->get_token_location()));
            }
        }

        return $this->_token_file_handle;
    }

    public function get_account_type() {
        if ($this->_account_type === '') {
            $this->_account_type = $this->get_processor()->get_setting('onedrive_account_type');
        }
        return $this->_account_type;
    }

    public function set_account_type($_account_type) {
        $this->_account_type = $_account_type;
        return $this->get_processor()->set_setting('onedrive_account_type', $_account_type);
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor() {
        return $this->_processor;
    }

    /**
     * 
     * @return \SODOneDrive_Client
     */
    public function get_client() {

        if (empty($this->_client)) {
            $this->_client = $this->start_client();
        }

        return $this->_client;
    }

    /**
     * 
     * @return \SODOneDrive_Service_Drive
     */
    public function get_drive() {
        if (empty($this->_onedrive_drive_service)) {
            $client = $this->get_client();
            $this->_onedrive_drive_service = new \SODOneDrive_Service_Drive($client);
        }
        return $this->_onedrive_drive_service;
    }

    /**
     * 
     * @return \SODOneDrive_Service_User
     */
    public function get_user() {
        if (empty($this->_onedrive_user_service)) {
            $client = $this->get_client();
            $this->_onedrive_user_service = new \SODOneDrive_Service_User($client);
        }
        return $this->_onedrive_user_service;
    }

    public function get_redirect_uri() {
        return $this->_redirect_uri;
    }

    public function set_redirect_uri() {
        $this->_redirect_uri = admin_url('admin.php?page=ShareoneDrive_settings');
    }

}
