<?php
if (class_exists("GFForms")) {
    GFForms::include_addon_framework();

    class GFShareoneDriveAddOn extends GFAddOn {

        protected $_version = "1.0";
        protected $_min_gravityforms_version = "1.9";
        protected $_slug = "shareonedriveaddon";
        protected $_path = "share-one-drive/includes/ShareoneDrive_GravityForms.php";
        protected $_full_path = __FILE__;
        protected $_title = "Gravity Forms Share-one-Drive Add-On";
        protected $_short_title = "Share-one-Drive Add-On";

        public function init() {
            parent::init();

            if (isset($this->_min_gravityforms_version) && !$this->is_gravityforms_supported($this->_min_gravityforms_version)) {
                return;
            }

            /* Add a Share-one-Drive button to the advanced to the field editor */
            add_filter('gform_add_field_buttons', array($this, "shareonedrive_field"));
            add_filter('admin_enqueue_scripts', array($this, "shareonedrive_extra_scripts"));

            /* Now we execute some javascript technicalitites for the field to load correctly */
            add_action("gform_editor_js", array($this, "gform_editor_js"));
            add_filter('gform_field_input', array($this, "shareonedrive_input"), 10, 5);

            /* Add a custom setting to the field */
            add_action("gform_field_standard_settings", array($this, "shareonedrive_settings"), 10, 2);

            /* Adds title to the custom field */
            add_filter('gform_field_type_title', array($this, 'shareonedrive_title'), 10, 2);

            /* Filter to add the tooltip for the field */
            add_filter('gform_tooltips', array($this, 'add_shareonedrive_tooltips'));

            /* Save some data for this field */
            add_filter('gform_field_validation', array($this, 'shareonedrive_validation'), 10, 4);

            /* Display values in a proper way */
            add_filter('gform_entry_field_value', array($this, 'shareonedrive_entry_field_value'), 10, 4);
            add_filter('gform_entries_field_value', array($this, 'shareonedrive_entries_field_value'), 10, 4);
            add_filter('gform_merge_tag_filter', array($this, 'shareonedrive_merge_tag_filter'), 10, 5);
        }

        public function shareonedrive_extra_scripts() {
            if (RGForms::is_gravity_page()) {
                add_thickbox();
            }
        }

        public function shareonedrive_field($field_groups) {
            foreach ($field_groups as &$group) {
                if ($group["name"] == "advanced_fields") {
                    $group["fields"][] = array(
                        'class' => 'button',
                        'value' => 'Share-one-Drive',
                        'date-type' => 'shareonedrive',
                        'onclick' => "StartAddField('shareonedrive');"
                    );
                    break;
                }
            }
            return $field_groups;
        }

        public function gform_editor_js() {
            ?>
            <script type='text/javascript'>
                jQuery(document).ready(function ($) {
                  /* Which settings field should be visible for our custom field*/
                  fieldSettings["shareonedrive"] = ".label_setting, .description_setting, .admin_label_setting, .error_message_setting, .css_class_setting, .visibility_setting, .rules_setting, .label_placement_setting, .shareonedrive_setting, .conditional_logic_field_setting, .conditional_logic_page_setting, .conditional_logic_nextbutton_setting"; //this will show all the fields of the Paragraph Text field minus a couple that I didn't want to appear.

                  /* binding to the load field settings event to initialize */
                  $(document).bind("gform_load_field_settings", function (event, field, form) {
                    if (field["ShareoneDriveShortcode"] !== undefined) {
                      jQuery("#field_shareonedrive").val(field["ShareoneDriveShortcode"]);
                    } else {
                      /* Default value */
                      var defaultvalue = '[shareonedrive mode="upload" upload="1" uploadrole="all" userfolders="auto"]';
                      jQuery("#field_shareonedrive").val(defaultvalue);
                    }
                  });

                  /* Shortcode Generator Popup */
                  $('.ShareoneDrive-GF-shortcodegenerator').click(function () {
                    var shortcode = jQuery("#field_shareonedrive").val();
                    shortcode = shortcode.replace('[shareonedrive ', '').replace('"]', '');
                    var query = encodeURIComponent(shortcode).split('%3D%22').join('=').split('%22%20').join('&');
                    tb_show("Build Shortcode for Form", ajaxurl + '?action=shareonedrive-getpopup&' + query + '&type=gravityforms&TB_iframe=true&height=600&width=800');
                  });
                });

                function SetDefaultValues_shareonedrive(field) {
                  field.label = '<?php _e('Upload your Files', 'shareonedrive'); ?>';
                }
            </script>
            <?php
        }

        public function shareonedrive_input($input, $field, $value, $lead_id, $form_id) {
            if ($field->type == "shareonedrive") {
                if (!$this->is_form_editor()) {
                    $return = do_shortcode($field->ShareoneDriveShortcode);
                    $return .= "<input type='hidden' name='input_" . $field->id . "' id='input_" . $form_id . "_" . $field->id . "'  class='fileupload-filelist' value='" . (isset($_REQUEST['input_' . $field->id]) ? stripslashes($_REQUEST['input_' . $field->id]) : '') . "'>";
                    return $return;
                } else {
                    $style = 'background: #176cff url(' . SHAREONEDRIVE_ROOTPATH . '/css/images/shortcode_image.png) no-repeat center center; height: 150px;  width: 99%;  border: 1px solid #aaa;  outline: 0;  cursor: pointer;';
                    return '<div style="' . $style . '"></div>';
                }
            }
            return $input;
        }

        public function shareonedrive_settings($position, $form_id) {
            if ($position == 1430) {
                ?>
                <li class="shareonedrive_setting field_setting">
                  <label for="field_shareonedrive">Share-one-Drive Shortcode <?php echo gform_tooltip("form_field_shareonedrive"); ?></label>
                  <a href="#" class='button-primary ShareoneDrive-GF-shortcodegenerator '><?php _e('Build your Share-one-Drive shortcode', 'shareonedrive'); ?></a>
                  <textarea id="field_shareonedrive" class="fieldwidth-3 fieldheight-2" onchange="SetFieldProperty('ShareoneDriveShortcode', this.value)"></textarea>
                  <br/><small>Missing a Share-one-Drive Gravity Form feature? Please let me <a href="https://florisdeleeuwnl.zendesk.com/hc/en-us/requests/new" target="_blank">know</a>!</small>
                </li>
                <?php
            }
        }

        function shareonedrive_title($title, $field_type) {
            if ($field_type === 'shareonedrive') {
                return __('Share-one-Drive Upload', 'shareonedrive');
            }
            return $title;
        }

        public function add_shareonedrive_tooltips($tooltips) {
            $tooltips["form_field_shareonedrive"] = "<h6>Share-one-Drive Shortcode</h6>" . __('Build here your Share-one-Drive shortcode', 'shareonedrive');
            return $tooltips;
        }

        public function shareonedrive_validation($result, $value, $form, $field) {
            if ($field->type !== 'shareonedrive') {
                return $result;
            }

            if ($field->isRequired === false) {
                return $result;
            }

            /* Get information uploaded files from hidden input */
            $filesinput = rgpost('input_' . $field->id);
            $uploadedfiles = json_decode($filesinput);

            if (($uploadedfiles === NULL) || (count($uploadedfiles) === 0)) {
                $result['is_valid'] = false;
                $result['message'] = __('This field is required. Please upload your files.', 'gravityforms');
            } else {
                $result['is_valid'] = true;
                $result['message'] = '';
            }

            return $result;
        }

        public function shareonedrive_entry_field_value($value, $field, $lead, $form) {
            if ($field->type !== "shareonedrive") {
                return $value;
            }

            return $this->renderUploadedFiles(html_entity_decode($value));
        }

        public function shareonedrive_entries_field_value($value, $form_id, $field_id, $entry) {
            $form = RGFormsModel::get_form_meta($form_id);

            if (is_array($form["fields"])) {
                foreach ($form["fields"] as $field) {
                    if ($field->type === 'shareonedrive' && $field_id == $field->id) {
                        return $this->renderUploadedFiles(html_entity_decode($value));
                    }
                }
            }

            return $value;
        }

        public function shareonedrive_set_export_values($value, $form_id, $field_id, $lead) {
            $form = RGFormsModel::get_form_meta($form_id);

            if (is_array($form["fields"])) {
                foreach ($form["fields"] as $field) {
                    if ($field->type === 'shareonedrive' && $field_id == $field->id) {
                        return $this->renderUploadedFiles(html_entity_decode($value), false);
                    }
                }
            }

            return $value;
        }

        public function shareonedrive_merge_tag_filter($value, $merge_tag, $modifier, $field, $rawvalue) {

            if ($field->type == 'shareonedrive') {
                return $this->renderUploadedFiles(html_entity_decode($value));
            } else {
                return $value;
            }
        }

        public function renderUploadedFiles($data, $ashtml = true) {

            $uploadedfiles = json_decode($data);

            if (($uploadedfiles !== NULL) && (count((array) $uploadedfiles) > 0)) {
                /* Fill our custom field with the details of our upload session */
                $html = sprintf(__('%d file(s) uploaded:', 'shareonedrive'), count((array) $uploadedfiles));
                $html .= ($ashtml) ? '<ul>' : "\r\n";

                foreach ($uploadedfiles as $fileid => $file) {
                    $html .= ($ashtml) ? '<li><a href="' . urldecode($file->link) . '">' : "";
                    $html .= $file->path;
                    $html .= ($ashtml) ? '</a>' : "";
                    $html .= ' (' . $file->size . ')';
                    $html .= ($ashtml) ? '</li>' : "\r\n";
                }

                $html .= ($ashtml) ? '</ul>' : "";
            } else {
                return $data;
            }

            return $html;
        }

    }

    $GFShareoneDriveAddOn = new GFShareoneDriveAddOn();
    /* This filter isn't fired if inside class */
    add_filter('gform_export_field_value', array($GFShareoneDriveAddOn, 'shareonedrive_set_export_values'), 10, 4);
}