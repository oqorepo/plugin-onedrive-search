<?php

namespace TheLion\ShareoneDrive;

class UserFolders {

    /**
     *
     * @var \TheLion\ShareoneDrive\Client 
     */
    private $_client;

    /**
     *
     * @var \TheLion\ShareoneDrive\Processor 
     */
    private $_processor;

    /**
     *
     * @var string 
     */
    private $_user_name_template;

    /**
     *
     * @var string 
     */
    private $_user_folder_name;

    /**
     *
     * @var  \TheLion\ShareoneDrive\Entry
     */
    private $_user_folder_entry;

    public function __construct(\TheLion\ShareoneDrive\Processor $_processor = null) {
        $this->_client = $_processor->get_client();
        $this->_processor = $_processor;
        $this->_user_name_template = $this->get_processor()->get_setting('userfolder_name');

        $shortcode = $this->get_processor()->get_shortcode();
        if (!empty($shortcode) && !empty($shortcode['user_folder_name_template'])) {
            $this->_user_name_template = $shortcode['user_folder_name_template'];
        }
    }

    public function get_auto_linked_folder_name_for_user() {
        $shortcode = $this->get_processor()->get_shortcode();
        if (!isset($shortcode['user_upload_folders']) || $shortcode['user_upload_folders'] !== 'auto') {
            return false;
        }

        if (!empty($this->_user_folder_name)) {
            return $this->_user_folder_name;
        }

        if (is_user_logged_in()) {
            $current_user = wp_get_current_user();
            $userfoldername = $this->get_user_name_template($current_user);
        } else {
            $userfoldername = $this->get_guest_user_name();
        }

        $this->_user_folder_name = $userfoldername;

        return $userfoldername;
    }

    public function get_auto_linked_folder_for_user() {
        $shortcode = $this->get_processor()->get_shortcode();
        if (!isset($shortcode['user_upload_folders']) || $shortcode['user_upload_folders'] !== 'auto') {
            return false;
        }

        if (!empty($this->_user_folder_entry)) {
            return $this->_user_folder_entry;
        }

        /* Add folder if needed */
        $result = $this->create_user_folder($this->get_auto_linked_folder_name_for_user(), $this->get_processor()->get_shortcode(), 0);

        do_action('shareonedrive_after_private_folder_added', $result, $this->_processor);

        if ($result === false) {
            error_log('[Share-one-Drive message]: ' . 'Cannot find auto folder link for user');
            die();
        }

        $this->_user_folder_entry = $result;

        return $this->_user_folder_entry;
    }

    public function get_manually_linked_folder_for_user() {
        $shortcode = $this->get_processor()->get_shortcode();
        if (!isset($shortcode['user_upload_folders']) || $shortcode['user_upload_folders'] !== 'manual') {
            return false;
        }

        if (!empty($this->_user_folder_entry)) {
            return $this->_user_folder_entry;
        }

        $userfolder = get_user_option('share_one_drive_linkedto');
        if (is_array($userfolder) && isset($userfolder['foldertext'])) {
            $this->_user_folder_entry = $this->get_client()->get_entry($userfolder['folderid'], false);
        } else {
            $defaultuserfolder = get_site_option('share_one_drive_guestlinkedto');
            if (is_array($defaultuserfolder) && isset($defaultuserfolder['folderid'])) {
                $this->_user_folder_entry = $this->get_client()->get_entry($defaultuserfolder['folderid'], false);
            } else {

                if (is_user_logged_in()) {
                    $current_user = wp_get_current_user();
                    error_log('[Share-one-Drive message]: ' . sprintf('Cannot find manual folder link for user: %s', $current_user->user_login));
                } else {
                    error_log('[Share-one-Drive message]: ' . 'Cannot find manual folder link for guest user');
                }

                die(-1);
            }
        }

        return $this->_user_folder_entry;
    }

    public function manually_link_folder($user_id, $linkedto) {

        if ($user_id === 'GUEST') {
            $result = update_site_option('share_one_drive_guestlinkedto', $linkedto);
        } else {
            $result = update_user_option($user_id, 'share_one_drive_linkedto', $linkedto, false);
        }

        if ($result !== false) {
            die('1');
        }
    }

    public function manually_unlink_folder($user_id) {

        if ($user_id === 'GUEST') {
            $result = delete_site_option('share_one_drive_guestlinkedto');
        } else {
            $result = delete_user_option($user_id, 'share_one_drive_linkedto', false);
        }

        if ($result !== false) {
            die('1');
        }
    }

    public function create_user_folder($userfoldername, $shortcode, $mswaitaftercreation = 0) {
        @set_time_limit(60);

        $parent_folder = $this->get_client()->get_folder($shortcode['root'], false);

        /* If root folder doesn't exists */
        if (empty($parent_folder)) {
            return false;
        }

        /* First try to find the User Folder in Cache */
        $userfolder = $this->get_client()->get_cache()->get_node_by_name($userfoldername, $parent_folder['folder']);

        /* If User Folder isn't in cache yet,
         * Update the parent folder to make sure the latest version is loaded */
        if ($userfolder === false) {
            $this->get_client()->get_cache()->pull_for_changes(true);
            $userfolder = $this->get_client()->get_cache()->get_node_by_name($userfoldername, $parent_folder['folder']);
        }

        /* If User Folder still isn't found, create new folder in the Cloud */
        if ($userfolder === false) {

            if (empty($shortcode['user_template_dir'])) {

                $newfolder = new \SODOneDrive_Service_Drive_Item();
                $newfolder->setName($userfoldername);
                $newfolder->setFolder(new \SODOneDrive_Service_Drive_FolderFacet());
                $newfolder["@name.conflictBehavior"] = "rename";

                try {
                    $api_entry = $this->get_app()->get_drive()->items->insert($parent_folder['folder']->get_id(), $newfolder, array("expand" => $this->get_client()->apifilefields));

                    /* Wait a moment in case many folders are created at once */
                    usleep($mswaitaftercreation);
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to add user folder: %s', $ex->getMessage()));

                    return new \WP_Error('broke', __('Failed to add user folder', 'shareonedrive'));
                }

                /* Add new file to our Cache */
                $newentry = new Entry($api_entry);
                $userfolder = $this->get_client()->get_cache()->add_to_cache($newentry);

                do_action('shareonedrive_log_event', 'shareonedrive_created_entry', $userfolder);
            } else {

                /* 3: Get the Template folder */
                $cached_template_folder = $this->get_client()->get_folder($shortcode['user_template_dir'], false);

                /* 4: Make sure that the Template folder can be used */
                if ($cached_template_folder === false || $cached_template_folder['folder'] === false || $cached_template_folder['folder']->has_children() === false) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to add user folder as the template folder does not exist: %s', $ex->getMessage()));
                    return new \WP_Error('broke', __('Failed to add user folder', 'shareonedrive'));
                }

                /* Copy the contents of the Template Folder into the User Folder */
                /* Set new parent for entry */
                $newParent = new \SODOneDrive_Service_Drive_ItemReference();
                $newParent->setId($parent_folder['folder']->get_id());
                $updaterequest = new \SODOneDrive_Service_Drive_Item();
                $updaterequest->setParentReference($newParent);
                $updaterequest->setName($userfoldername);


                try {
                    $monitor_url = $this->get_app()->get_drive()->items->copy($cached_template_folder['folder']->get_id(), $updaterequest);

                    /* Monitor Copy process in case it is async and the response does contain an url we can check for updates.
                     * Only wait for it if the current user is loading its own folder (i.e. $mswaitaftercreation = 0).
                     *  */
                    if (!empty($monitor_url)) {
                        $wait_for_copy_finished = ($mswaitaftercreation === 0 ) ? true : false;

                        while ($wait_for_copy_finished) {
                            $request = new \SODOneDrive_Http_Request($monitor_url);
                            $httpRequest = $this->get_app()->get_client()->getAuth()->authenticatedRequest($request);
                            $response = json_decode($httpRequest->getResponseBody(), true);
                            $wait_for_copy_finished = (in_array($response['status'], array('notStarted', 'inProgress', 'updating', 'waiting')));
                        }
                    }

                    if ($mswaitaftercreation === 0) {
                        $this->get_client()->get_cache()->set_last_check_for_update(null);
                        $this->get_client()->get_cache()->pull_for_changes(true);
                    }
                    $userfolder = $this->get_client()->get_cache()->get_node_by_name($userfoldername, $parent_folder['folder']);

                    do_action('shareonedrive_log_event', 'shareonedrive_created_entry', $userfolder);
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to add user folder with template folder: %s', $ex->getMessage()));
                    return new \WP_Error('broke', __('Failed to add user folder with template folder', 'shareonedrive'));
                }
            }
        }

        return $userfolder;
    }

    public function create_user_folders_for_shortcodes($user_id) {
        $new_user = get_user_by('id', $user_id);
        $new_userfoldersname = $this->get_user_name_template($new_user);

        $shareonedrivelists = get_option('share_one_drive_lists', array());

        foreach ($shareonedrivelists as $list) {

            if (!isset($list['user_upload_folders']) || $list['user_upload_folders'] !== 'auto') {
                continue;
            }

            $result = $this->create_user_folder($new_userfoldersname, $list);

            do_action('shareonedrive_after_private_folder_added', $result, $this->_processor);
        }
    }

    public function create_user_folders($users = array()) {

        if (count($users) === 0) {
            return;
        }

        foreach ($users as $user) {
            $userfoldersname = $this->get_user_name_template($user);
            $result = $this->create_user_folder($userfoldersname, $this->get_processor()->get_shortcode(), 50);

            do_action('shareonedrive_after_private_folder_added', $result, $this->_processor);
        }

        $this->get_client()->get_cache()->pull_for_changes(true);
    }

    public function remove_user_folder($user_id) {

        $deleted_user = get_user_by('id', $user_id);
        $userfoldername = $this->get_user_name_template($deleted_user);

        $shareonedrivelists = get_option('share_one_drive_lists', array());
        $update_folders = array();

        foreach ($shareonedrivelists as $list) {

            if (!isset($list['user_upload_folders']) || $list['user_upload_folders'] !== 'auto') {
                continue;
            }

            /* Skip shortcode if folder is already updated in an earlier call */
            if (isset($update_folders[$list['root']])) {
                continue;
            }

            /* 2: try to find the User Folder in Cache */
            $userfolder = $this->get_client()->get_cache()->get_node_by_name($userfoldername, $list['root']);
            if (!empty($userfolder)) {
                try {
                    $headers = array();
                    $this->get_app()->get_drive()->items->delete($userfolder->getId(), $headers);
                    $update_folders[$list['root']] = true;
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to remove user folder: %s', $ex->getMessage()));
                    continue;
                }
            } else {
                /* Find all items containing query */
                $params = array('filter' => "startswith(name,'$userfoldername')");

                try {
                    $api_list = $this->get_app()->get_drive()->items->children($list['root'], $params);
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to remove user folder: %s', $ex->getMessage()));
                    return false;
                }

                $api_files = $api_list->getValue();

                /* Stop when no User Folders are found */
                if (count($api_files) === 0) {
                    $update_folders[$list['root']] = true;
                    continue;
                }

                /* Delete all the user folders that are found */
                foreach ($api_files as $api_file) {
                    try {
                        $headers = array();
                        $this->get_app()->get_drive()->items->delete($api_file->getId(), $headers);
                    } catch (\Exception $ex) {
                        error_log('[Share-one-Drive message]: ' . sprintf('Failed to remove user folder: %s', $ex->getMessage()));
                        continue;
                    }
                }
                $update_folders[$list['root']] = true;
            }
        }

        $this->get_client()->get_cache()->pull_for_changes(true);

        return true;
    }

    public function update_user_folder($user_id, $old_user) {


        $updated_user = get_user_by('id', $user_id);
        $new_userfoldersname = $this->get_user_name_template($updated_user);

        $old_userfoldersname = $this->get_user_name_template($old_user);

        if ($new_userfoldersname === $old_userfoldersname) {
            return false;
        }

        $shareonedrivelists = get_option('share_one_drive_lists', array());
        $update_folders = array();

        $this->get_client()->get_cache()->pull_for_changes(true);

        foreach ($shareonedrivelists as $list) {

            if (!isset($list['user_upload_folders']) || $list['user_upload_folders'] !== 'auto') {
                continue;
            }

            if (defined('share_one_drive_update_user_folder_' . $list['root'] . '_' . $new_userfoldersname)) {
                continue;
            }

            define('share_one_drive_update_user_folder_' . $list['root'] . '_' . $new_userfoldersname, true);

            /* 1: Create an the entry for Patch */
            $updaterequest = array('name' => $new_userfoldersname);

            /* 2: try to find the User Folder in Cache */
            $userfolder = $this->get_client()->get_cache()->get_node_by_name($old_userfoldersname, $list['root']);
            if (!empty($userfolder)) {

                try {
                    $api_entry = $this->get_app()->get_drive()->items->patch($userfolder->get_id(), $updaterequest);
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to update user folder: %s', $ex->getMessage()));
                    continue;
                }
            } else {

                /* Find all items containing query */
                $params = array('filter' => "startswith(name,'$old_userfoldersname')");

                try {
                    $api_list = $this->get_app()->get_drive()->items->children($list['root'], $params);
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Failed to update user folder: %s', $ex->getMessage()));
                    return false;
                }

                $api_files = $api_list->getValue();

                /* Stop when no User Folders are found */
                if (count($api_files) === 0) {
                    continue;
                }

                /* Delete all the user folders that are found */


                foreach ($api_files as $api_file) {
                    try {
                        $api_entry = $this->get_app()->get_drive()->items->patch($api_file->getId(), $updaterequest);
                    } catch (\Exception $ex) {
                        error_log('[Share-one-Drive message]: ' . sprintf('Failed to update user folder: %s', $ex->getMessage()));
                        continue;
                    }
                }
            }
        }

        $this->get_client()->get_cache()->pull_for_changes(true);

        return true;
    }

    public function get_user_name_template($user_data) {

        $user_folder_name = strtr($this->_user_name_template, array(
            "%user_login%" => isset($user_data->user_login) ? $user_data->user_login : '',
            "%user_email%" => isset($user_data->user_email) ? $user_data->user_email : '',
            "%user_firstname%" => isset($user_data->user_firstname) ? $user_data->user_firstname : '',
            "%user_lastname%" => isset($user_data->user_lastname) ? $user_data->user_lastname : '',
            "%display_name%" => isset($user_data->display_name) ? $user_data->display_name : '',
            "%ID%" => isset($user_data->ID) ? $user_data->ID : '',
            "%user_role%" => isset($user_data->roles) ? implode(',', $user_data->roles) : '',
            "%jjjj-mm-dd%" => date('Y-m-d')
        ));

        $user_folder_name = ltrim(Helpers::clean_folder_path($user_folder_name), '/');

        return apply_filters('shareonedrive_private_folder_name', $user_folder_name, $this->get_processor());
    }

    public function get_guest_user_name() {
        $username = $this->get_guest_id();

        $current_user = new \stdClass();
        $current_user->user_login = md5($username);
        $current_user->display_name = $username;
        $current_user->ID = $username;
        $current_user->user_role = __('Guest', 'shareonedrive');

        $user_folder_name = $this->get_user_name_template($current_user);

        return apply_filters('shareonedrive_private_folder_name_guests', __('Guests', 'shareonedrive') . ' - ' . $user_folder_name, $this->get_processor());
    }

    public function get_guest_id() {
        $id = uniqid();
        if (!isset($_COOKIE['SOD-ID'])) {
            $expire = time() + 60 * 60 * 24 * 7;
            @setcookie('SOD-ID', $id, $expire, COOKIEPATH, COOKIE_DOMAIN);
        } else {
            $id = $_COOKIE['SOD-ID'];
        }

        return $id;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor() {
        return $this->_processor;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\App
     */
    public function get_app() {
        return $this->get_processor()->get_app();
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Client
     */
    public function get_client() {
        return $this->get_processor()->get_client();
    }

}
