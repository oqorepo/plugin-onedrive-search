<?php

namespace TheLion\ShareoneDrive;

class Admin {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $settings_key = 'share_one_drive_settings';
    private $plugin_options_key = 'ShareoneDrive_settings';
    private $plugin_network_options_key = 'ShareoneDrive_network_settings';
    private $canconnect = true;
    private $plugin_id = 11453104;
    private $settingspage;
    private $filebrowserpage;
    private $shortcodebuilderpage;
    private $dashboardpage;
    private $userpage;

    /**
     * Construct the plugin object
     */
    public function __construct(\TheLion\ShareoneDrive\Main $main) {
        $this->_main = $main;

        /* Check if plugin can be used */
        if ($main->can_run_plugin() === false) {
            add_action('admin_notices', array(&$this, 'get_admin_notice'));
            return;
        }

        /* Init */
        add_action('init', array(&$this, 'load_settings'));
        add_action('admin_init', array(&$this, 'register_settings'));
        add_action('admin_init', array(&$this, 'check_for_updates'));
        add_action('admin_enqueue_scripts', array(&$this, 'load_admin'));

        /* add TinyMCE button */
        /* Depends on the theme were to load.... */
        add_action('init', array(&$this, 'load_shortcode_buttons'));
        add_action('admin_head', array(&$this, 'load_shortcode_buttons'));

        /* Add menu's */
        add_action('admin_menu', array(&$this, 'add_admin_menu'));
        add_action('network_admin_menu', array(&$this, 'add_admin_network_menu'));

        /* Network save settings call */
        add_action('network_admin_edit_' . $this->plugin_network_options_key, array($this, 'save_settings_network'));

        /* Save settings call */
        add_filter('pre_update_option_' . $this->settings_key, array($this, 'save_settings'), 10, 2);

        /* Notices */
        add_action('admin_notices', array(&$this, 'get_admin_notice_not_authorized'));

        add_filter('admin_footer_text', array($this, 'admin_footer_text'), 1);
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Main
     */
    public function get_main() {
        return $this->_main;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor() {
        return $this->_main->get_processor();
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\App
     */
    public function get_app() {
        return $this->get_processor()->get_app();
    }

    public function load_admin($hook) {

        if ($hook == $this->filebrowserpage || $hook == $this->userpage || $hook == $this->settingspage || $hook == $this->shortcodebuilderpage || $hook == $this->dashboardpage) {
            $this->get_main()->load_scripts();
            $this->get_main()->load_styles();

            wp_enqueue_script('jquery-effects-fade');
            wp_enqueue_script('WPCloudplugin.Libraries');

            wp_enqueue_style('qtip');
            wp_enqueue_style('ShareoneDrive-dialogs');
            wp_enqueue_style('ShareoneDrive.tinymce');
            wp_enqueue_style('Awesome-Font-5-css');
        }

        if ($hook == $this->settingspage) {
            wp_enqueue_script('jquery-form');
            wp_enqueue_script('ShareoneDrive.tinymce');
            wp_enqueue_script('wp-color-picker-alpha', plugins_url('/wp-color-picker-alpha/wp-color-picker-alpha.min.js', __FILE__), array('wp-color-picker'), '1.0.0', true);
            wp_enqueue_style('wp-color-picker');
            wp_enqueue_script('jquery-ui-accordion');
            wp_enqueue_media();
            add_thickbox();
        }

        if ($hook == $this->userpage) {
            wp_enqueue_style('ShareoneDrive');
            add_thickbox();
        }

        if ($hook == $this->dashboardpage) {
            wp_enqueue_script('ShareoneDrive.Dashboard');
            wp_enqueue_style('ShareoneDrive.Datatables.css');
            wp_dequeue_style('ShareoneDrive');
        }
    }

    /**
     * add a menu
     */
    public function add_admin_menu() {
        /* Add a page to manage this plugin's settings */
        $menuadded = false;

        if (Helpers::check_user_role($this->settings['permissions_edit_settings'])) {
            add_menu_page('Share-one-Drive', 'Share-one-Drive', 'read', $this->plugin_options_key, array(&$this, 'load_settings_page'), plugin_dir_url(__FILE__) . '../css/images/onedrive_logo_small.png');
            $menuadded = true;
            $this->settingspage = add_submenu_page($this->plugin_options_key, 'Share-one-Drive - ' . __('Settings'), __('Settings'), 'read', $this->plugin_options_key, array(&$this, 'load_settings_page'));
        }

        if (Helpers::check_user_role($this->settings['permissions_see_dashboard']) && ($this->settings['log_events'] === 'Yes')) {
            if (!$menuadded) {
                $this->dashboardpage = add_menu_page('Share-one-Drive', 'Share-one-Drive', 'read', $this->plugin_options_key, array(&$this, 'load_dashboard_page'), plugin_dir_url(__FILE__) . '../css/images/onedrive_logo_small.png');
                $this->dashboardpage = add_submenu_page($this->plugin_options_key, __('Reports', 'shareonedrive'), __('Reports', 'shareonedrive'), 'read', $this->plugin_options_key, array(&$this, 'load_dashboard_page'));
                $menuadded = true;
            } else {
                $this->dashboardpage = add_submenu_page($this->plugin_options_key, __('Reports', 'shareonedrive'), __('Reports', 'shareonedrive'), 'read', $this->plugin_options_key . '_dashboard', array(&$this, 'load_dashboard_page'));
            }
        }

        if (Helpers::check_user_role($this->settings['permissions_add_shortcodes'])) {
            if (!$menuadded) {
                $this->shortcodebuilderpage = add_menu_page('Share-one-Drive', 'Share-one-Drive', 'read', $this->plugin_options_key, array(&$this, 'load_shortcodebuilder_page'), plugin_dir_url(__FILE__) . '../css/images/onedrive_logo_small.png');
                $this->shortcodebuilderpage = add_submenu_page($this->plugin_options_key, __('Shortcode Builder', 'shareonedrive'), __('Shortcode Builder', 'shareonedrive'), 'read', $this->plugin_options_key, array(&$this, 'load_shortcodebuilder_page'));
                $menuadded = true;
            } else {
                $this->shortcodebuilderpage = add_submenu_page($this->plugin_options_key, __('Shortcode Builder', 'shareonedrive'), __('Shortcode Builder', 'shareonedrive'), 'read', $this->plugin_options_key . '_shortcodebuilder', array(&$this, 'load_shortcodebuilder_page'));
            }
        }

        if (Helpers::check_user_role($this->settings['permissions_link_users'])) {
            if (!$menuadded) {
                $this->userpage = add_menu_page('Share-one-Drive', 'Share-one-Drive', 'read', $this->plugin_options_key, array(&$this, 'load_linkusers_page'), plugin_dir_url(__FILE__) . '../css/images/onedrive_logo_small.png');
                $this->userpage = add_submenu_page($this->plugin_options_key, __('Link Private Folders', 'shareonedrive'), __('Link Private Folders', 'shareonedrive'), 'read', $this->plugin_options_key, array(&$this, 'load_linkusers_page'));
                $menuadded = true;
            } else {
                $this->userpage = add_submenu_page($this->plugin_options_key, __('Link Private Folders', 'shareonedrive'), __('Link Private Folders', 'shareonedrive'), 'read', $this->plugin_options_key . '_linkusers', array(&$this, 'load_linkusers_page'));
            }
        }
        if (Helpers::check_user_role($this->settings['permissions_see_filebrowser'])) {
            if (!$menuadded) {
                $this->filebrowserpage = add_menu_page('Share-one-Drive', 'Share-one-Drive', 'read', $this->plugin_options_key, array(&$this, 'load_filebrowser_page'), plugin_dir_url(__FILE__) . '../css/images/onedrive_logo_small.png');
                $this->filebrowserpage = add_submenu_page($this->plugin_options_key, __('File Browser', 'shareonedrive'), __('File Browser', 'shareonedrive'), 'read', $this->plugin_options_key, array(&$this, 'load_filebrowser_page'));
                $menuadded = true;
            } else {
                $this->filebrowserpage = add_submenu_page($this->plugin_options_key, __('File Browser', 'shareonedrive'), __('File Browser', 'shareonedrive'), 'read', $this->plugin_options_key . '_filebrowser', array(&$this, 'load_filebrowser_page'));
            }
        }
    }

    public function add_admin_network_menu() {
        add_menu_page('Share-one-Drive', 'Share-one-Drive', 'manage_options', $this->plugin_network_options_key, array(&$this, 'load_settings_network_page'), plugin_dir_url(__FILE__) . 'css/images/onedrive_logo_small.png');
    }

    public function register_settings() {
        register_setting($this->settings_key, $this->settings_key);
    }

    function load_settings() {
        $this->settings = (array) get_option($this->settings_key);

        $update = false;
        if (!isset($this->settings['onedrive_app_client_id'])) {
            $this->settings['onedrive_app_client_id'] = '';
            $this->settings['onedrive_app_client_secret'] = '';
            $update = true;
        }

        if ($update) {
            update_option($this->settings_key, $this->settings);
        }
    }

    public function load_settings_page() {
        if (!Helpers::check_user_role($this->settings['permissions_edit_settings'])) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'shareonedrive'));
        }


        include(sprintf("%s/templates/admin.php", SHAREONEDRIVE_ROOTDIR));
    }

    public function load_settings_network_page() {
        $shareonedrive_purchaseid = get_site_option('shareonedrive_purchaseid');
        ?>
        <div class="wrap">
          <div class='left' style="min-width:400px; max-width:650px; padding: 0 20px 0 0; float:left">
            <?php if ($_GET['updated']) { ?>
                <div id="message" class="updated"><p><?php _e('Saved!', 'shareonedrive'); ?></p></div>
            <?php } ?>
            <form action="<?php echo network_admin_url('edit.php?action=' . $this->plugin_network_options_key); ?>" method="post">
              <?php
              echo __('If you would like to receive updates, please insert your Purchase code', 'shareonedrive') . '. ' .
              '<a href="http://support.envato.com/index.php?/Knowledgebase/Article/View/506/54/where-can-i-find-my-purchase-code">' .
              __('Where do I find the purchase code?', 'shareonedrive') . '</a>.';
              ?>
              <table class="form-table">
                <tbody>
                  <tr valign="top">
                    <th scope="row"><?php _e('Purchase Code', 'shareonedrive'); ?></th>
                    <td><input type="text" name="shareonedrive_purchaseid" id="shareonedrive_purchaseid" value="<?php echo $shareonedrive_purchaseid; ?>" placeholder="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" maxlength="37" style="width:90%"/></td>
                  </tr>
                </tbody>
              </table>
              <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
            </form>
          </div>
        </div>
        <?php
    }

    public function save_settings($new_settings, $old_settings) {

        foreach ($new_settings as $setting_key => &$value) {
            if ($value === 'on') {
                $value = 'Yes';
            }

            if ($setting_key === 'onedrive_app_own' && $value === 'No') {
                $new_settings['onedrive_app_client_id'] = '';
                $new_settings['onedrive_app_client_secret'] = '';
            }

            if ($setting_key === 'colors') {
                $value = $this->_check_colors($value, $old_settings['colors']);
            }

            $new_settings['icon_set'] = rtrim($new_settings['icon_set'], '/') . '/';

            if ($new_settings['icon_set'] !== $old_settings['icon_set']) {
                $this->get_processor()->reset_complete_cache();
            }
        }


        return $new_settings;
    }

    public function save_settings_network() {
        if (current_user_can('manage_network_options')) {
            update_site_option('shareonedrive_purchaseid', $_POST['shareonedrive_purchaseid']);
        }

        wp_redirect(
                add_query_arg(
                        array('page' => $this->plugin_network_options_key, 'updated' => 'true'), network_admin_url('admin.php')
                )
        );
        exit;
    }

    private function _check_colors($colors, $old_colors) {
        $regex = '/(light|dark|transparent|#(?:[0-9a-f]{2}){2,4}|#[0-9a-f]{3}|(?:rgba?|hsla?)\((?:\d+%?(?:deg|rad|grad|turn)?(?:,|\s)+){2,3}[\s\/]*[\d\.]+%?\))/i';

        foreach ($colors as $color_id => &$color) {
            if (preg_match($regex, $color) !== 1) {
                $color = $old_colors[$color_id];
            }
        }

        return $colors;
    }

    public function admin_footer_text($footer_text) {

        $rating_asked = get_option('share_one_drive_rating_asked', false);
        if ($rating_asked == true || (Helpers::check_user_role($this->settings['permissions_edit_settings'])) === false) {
            return $footer_text;
        }

        $current_screen = get_current_screen();

        if (isset($current_screen->id) && in_array($current_screen->id, array($this->filebrowserpage, $this->userpage, $this->settingspage))) {
            $onclick = "jQuery.post( '" . SHAREONEDRIVE_ADMIN_URL . "', { action: 'shareonedrive-rating-asked' });jQuery( this ).parent().text( jQuery( this ).data( 'rated' ) )";

            $footer_text = sprintf(
                    __('If you like %1$s please leave us a %2$s rating. A huge thanks in advance!', 'shareonedrive'), sprintf('<strong>%s</strong>', esc_html__('Share-one-Drive', 'shareonedrive')), '<a href="https://1.envato.market/c/1260925/275988/4415?u=https%3A%2F%2Fcodecanyon.net%2Fitem%2Fshareonedrive-onedrive-plugin-for-wordpress%2Freviews%2F11453104" target="_blank" class="shareonedrive-rating-link" data-rated="' . esc_attr__('Thanks :)', 'shareonedrive') . '"  onclick="' . $onclick . '">&#9733;&#9733;&#9733;&#9733;&#9733;</a>'
            );
        }

        return $footer_text;
    }

    function load_filebrowser_page() {

        if (!Helpers::check_user_role($this->settings['permissions_see_filebrowser'])) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'shareonedrive'));
        }


        include(sprintf("%s/templates/admin_filebrowser.php", SHAREONEDRIVE_ROOTDIR));
    }

    function load_linkusers_page() {
        if (!Helpers::check_user_role($this->settings['permissions_link_users'])) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'shareonedrive'));
        }
        $linkusers = new LinkUsers($this->get_main());
        $linkusers->render();
    }

    function load_shortcodebuilder_page() {
        if (!Helpers::check_user_role($this->settings['permissions_add_shortcodes'])) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'shareonedrive'));
        }

        echo "<iframe src='" . SHAREONEDRIVE_ADMIN_URL . "?action=shareonedrive-getpopup&standaloneshortcodebuilder=1' width='90%' height='1000' tabindex='-1' frameborder='0'></iframe>";
    }

    function load_dashboard_page() {
        if (!Helpers::check_user_role($this->settings['permissions_see_dashboard'])) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'shareonedrive'));
        }

        include(sprintf("%s/templates/admin_dashboard.php", SHAREONEDRIVE_ROOTDIR));
    }

    public function get_plugin_activated_box() {
        $plugin = dirname(plugin_basename(__FILE__)) . '/share-one-drive.php';

        $purchasecode = $this->settings['purchase_code'];
        if (is_multisite() && is_plugin_active_for_network($plugin)) {
            $site_purchase_code = get_site_option('shareonedrive_purchaseid');

            if (!empty($site_purchase_code)) {
                $purchasecode = $site_purchase_code;
            }
        }

        /* Check if Auto-update is being activated */
        if (isset($_REQUEST['purchase_code']) && isset($_REQUEST['plugin_id']) && ((int) $_REQUEST['plugin_id'] === $this->plugin_id)) {
            $purchasecode = $this->settings['purchase_code'] = sanitize_key($_REQUEST['purchase_code']);
            update_option($this->settings_key, $this->settings);

            if (is_multisite() && is_plugin_active_for_network($plugin)) {
                update_site_option('shareonedrive_purchaseid', sanitize_key($_REQUEST['purchase_code']));
            }
        }


        $box_class = 'sod-updated';
        $box_text = __('The plugin is <strong>Activated</strong> and the <strong>Auto-Updater</strong> enabled', 'shareonedrive') . ". " . __('Your purchasecode', 'shareonedrive') . ":<br/><code style='user-select: initial;'>" . esc_attr($this->settings['purchase_code']) . '</code>';
        if (empty($purchasecode)) {
            $box_class = 'sod-error';
            $box_text = __('The plugin is <strong>Not Activated</strong> and the <strong>Auto-Updater</strong> disabled', 'shareonedrive') . ". " . __('Please activate your copy in order to have direct access to the latest updates and to get support', 'shareonedrive') . ". ";
            $box_text .= "</p><p><input id='updater_button' type='button' class='simple-button blue' value='" . __('Activate', 'shareonedrive') . "' />";
            $box_text .= '</p><p><a href="#" onclick="$(this).next().slideToggle()">' . __('Or insert your purchasecode manually and press Activate', 'shareonedrive') . '</a><input name="share_one_drive_settings[purchase_code]" id="purchase_code" class="shareonedrive-option-input-large" placeholder="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" style="display:none" value="' . esc_attr($this->settings['purchase_code']) . '">';
        } else {
            $box_text .= "</p><p><input id='check_updates_button' type='button' class='simple-button blue' value='" . __('Check for Updates', 'shareonedrive') . "' />";
            $box_text .= "<input id='deactivate_license_button' type='button' class='simple-button default' value='" . __('Deactivate License', 'shareonedrive') . "' />";
        }

        return "<div id='message' class='$box_class shareonedrive-option-description'><p>$box_text</p></div>";
    }

    public function get_plugin_authorization_box() {

        $revokebutton = "<div id='revokeOneDrive_button' type='button' class='simple-button blue'/>" . __('Revoke Authorization', 'shareonedrive') . "&nbsp;<div class='sod-spinner'></div></div>";

        try {
            $app = $this->get_app();
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Share-one-Drive has encountered an error: %s', $ex->getMessage()));


            $box_class = 'sod-error';
            $box_text = '<strong>' . __('Share-one-Drive has encountered an error', 'shareonedrive') . "</strong> ";
            $box_text .= '<p><em>' . __('Error Details', 'shareonedrive') . ":</em> <code>" . $ex->getMessage() . '</code></p>';
            return "<div id = 'message' class = '$box_class shareonedrive-option-description'><p>$box_text</p><p>$revokebutton</p></div>";
        }

        $app->set_approval_prompt('login');
        $authurl = $app->get_auth_url();
        $personal_url = str_replace('common', 'consumers', $authurl);
        $business_url = str_replace('common', 'organizations', $authurl);

        $authorizebutton = "<div id='authorizeOneDrive_button' type='button' class='simple-button blue' data-url='$authurl'/>" . __('(Re) Authorize the Plugin!', 'shareonedrive') . "&nbsp;<div class='sod-spinner'></div></div>";
        $authorizebutton .= "<div id='authorizeOneDrive_options' class='shareonedrive-option-description' style='display:none;'><p><br/><span>" . __('Trouble to login with your OneDrive account? Try a direct link instead:', 'shareonedrive') . "</span><br/>" .
                "<a href='$personal_url' target='_blank'>OneDrive Personal</a> | <a href='$business_url' target='_blank'>OneDrive Business</a>" .
                "</p></div>";

        if ($app->has_access_token()) {

            try {
                $client = $this->get_processor()->get_client();

                /* Update the Account data */
                $account = $client->get_drive_info();
                $account_name = $account->getOwner()->getUser()->getDisplayName();
                $account_email = $account->getOwner()->getUser()->getEmail();
                $account_space_quota_used = Helpers::bytes_to_size_1024($account->getQuota()->getUsed());
                $account_space_quota_total = Helpers::bytes_to_size_1024($account->getQuota()->getTotal());
                $account_type = ucwords($account->getDriveType());

                $box_class = 'sod-updated';
                $box_text = sprintf(__('Share-one-Drive is succesfully authorized and linked with OneDrive %s account:', 'shareonedrive'), $account_type) . " <br/><code>$account_name - $account_email ($account_space_quota_used/$account_space_quota_total)</code>";
                $box_buttons = $revokebutton;
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Share-one-Drive has encountered an error: %s', $ex->getMessage()));

                $box_class = 'sod-error';
                $box_text = '<strong>' . __('Share-one-Drive has encountered an error', 'shareonedrive') . "</strong> ";
                if ($app->has_plugin_own_app()) {
                    $box_text .= '<p>' . __('Please fall back to the default App by clearing the KEY and Secret on the Advanced settings tab', 'shareonedrive') . '.</p>';
                }

                $box_text .= '<p><em>' . __('Error Details', 'shareonedrive') . ": " . $ex->getMessage() . '</em></p>';
                $box_buttons = $revokebutton . $authorizebutton;

                define('SHAREONEDRIVE_NOAUTHORIZATION', true);
            }
        } else {
            $box_class = 'sod-error';
            $box_text = '<strong>' . __("Plugin isn't linked to your OneDrive... Please Authorize!", 'shareonedrive') . '</strong>';

            $box_text .= '<div class="shareonedrive-option-description">' . __("To authorize the plugin, you will agree with the following", 'shareonedrive') . ':<ul><li>' .
                    sprintf(__("Allow the plugin to read, create, update and delete files on your %s", 'OneDrive'), 'shareonedrive') . '.</li>' .
                    '<li>' . __("Allow the plugin to read and update data when your users are using the plugin", 'shareonedrive') . '.</li>' .
                    '<li>' . __("Allow the plugin to see basic user profile information.", 'shareonedrive') . '.</li>' .
                    '<li>' . __("Allow the plugin to read basic company information when available (Business Accounts).", 'shareonedrive') . '.</li></ul></div>';

            $box_buttons = $authorizebutton;
        }

        return "<div id = 'message' class = '$box_class shareonedrive-option-description'><p>$box_text</p><p>$box_buttons</p></div>";
    }

    public function get_plugin_reset_box() {
        $box_text = __('Share-one-Drive uses a cache to improve performance', 'shareonedrive') . ". " . __('If the plugin somehow is causing issues, try to reset the cache first', 'shareonedrive') . ".<br/>";

        $box_button = "<div id='resetOneDrive_button' type='button' class='simple-button blue'/>" . __('Reset Cache', 'shareonedrive') . "&nbsp;<div class='sod-spinner'></div></div>";
        return "<div id='message'><p>$box_text</p><p>$box_button</p> </div>";
    }

    public function get_admin_notice($force = false) {
        global $pagenow;
        if ($pagenow == 'index.php' || $pagenow == 'plugins.php' || $force === true) {
            if (version_compare(PHP_VERSION, '5.4.0') < 0) {
                echo '<div id="message" class="error"><p><strong>Share-one-Drive - Error: </strong>' . __('You need at least PHP 5.4 if you want to use Share-one-Drive', 'shareonedrive') . '. ' .
                __('You are using:', 'shareonedrive') . ' <u>' . phpversion() . '</u></p></div>';
            } elseif (!function_exists('curl_init')) {
                $this->canconnect = false;
                echo '<div id="message" class="error"><p><strong>Share-one-Drive - Error: </strong>' .
                __("We are not able to connect to the OneDrive API as you don't have the cURL PHP extension installed", 'shareonedrive') . '. ' .
                __("Please enable or install the cURL extension on your server", 'shareonedrive') . '. ' .
                '</p></div>';
            } elseif (class_exists('SODOneDrive_Client') && (!method_exists('SODOneDrive_Client', 'getLibraryVersion'))) {
                $this->canconnect = false;
                echo '<div id="message" class="error"><p><strong>Share-one-Drive - Error: </strong>' .
                __("We are not able to connect to the OneDrive API as the plugin is interfering with an other plugin", 'shareonedrive') . '. <br/><br/>' .
                __("The other plugin is using an old version of the OneDrive-Api-PHP-client that isn't capable of running multiple configurations", 'shareonedrive') . '. ' .
                __("Please disable this other plugin if you would like to use Share-one-Drive", 'shareonedrive') . '. ' .
                __("If you would like to use both plugins, ask the developer to update it's code", 'shareonedrive') . '. ' .
                '</p></div>';
            } elseif (!file_exists(SHAREONEDRIVE_CACHEDIR) || !is_writable(SHAREONEDRIVE_CACHEDIR) || !file_exists(SHAREONEDRIVE_CACHEDIR . '/.htaccess')) {
                echo '<div id="message" class="error"><p><strong>Share-one-Drive - Error: </strong>' . sprintf(__('Cannot create the cache directory %s, or it is not writable', 'shareonedrive'), '<code>' . SHAREONEDRIVE_CACHEDIR . '</code>') . '. ' .
                sprintf(__('Please check if the directory exists on your server and has %s writing permissions %s', 'shareonedrive'), '<a href="https://codex.wordpress.org/Changing_File_Permissions" target="_blank">', '</a>') . '</p></div>';
            }
        }
    }

    public function get_admin_notice_not_authorized() {
        global $pagenow;
        if ($pagenow == 'index.php' || $pagenow == 'plugins.php') {
            if (current_user_can('manage_options') || current_user_can('edit_theme_options')) {

                $app = new \TheLion\ShareoneDrive\App($this->get_processor());

                if ($app->has_access_token() === false || (wp_next_scheduled('shareonedrive_lost_authorisation_notification') !== false)) {
                    $location = get_admin_url(null, 'admin.php?page=ShareoneDrive_settings');
                    echo '<div id="message" class="error"><p><strong>Share-one-Drive: </strong>' . __('The plugin isn\'t autorized to use your OneDrive', 'shareonedrive') . '. ' .
                    "<a href='$location' class='button-primary'>" . __('Authorize the plugin!', 'shareonedrive') . '</a></p></div>';
                }
            }
        }
    }

    public function check_for_updates() {
        /* Updater */
        $purchasecode = false;

        $plugin = dirname(plugin_basename(__FILE__)) . '/share-one-drive.php';
        if (is_multisite() && is_plugin_active_for_network($plugin)) {
            $purchasecode = get_site_option('shareonedrive_purchaseid');
        } else {
            $purchasecode = $this->settings['purchase_code'];
        }

        if (!empty($purchasecode)) {
            require_once 'plugin-update-checker/plugin-update-checker.php';
            $updatechecker = \Puc_v4_Factory::buildUpdateChecker('https://www.wpcloudplugins.com/updates/?action=get_metadata&slug=share-one-drive&purchase_code=' . $purchasecode . '&plugin_id=' . $this->plugin_id, plugin_dir_path(__DIR__) . '/share-one-drive.php');
        }
    }

    public function get_system_information() {
        $check = array();

        array_push($check, array('success' => true, 'warning' => false, 'value' => __('WordPress version', 'shareonedrive'), 'description' => get_bloginfo('version')));
        array_push($check, array('success' => true, 'warning' => false, 'value' => __('Plugin version', 'shareonedrive'), 'description' => SHAREONEDRIVE_VERSION));
        array_push($check, array('success' => true, 'warning' => false, 'value' => __('Memory limit', 'shareonedrive'), 'description' => (ini_get('memory_limit'))));

        if (version_compare(PHP_VERSION, '5.4.0') < 0) {
            array_push($check, array('success' => false, 'warning' => false, 'value' => __('PHP version', 'shareonedrive'), 'description' => phpversion() . ' ' . __('You need at least PHP 5.4 if you want to use Share-one-Drive', 'shareonedrive')));
        } else {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('PHP version', 'shareonedrive'), 'description' => phpversion()));
        }

        /* Check if we can use CURL */
        if (function_exists('curl_init')) {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('cURL PHP extension', 'shareonedrive'), 'description' => __('You have the cURL PHP extension installed and we can access OneDrive with cURL', 'shareonedrive')));
        } else {
            array_push($check, array('success' => false, 'warning' => false, 'value' => __('cURL PHP extension', 'shareonedrive'), 'description' => __("You don't have the cURL PHP extension installed (couldn't find function \"curl_init\"), please enable or install this extension", 'shareonedrive')));
        }

        /* Check if we can use fOpen */
        if (ini_get('allow_url_fopen')) {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is allow_url_fopen enabled?', 'shareonedrive'), 'description' => __('Yes, we can access OneDrive with fopen', 'shareonedrive')));
        } else {
            array_push($check, array('success' => false, 'warning' => false, 'value' => __('Is allow_url_fopen enabled?', 'shareonedrive'), 'description' => __("No, we can't access OneDrive with fopen", 'shareonedrive')));
        }

        /* Check which version of the OneDrive API Client we are using */
        if (class_exists('SODOneDrive_Client') && (method_exists('SODOneDrive_Client', 'getLibraryVersion'))) {
            $OneDriveClient = new \SODOneDrive_Client();
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Version OneDrive API Client', 'shareonedrive'), 'description' => $OneDriveClient->getLibraryVersion()));
        } else {
            array_push($check, array('success' => false, 'warning' => false, 'value' => __('Version OneDrive API Client', 'shareonedrive'), 'description' => __("Before version 1.0.0", 'shareonedrive') . '. ' . __("Another plugin is loading an old OneDrive Client library. Share-one-Drive isn't compatible with this version.", 'shareonedrive')));
        }

        /* Check if temp dir is writeable */
        $uploadir = wp_upload_dir();

        if (!is_writable($uploadir['path'])) {
            array_push($check, array('success' => false, 'warning' => false, 'value' => __('Is TMP directory writable?', 'shareonedrive'), 'description' => __('TMP directory', 'shareonedrive') . ' \'' . $uploadir['path'] . '\' ' . __('isn\'t writable. You are not able to upload files to OneDrive.', 'shareonedrive') . ' ' . __('Make sure TMP directory is writable', 'shareonedrive')));
        } else {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is TMP directory writable?', 'shareonedrive'), 'description' => __('TMP directory is writable', 'shareonedrive')));
        }

        /* Check if cache dir is writeable */
        if (!file_exists(SHAREONEDRIVE_CACHEDIR)) {
            @mkdir(SHAREONEDRIVE_CACHEDIR, 0755);
        }

        if (!is_writable(SHAREONEDRIVE_CACHEDIR)) {
            @chmod(SHAREONEDRIVE_CACHEDIR, 0755);

            if (!is_writable(SHAREONEDRIVE_CACHEDIR)) {
                array_push($check, array('success' => false, 'warning' => false, 'value' => __('Is CACHE directory writable?', 'shareonedrive'), 'description' => __('CACHE directory', 'shareonedrive') . ' \'' . SHAREONEDRIVE_CACHEDIR . '\' ' . __('isn\'t writable. The gallery will load very slowly.', 'shareonedrive') . ' ' . __('Make sure CACHE directory is writable', 'shareonedrive')));
            } else {
                array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is CACHE directory writable?', 'shareonedrive'), 'description' => __('CACHE directory is now writable', 'shareonedrive')));
            }
        } else {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is CACHE directory writable?', 'shareonedrive'), 'description' => __('CACHE directory is writable', 'shareonedrive')));
        }

        /* Check if cache index-file is writeable */
        if (!is_readable(SHAREONEDRIVE_CACHEDIR . '/index')) {
            @file_put_contents(SHAREONEDRIVE_CACHEDIR . '/index', json_encode(array()));

            if (!is_readable(SHAREONEDRIVE_CACHEDIR . '/index')) {
                array_push($check, array('success' => false, 'warning' => false, 'value' => __('Is CACHE-index file writable?', 'shareonedrive'), 'description' => __('-index file', 'shareonedrive') . ' \'' . SHAREONEDRIVE_CACHEDIR . 'index' . '\' ' . __('isn\'t writable. The gallery will load very slowly.', 'shareonedrive') . ' ' . __('Make sure CACHE-index file is writable', 'shareonedrive')));
            } else {
                array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is CACHE-index file writable?', 'shareonedrive'), 'description' => __('CACHE-index file is now writable', 'shareonedrive')));
            }
        } else {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is CACHE-index file writable?', 'shareonedrive'), 'description' => __('CACHE-index file is writable', 'shareonedrive')));
        }

        /* Check if cache dir is writeable */
        if (!file_exists(SHAREONEDRIVE_CACHEDIR . '/thumbnails')) {
            @mkdir(SHAREONEDRIVE_CACHEDIR . '/thumbnails', 0755);
        }

        if (!is_writable(SHAREONEDRIVE_CACHEDIR . '/thumbnails')) {
            @chmod(SHAREONEDRIVE_CACHEDIR . '/thumbnails', 0755);

            if (!is_writable(SHAREONEDRIVE_CACHEDIR . '/thumbnails')) {
                array_push($check, array('success' => false, 'warning' => false, 'value' => __('Is THUMBNAIL directory writable?', 'shareonedrive'), 'description' => __('THUMBNAIL directory', 'shareonedrive') . ' \'' . SHAREONEDRIVE_CACHEDIR . '\thumbnails ' . __('isn\'t writable. Thumbnails of Google Doc files will not always properly.', 'shareonedrive') . ' ' . __('Make sure THUMBNAIL directory is writable', 'shareonedrive')));
            } else {
                array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is THUMBNAIL directory writable?', 'shareonedrive'), 'description' => __('THUMBNAIL directory is now writable', 'shareonedrive')));
            }
        } else {
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Is THUMBNAIL directory writable?', 'shareonedrive'), 'description' => __('THUMBNAIL directory is writable', 'shareonedrive')));
        }

        /* Check if we can use ZIP class */
        if (class_exists('ZipArchive')) {
            $message = __("You can use the ZIP function", 'shareonedrive');
            array_push($check, array('success' => true, 'warning' => false, 'value' => __('Download files as ZIP', 'shareonedrive'), 'description' => $message));
        } else {
            $message = __("You cannot download files as ZIP", 'shareonedrive');
            array_push($check, array('success' => true, 'warning' => true, 'value' => __('Download files as ZIP', 'shareonedrive'), 'description' => $message));
        }


        if (!extension_loaded('mbstring')) {
            array_push($check, array('success' => false, 'warning' => false, 'value' => __('mbstring extension enabled?', 'shareonedrive'), 'description' => __('The required mbstring extension is not enabled on your server. Please enable this extension.', 'shareonedrive')));
        }

        /* Check if Gravity Forms is installed and can be used */
        if (class_exists("GFForms")) {
            $is_correct_version = version_compare(\GFCommon::$version, '1.9', '>=');

            if ($is_correct_version) {
                $message = __("You can use Share-one-Drive in Gravity Forms (" . \GFCommon::$version . ")", 'shareonedrive');
                array_push($check, array('success' => true, 'warning' => false, 'value' => __('Gravity Forms integration', 'shareonedrive'), 'description' => $message));
            } else {
                $message = __("You have Gravity Forms (" . \GFCommon::$version . ") installed, but versions before 1.9 are not supported. Please update Gravity Forms if you want to use this plugin in combination with Gravity Forms", 'shareonedrive');
                array_push($check, array('success' => false, 'warning' => true, 'value' => __('Gravity Forms integration', 'shareonedrive'), 'description' => $message));
            }
        }

        if (defined("WPCF7_PLUGIN")) {
            $is_correct_version = false;
            if (defined("WPCF7_VERSION")) {
                $is_correct_version = version_compare(WPCF7_VERSION, '5.0', '>=');
            }
            if ($is_correct_version) {
                $message = __("You can use Share-one-Drive in Contact Form 7 (" . WPCF7_VERSION . ")", 'shareonedrive');
                array_push($check, array('success' => true, 'warning' => false, 'value' => __('Contact Form integration', 'shareonedrive'), 'description' => $message));
            } else {
                $message = __("You have Contact Form 7 installed, but this version is not supported. Please update Contact Form 7 to the latest version if you want to use this plugin in combination with Contact Form", 'shareonedrive');
                array_push($check, array('success' => false, 'warning' => true, 'value' => __('Contact Form integration', 'shareonedrive'), 'description' => $message));
            }
        }

        if (class_exists("WC_Integration")) {

            global $woocommerce;
            $is_correct_version = (is_object($woocommerce) ? version_compare($woocommerce->version, '3.0', '>=') : false);

            if ($is_correct_version) {
                $message = __("You can use Share-one-Drive in WooCommerce (" . $woocommerce->version . ") for your Digital Products ", 'shareonedrive') . '<br/><br/> ';
                array_push($check, array('success' => true, 'warning' => false, 'value' => __('WooCommerce Digital Products', 'shareonedrive'), 'description' => $message));
            } else {
                $message = __("You have WooCommerce (" . $woocommerce->version . ") installed, but versions before 3.0 are not supported. Please update WooCommerce if you want to use this plugin in combination with WooCommerce", 'shareonedrive');
                array_push($check, array('success' => false, 'warning' => true, 'value' => __('WooCommerce Digital Products', 'shareonedrive'), 'description' => $message));
            }
        }

        /* Create Table */
        $html = '<table border="0" cellspacing="0" cellpadding="0">';

        foreach ($check as $row) {

            $color = ($row['success']) ? 'green' : 'red';
            $color = ($row['warning']) ? 'orange' : $color;

            $html .= '<tr style="vertical-align:top;"><td width="300" style="padding: 5px; color:' . $color . '"><strong>' . $row['value'] . '</strong></td><td style="padding: 5px;">' . $row['description'] . '</td></tr>';
        }

        $html .= '</table>';

        return $html;
    }

    /*
     * Add MCE buttons and script
     */

    public function load_shortcode_buttons() {

        /* Abort early if the user will never see TinyMCE */
        if (
                !(Helpers::check_user_role($this->settings['permissions_add_shortcodes'])) &&
                !(Helpers::check_user_role($this->settings['permissions_add_links'])) &&
                !(Helpers::check_user_role($this->settings['permissions_add_embedded']))
        ) {
            return;
        }

        if (get_user_option('rich_editing') !== 'true')
            return;

        /* Add a callback to regiser our tinymce plugin */
        add_filter("mce_external_plugins", array(&$this, "register_tinymce_plugin"));

        /* Add a callback to add our button to the TinyMCE toolbar */
        add_filter('mce_buttons', array(&$this, 'register_tinymce_plugin_buttons'));

        /* Add custom CSS for placeholders */
        add_editor_style(SHAREONEDRIVE_ROOTPATH . '/css/tinymce_editor.css');

        add_action('enqueue_block_editor_assets', array(&$this, 'enqueue_tinymce_css_gutenberg'));
    }

    /* This callback registers our plug-in */

    function register_tinymce_plugin($plugin_array) {
        $plugin_array['shareonedrive'] = SHAREONEDRIVE_ROOTPATH . "/includes/js/Tinymce.js";
        return $plugin_array;
    }

    /* This callback adds our button to the toolbar */

    function register_tinymce_plugin_buttons($buttons) {
        /* Add the button ID to the $button array */

        if (Helpers::check_user_role($this->settings['permissions_add_shortcodes'])) {
            $buttons[] = "shareonedrive";
        }
        if (Helpers::check_user_role($this->settings['permissions_add_links'])) {
            $buttons[] = "shareonedrive_links";
        }
        if (Helpers::check_user_role($this->settings['permissions_add_embedded'])) {
            $buttons[] = "shareonedrive_embed";
        }

        return $buttons;
    }

    function enqueue_tinymce_css_gutenberg() {
        wp_enqueue_style('shareonedrive-css-gutenberg', SHAREONEDRIVE_ROOTPATH . '/css/tinymce_editor.css');
    }

}
