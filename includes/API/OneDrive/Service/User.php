<?php

/*
 * Copyright 2010 Google Inc.
 * Copyright 2015 www.florisdeleeuw.nl
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

/**
 * Service definition for Drive (v2).
 *
 * <p>
 * The API to interact with Drive.</p>
 *
 * <p>
 * For more information about this service, see the API
 * <a href="https://api.onedrive.com/v1.0/drive/" target="_blank">Documentation</a>
 * </p>
 *
 * @author OneDrive, Inc.
 */
class SODOneDrive_Service_User extends SODOneDrive_Service {

    const DRIVE = "https://api.onedrive.com/v1.0/me/";

    public $me;

    /**
     * Constructs the internal representation of the Drive service.
     *
     * @param SODOneDrive_Client $client
     */
    public function __construct(SODOneDrive_Client $client) {
        parent::__construct($client);
        $this->servicePath = 'v1.0/me/';
        $this->version = 'v1.0';
        $this->serviceName = 'me';

        $this->me = new SODOneDrive_Service_User_Me_Resource(
                $this, $this->serviceName, 'me', array(
            'methods' => array(
                'get' => array(
                    'path' => '',
                    'httpMethod' => 'GET',
                    'parameters' => array(
                        '$select' => array(
                            'location' => 'query',
                            'type' => 'string',
                        ),
                        'expand' => array(
                            'location' => 'query',
                            'type' => 'string',
                        )
                    ),
                ),
            )
                )
        );
    }

}

/**
 * The "about" collection of methods.
 * Typical usage is:
 *  <code>
 *   $driveService = new SODOneDrive_Service_Drive(...);
 *   $about = $driveService->about;
 *  </code>
 */
class SODOneDrive_Service_User_Me_Resource extends SODOneDrive_Service_Resource {

    /**
     * Gets the information about the current user along with Drive API settings
     * (about.get)
     *
     * @param array $optParams Optional parameters.
     *
     * @opt_param bool includeSubscribed When calculating the number of remaining
     * change IDs, whether to include public files the user has opened and shared
     * files. When set to false, this counts only change IDs for owned files and any
     * shared or public files that the user has explicitly added to a folder they
     * own.
     * @opt_param string maxChangeIdCount Maximum number of remaining change IDs to
     * count
     * @opt_param string startChangeId Change ID to start counting from when
     * calculating number of remaining change IDs
     * @return SODOneDrive_Service_Drive_Me
     */
    public function get($optParams = array()) {
        $params = array();
        $params = array_merge($params, $optParams);
        return $this->call('get', array($params), "SODOneDrive_Service_User_Me");
    }

}

class SODOneDrive_Service_User_Me extends SODOneDrive_Collection {

    public $mySite;

    public function getMySite() {
        return $this->mySite;
    }

}
