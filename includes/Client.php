<?php

namespace TheLion\ShareoneDrive;

class Client {

    public $apifilefields = 'thumbnails(select=c48x48,medium,large,c1500x1500),children(expand=thumbnails(select=c48x48,medium,large,c1500x1500))';
    public $apifilefieldsexpire = 'thumbnails(select=c48x48,medium,large,c1500x1500),children(expand=thumbnails(select=c48x48,medium,large,c1500x1500))';
    public $apilistfilesfields = 'thumbnails(select=c48x48,medium,large,c1500x1500)';
    public $apilistfilesexpirefields = 'thumbnails(select=c48x48,medium,large,c1500x1500)';

    /**
     *
     * @var \TheLion\ShareoneDrive\App
     */
    private $_app;

    /**
     *
     * @var \TheLion\ShareoneDrive\Processor
     */
    private $_processor;
    private $_user_ip;

    public function __construct(App $_app, Processor $_processor = null) {
        $this->_app = $_app;
        $this->_processor = $_processor;
        $this->_user_ip = $_processor->get_user_ip();
    }

    /*
     * Get DriveInfo
     *
     * @return mixed|WP_Error
     */

    function get_drive_info() {
        $driveInfo = $this->get_app()->get_drive()->about->get();
        return $driveInfo;
    }

    /* Get entry */

    /**
     * 
     * @param type $entryid
     * @param type $checkauthorized
     * @return \TheLion\ShareoneDrive\CacheNode|boolean
     */
    public function get_entry($entryid = false, $checkauthorized = true) {

        if ($entryid === false) {
            $entryid = $this->get_processor()->get_requested_entry();
        }

        /* Load the root folder when needed */
        $this->get_root_folder();

        /* Get entry from cache */
        $cachedentry = $this->get_cache()->is_cached($entryid);

        /* Get metadata if entry isn't cached  */
        if (!$cachedentry) {

            try {
                $api_entry = $this->get_app()->get_drive()->items->get($entryid, array("expand" => $this->apifilefields));
                $entry = new Entry($api_entry);
                $cachedentry = $this->get_cache()->add_to_cache($entry);
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return false;
            }
        }

        if ($checkauthorized === true) {
            if ($entryid !== 'root' && !$this->get_processor()->_is_entry_authorized($cachedentry)) {
                return false;
            }
        }

        return $cachedentry;
    }

    public function get_root_folder() {

        $root_node = $this->get_cache()->get_root_node();

        if ($root_node !== false) {
            return $root_node;
        }

        /* First get the root of the cloud */
        try {
            $root = $this->get_folder('root', false);
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s not able to retreive root folder on your OneDrive: %s', __LINE__, $ex->getMessage()));
            return false;
        }

        return $this->get_cache()->get_root_node();
    }

    /**
     * Get folders and files
     * @param string $folderid
     * @param boolean $checkauthorized
     * @return array|boolean
     */
    public function get_folder($folderid = false, $checkauthorized = true) {
        /* Load the root folder when needed */
        if ($folderid !== 'root') {
            $rootfolder = $this->get_root_folder();
        }

        if ($folderid === false) {
            $folderid = $this->get_processor()->get_requested_entry();
        }

        /* Load cached folder if present */
        $cachedfolder = $this->get_cache()->is_cached($folderid, 'id', false);

        /* If folder isn't present in cache, load it */
        if (!$cachedfolder) {
            try {
                $results = $this->get_app()->get_drive()->items->get($folderid, array("expand" => $this->apilistfilesfields));
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return false;
            }

            $folder_entry = new Entry($results);
            $cachedfolder = $this->get_cache()->add_to_cache($folder_entry);

            try {
                $results_children = $this->get_app()->get_drive()->items->children($folderid, array("expand" => $this->apilistfilesfields));
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return false;
            }

            $files_in_folder = $results_children->getValue();
            $next_page_token = $results_children['@odata.nextLink'];

            /* Get all files in folder */
            while (!empty($next_page_token)) {

                $next_link = parse_url($next_page_token);
                parse_str($next_link['query'], $next_link_attributes);
                $next_page_token = $next_link_attributes['$skiptoken'];

                try {
                    $more_files = $this->get_app()->get_drive()->items->children($folderid, array("expand" => $this->apilistfilesfields, 'skiptoken' => $next_page_token));
                    $files_in_folder = array_merge($files_in_folder, $more_files->getValue());
                    $next_page_token = $more_files['@odata.nextLink'];
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                    return false;
                }
            }

            /* Convert the items to Framework Entry */

            $folder_items = array();

            /* Add all entries in folder to cache */

            foreach ($files_in_folder as $entry) {
                $item = new Entry($entry);
                $newitem = $this->get_cache()->add_to_cache($item);
            }

            $cachedfolder->set_loaded_children(true);
            $this->get_cache()->update_cache();
        }

        $folder = $cachedfolder;
        $files_in_folder = $cachedfolder->get_children();

        /* Check if folder is in the shortcode-set rootfolder */
        if ($checkauthorized === true) {
            if (!$this->get_processor()->_is_entry_authorized($cachedfolder)) {
                return false;
            }
        }

        return array('folder' => $folder, 'contents' => $files_in_folder);
    }

    public function update_expired_entry(CacheNode $cachedentry) {
        $entry = $cachedentry->get_entry();
        try {
            $api_entry = $this->get_app()->get_drive()->items->get($entry->get_id(), array("expand" => $this->apifilefieldsexpire));
            $entry = new Entry($api_entry);
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
            return false;
        }

        return $this->get_cache()->add_to_cache($entry);
    }

    public function update_expired_folder(CacheNode $cachedentry) {
        $entry = $cachedentry->get_entry();

        try {
            $results_children = $this->get_app()->get_drive()->items->children($entry->get_id(), array("expand" => $this->apilistfilesexpirefields));
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
            return false;
        }

        $files_in_folder = $results_children->getValue();
        $next_page_token = $results_children['@odata.nextLink'];

        /* Get all files in folder */
        while (!empty($next_page_token)) {

            $next_link = parse_url($next_page_token);
            parse_str($next_link['query'], $next_link_attributes);
            $next_page_token = $next_link_attributes['$skiptoken'];

            try {
                $more_files = $this->get_app()->get_drive()->items->children($entry->get_id(), array("expand" => $this->apilistfilesfields, 'skiptoken' => $next_page_token));
                $files_in_folder = array_merge($files_in_folder, $more_files->getValue());
                $next_page_token = isset($next_link_attributes['@odata.nextLink']) ? $next_link_attributes['@odata.nextLink'] : null;
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return false;
            }
        }

        $folder_items = array();
        $current_children = $cachedentry->get_children();
        foreach ($files_in_folder as $api_entry) {
            $entry = new Entry($api_entry);
            $this->get_cache()->add_to_cache($entry);
        }

        $this->get_cache()->add_to_cache($cachedentry->get_entry());

        return $cachedentry;
    }

    /*
     * Search entry by name
     */

    public function search_by_name($query) {

        if ($this->get_processor()->get_shortcode_option('searchfrom') === 'parent') {
            $searchedfolder = $this->get_processor()->get_requested_entry();
        } else {
            $searchedfolder = $this->get_root_folder()->get_id();
        }

        /* Find all items containing query */
        $params = array('id' => $searchedfolder, 'q' => stripslashes($query), "expand" => $this->apilistfilesfields);

        /* Do the request */
        $next_page_token = null;
        $files_found = array();
        $entries_found = array();
        $entries_in_searchedfolder = array();

        do_action('shareonedrive_log_event', 'shareonedrive_searched', $this->get_entry($searchedfolder, false), array('query' => $query));

        do {
            try {
                $search_response = $this->get_app()->get_drive()->items->search($params);
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return array();
            }

            /* Process the response */
            $more_files = $search_response->getValue();
            $files_found = array_merge($files_found, $more_files);

            if (isset($search_response['@odata.nextLink'])) {
                $next_page_token = $search_response['@odata.nextLink'];
                $next_link = parse_url($next_page_token);
                parse_str($next_link['query'], $next_link_attributes);
                $next_page_token = isset($next_link_attributes['$skiptoken']) ? $next_link_attributes['$skiptoken'] : null;
            } else {
                $next_page_token = null;
            }
            $params['skipToken'] = $next_page_token;
        } while ($next_page_token !== null);

        foreach ($files_found as $file) {
            $entries_found[] = new Entry($file);
        }

        foreach ($entries_found as $entry) {
            /* Check if entries are in cache */
            $cachedentry = $this->get_cache()->is_cached($entry->get_id());

            /* If not found, add to cache */
            if ($cachedentry === false) {
                $cachedentry = $this->get_cache()->add_to_cache($entry);
            }

            if ($this->get_processor()->_is_entry_authorized($cachedentry) === false) {
                continue;
            }

            $parent_folder = ($this->get_processor()->get_shortcode_option('searchfrom') === 'parent') ? $this->get_processor()->get_requested_entry() : $this->get_root_folder()->get_id();

            if ($cachedentry->is_in_folder($parent_folder) === false) {
                continue;
            }

            $entries_in_searchedfolder[] = $cachedentry;
        }

        /* Update the cache already here so that the Search Output is cached */
        $this->get_cache()->update_cache();

        return array('folder' => $this->get_entry($searchedfolder), 'contents' => $entries_in_searchedfolder);
    }

    /*
     * Delete multiple entries from OneDrive
     */

    public function delete_entries($entries_to_delete = array()) {
        $deleted_entries = array();

        foreach ($entries_to_delete as $target_entry_path) {
            $target_cached_entry = $this->get_entry($target_entry_path);

            if ($target_cached_entry === false) {
                continue;
            }

            $target_entry = $target_cached_entry->get_entry();

            if ($target_entry->is_file() && $this->get_processor()->get_user()->can_delete_files() === false) {
                error_log('[Share-one-Drive message]: ' . sprintf('Failed to delete %s as user is not allowed to remove files.', $target_entry->get_path()));
                $deleted_entries[$target_entry->get_id()] = false;
                continue;
            }

            if ($target_entry->is_dir() && $this->get_processor()->get_user()->can_delete_folders() === false) {
                error_log('[Share-one-Drive message]: ' . sprintf('Failed to delete %s as user is not allowed to remove folders.', $target_entry->get_path()));
                $deleted_entries[$target_entry->get_id()] = false;
                continue;
            }

            if ($this->get_processor()->get_shortcode_option('demo') === '1') {
                $deleted_entries[$target_entry->get_id()] = false;
                continue;
            }


            try {
                /* Issue with if-match header
                 * https://github.com/OneDrive/onedrive-api-docs/issues/131
                 * If solved, change to: 
                 * $headers = array("if-match" => '*'); */
                $headers = array();
                $deleted_entry = $this->get_app()->get_drive()->items->delete($target_entry->get_id(), $headers);
                $deleted_entries[$target_entry->get_id()] = $target_cached_entry;
                $this->get_cache()->remove_from_cache($target_entry->get_id(), 'deleted');

                do_action('shareonedrive_log_event', 'shareonedrive_deleted_entry', $target_cached_entry, array());
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                $deleted_entries[$target_entry->get_id()] = false;
            }
        }


        if ($this->get_processor()->get_shortcode_option('notificationdeletion') === '1') {
            /* TO DO NOTIFICATION */
            $this->get_processor()->send_notification_email('deletion', $deleted_entries);
        }

        /* Remove items from cache */
        $this->get_cache()->pull_for_changes(true);

        /* Clear Cached Requests */
        //CacheRequest::clear_local_cache_for_shortcode($this->get_processor()->get_listtoken());

        return $deleted_entries;
    }

    /*
     * Rename entry from OneDrive
     */

    function rename_entry($new_filename = null) {

        if ($this->get_processor()->get_shortcode_option('demo') === '1') {
            return new \WP_Error('broke', __('Failed to rename entry', 'shareonedrive'));
        }

        if ($new_filename === null && $this->get_processor()->get_shortcode_option('debug') === '1') {
            return new \WP_Error('broke', __('No new name set', 'shareonedrive'));
        }

        /* Get entry meta data */
        $cachedentry = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if ($cachedentry === false) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
            if ($cachedentry === false) {
                if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                    return new \WP_Error('broke', __('Invalid entry', 'shareonedrive'));
                } else {
                    return new \WP_Error('broke', __('Failed to rename entry', 'shareonedrive'));
                }
                return new \WP_Error('broke', __('Failed to rename entry', 'shareonedrive'));
            }
        }

        /* Check if user is allowed to delete from this dir */
        if (!$cachedentry->is_in_folder($this->get_processor()->get_last_folder())) {
            return new \WP_Error('broke', __("You are not authorized to rename files in this directory", 'shareonedrive'));
        }

        $entry = $cachedentry->get_entry();

        /* Check user permission */
        if (!$entry->get_permission('canrename')) {
            return new \WP_Error('broke', __('You are not authorized to rename this file or folder', 'shareonedrive'));
        }

        /* Check if entry is allowed */
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', __('You are not authorized to rename this file or folder', 'shareonedrive'));
        }

        if ($entry->is_dir() && ($this->get_processor()->get_user()->can_rename_folders() === false)) {
            return new \WP_Error('broke', __('You are not authorized to rename folder', 'shareonedrive'));
        }

        if ($entry->is_file() && ($this->get_processor()->get_user()->can_rename_files() === false)) {
            return new \WP_Error('broke', __('You are not authorized to rename this file', 'shareonedrive'));
        }

        $extension = $entry->get_extension();
        $name = (!empty($extension)) ? $new_filename . '.' . $extension : $new_filename;
        $updaterequest = array('name' => $name);

        try {
            $renamed_entry = $this->update_entry($entry->get_id(), $updaterequest);

            if ($renamed_entry !== false && $renamed_entry !== null) {
                $this->get_cache()->update_cache();
            }

            do_action('shareonedrive_log_event', 'shareonedrive_renamed_entry', $renamed_entry, array('old_name' => $entry->get_name()));
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                return new \WP_Error('broke', $ex->getMessage());
            } else {
                return new \WP_Error('broke', __('Failed to rename entry', 'shareonedrive'));
            }
        }


        $this->get_cache()->pull_for_changes(true);

        return $renamed_entry;
    }

    /*
     * Edit descriptions entry from OneDrive
     */

    function move_entry($target = null, $copy = false) {

        if ($this->get_processor()->get_shortcode_option('demo') === '1') {
            return new \WP_Error('broke', __('Failed to move entry', 'shareonedrive'));
        }

        if ($this->get_processor()->get_requested_entry() === null || $target === null) {
            return new \WP_Error('broke', __('Failed to move entry', 'shareonedrive'));
        }

        /* Get entry meta data */
        $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
        $cachedtarget = $this->get_entry($target);
        $cachedcurrentfolder = $this->get_entry($this->get_processor()->get_last_folder());

        if ($cachedentry === false || $cachedtarget === false) {
            return new \WP_Error('broke', __('Failed to move entry', 'shareonedrive'));
        }

        /* Check if user is allowed to delete from this dir */
        if (!$cachedentry->is_in_folder($cachedcurrentfolder->get_id())) {
            return new \WP_Error('broke', __("You are not authorized to move items in this directory", 'shareonedrive'));
        }

        $entry = $cachedentry->get_entry();

        /* Check user permission */
        if (!$entry->get_permission('candelete')) {
            return new \WP_Error('broke', __('You are not authorized to move this file or folder', 'shareonedrive'));
        }

        /* Check if entry is allowed */
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', __('You are not authorized to move this file or folder', 'shareonedrive'));
        }

        if ($entry->is_dir() && ($this->get_processor()->get_user()->can_move_folders() === false)) {
            return new \WP_Error('broke', __('You are not authorized to move folder', 'shareonedrive'));
        }

        if ($entry->is_file() && ($this->get_processor()->get_user()->can_move_files() === false)) {
            return new \WP_Error('broke', __('You are not authorized to move this file', 'shareonedrive'));
        }

        /* Set new parent for entry */
        $newParent = new \SODOneDrive_Service_Drive_ItemReference();
        $newParent->setId($cachedtarget->get_id());
        $updaterequest = new \SODOneDrive_Service_Drive_Item();
        $updaterequest->setParentReference($newParent);

        try {
            $this->get_cache()->remove_from_cache($cachedentry->get_id(), 'moved');

            if ($copy) {
                $updated_entry = $this->get_app()->get_drive()->items->copy($entry->get_id(), $updaterequest, array("Prefer" => 'respond-async'));
                /* Copying can take some time, so the target folder is removed from the cache and will be loaded if the user enters that directory */
                $cachedtarget->set_loaded(false);
                do_action('shareonedrive_log_event', 'shareonedrive_moved_entry', $cachedentry);
            } else {
                $updated_entry = $this->update_entry($entry->get_id(), $updaterequest);
                do_action('shareonedrive_log_event', 'shareonedrive_moved_entry', $updated_entry);
            }
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                return new \WP_Error('broke', $ex->getMessage());
            } else {
                return new \WP_Error('broke', __('Failed to move entry', 'shareonedrive'));
            }
        }

        $this->get_cache()->pull_for_changes(true);

        return $updated_entry;
    }

    /*
     * Edit description of entry
     */

    function update_description($new_description = null) {

        if ($new_description === null && $this->get_processor()->get_shortcode_option('debug') === '1') {
            return new \WP_Error('broke', __('No new description set', 'shareonedrive'));
        }

        /* Get entry meta data */
        $cachedentry = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if ($cachedentry === false) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
            if ($cachedentry === false) {
                if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                    return new \WP_Error('broke', __('Invalid entry', 'shareonedrive'));
                } else {
                    return new \WP_Error('broke', __('Failed to edit entry', 'shareonedrive'));
                }
                return new \WP_Error('broke', __('Failed to edit entry', 'shareonedrive'));
            }
        }

        /* Check if user is allowed to delete from this dir */
        if (!$cachedentry->is_in_folder($this->get_processor()->get_last_folder())) {
            return new \WP_Error('broke', __("You are not authorized to edit files in this directory", 'shareonedrive'));
        }

        $entry = $cachedentry->get_entry();

        /* Check if entry is allowed */
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', __('You are not authorized to edit this file or folder', 'shareonedrive'));
        }

        /* Set new description, and update the entry */
        $updaterequest = array('description' => $new_description);

        try {
            $edited_entry = $this->update_entry($entry->get_id(), $updaterequest);

            if ($edited_entry !== false && $edited_entry !== null) {
                do_action('shareonedrive_log_event', 'shareonedrive_updated_metadata', $edited_entry, array('metadata_field' => 'Description'));
                $this->get_cache()->update_cache();
            }
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                return new \WP_Error('broke', $ex->getMessage());
            } else {
                return new \WP_Error('broke', __('Failed to edit entry', 'shareonedrive'));
            }
        }

        return $edited_entry->get_entry()->get_description();
    }

    /*
     * Update entry from OneDrive
     */

    public function update_entry($entry_id, $updaterequest = array()) {

        try {
            $api_entry = $this->get_app()->get_drive()->items->patch($entry_id, $updaterequest, array("expand" => $this->apilistfilesfields));
            $entry = new Entry($api_entry);
            $cachedentry = $this->get_cache()->add_to_cache($entry);
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));

            throw $ex;
        }

        return $cachedentry;
    }

    /*
     * Add directory to OneDrive
     */

    function add_folder($new_folder_name = null) {
        if ($this->get_processor()->get_shortcode_option('demo') === '1') {
            return new \WP_Error('broke', __('Failed to add folder', 'shareonedrive'));
        }

        if ($new_folder_name === null && $this->get_processor()->get_shortcode_option('debug') === '1') {
            return new \WP_Error('broke', __('No new foldername set', 'shareonedrive'));
        }

        /* Get entry meta data of current folder */
        $cachedentry = $this->get_cache()->is_cached($this->get_processor()->get_last_folder());


        if ($cachedentry === false) {
            $cachedentry = $this->get_entry($this->get_processor()->get_last_folder());
            if ($cachedentry === false) {
                if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                    return new \WP_Error('broke', __('Invalid entry', 'shareonedrive'));
                } else {
                    return new \WP_Error('broke', __('Failed to add entry', 'shareonedrive'));
                }
                return new \WP_Error('broke', __('Failed to add entry', 'shareonedrive'));
            }
        }

        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            return new \WP_Error('broke', __('You are not authorized to add folders in this directory', 'shareonedrive'));
        }

        $currentfolder = $cachedentry->get_entry();

        /* Check user permission */
        if (!$currentfolder->get_permission('canadd')) {
            return new \WP_Error('broke', __('You are not authorized to add a folder', 'shareonedrive'));
        }

        /* Create new folder object */
        $newfolder = new \SODOneDrive_Service_Drive_Item();
        $newfolder->setName($new_folder_name);
        $newfolder->setFolder(new \SODOneDrive_Service_Drive_FolderFacet());
        $newfolder["@name.conflictBehavior"] = "rename";

        try {
            $api_entry = $this->get_app()->get_drive()->items->insert($currentfolder->get_id(), $newfolder, array("expand" => $this->apifilefields));
            /* Add new file to our Cache */
            $newentry = new Entry($api_entry);
            $cached_entry = $this->get_cache()->add_to_cache($newentry);

            do_action('shareonedrive_log_event', 'shareonedrive_created_entry', $cached_entry);
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));

            if ($this->get_processor()->get_shortcode_option('debug') === '1') {
                return new \WP_Error('broke', $ex->getMessage());
            } else {
                return new \WP_Error('broke', __('Failed to add folder', 'shareonedrive'));
            }
        }

        /* Remove items from cache */
        $this->get_cache()->pull_for_changes(true);

        return $cached_entry;
    }

    function preview_entry() {
        /* Get file meta data */
        $cached_entry = $this->get_entry();

        if ($cached_entry === false) {
            die('-1');
        }

        $entry = $cached_entry->get_entry();
        if ($entry->get_can_preview_by_cloud() === false) {
            die('-1');
        }

        if ($this->get_processor()->get_user()->can_preview() === false) {
            die('-1');
        }

        do_action('shareonedrive_log_event', 'shareonedrive_previewed_entry', $cached_entry);

        /* Preview for Image files */
        if (in_array($entry->get_extension(), array('jpg', 'jpeg', 'gif', 'png'))) {

            if ($this->get_processor()->get_setting('loadimages') === 'onedrivethumbnail' || $this->get_processor()->get_user()->can_download() === false) {

                if ($entry->get_thumbnail_original() !== null) {
                    header('Location: ' . $entry->get_thumbnail_original());
                    die();
                } else if ($entry->get_thumbnail_large() !== null) {
                    header('Location: ' . $entry->get_thumbnail_large());
                    die();
                }
            }
        }

        /* Preview for Media files in HTML5 Player + PDF files */
        if (in_array($entry->get_extension(), array('jpg', 'jpeg', 'gif', 'png', 'mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'ogg', 'oga'))) {
            $temporarily_link = $this->get_temporarily_link($cached_entry);
            header('Location: ' . $temporarily_link);
            die();
        }

        if (in_array($entry->get_extension(), array('pdf'))) {

            if ($this->get_app()->get_account_type() === 'personal') {
                $temporarily_link = $this->get_embedded_link($cached_entry);
            } else {
                /* Use the Google Doc viewer to preview pdf files in Business Accounts as embedding is not supported */
                $temporarily_link = $this->get_temporarily_link($cached_entry);

                if ($this->get_processor()->get_setting('allow_google_viewer') === 'Yes') {
                    $temporarily_link = 'https://docs.google.com/viewerng/viewer?embedded=true&url=' . urlencode($temporarily_link);
                } else {
                    error_log('[Share-one-Drive message]: ' . sprintf('Preview Error on line %s: %s', __LINE__, 'On Business Accounts PDFs cannot be previewed without Google Doc Viewer'));
                    die();
                }
            }

            header('Location: ' . $temporarily_link);
            die();
        }


        /* Preview for Office files */
        if (in_array($entry->get_extension(), array('csv', 'doc', 'docx', 'odp', 'ods', 'odt', 'pot', 'potm', 'potx', 'pps', 'ppsx', 'ppsxm', 'ppt', 'pptm', 'pptx', 'rtf', 'xls', 'xlsx'))) {
            if ($this->get_processor()->get_user()->can_edit()) {

                /* If user has permissions to edit the file, show the preview in Office Online */
                $shared_link = $this->get_embedded_link($cached_entry);

                if (!empty($shared_link)) {
                    header('Location: ' . $shared_link);
                    die();
                }
            }
        }

        /* Preview for all other formats */
        $temporarily_link = $this->get_temporarily_link($cached_entry, 'pdf');

        //if ($this->get_processor()->get_user()->can_download() === false && $entry->get_size() < 25000000) {
        //if ($this->get_processor()->get_setting('allow_google_viewer') === 'Yes') {
        //    $temporarily_link = 'https://docs.google.com/viewerng/viewer?embedded=true&url=' . urlencode($temporarily_link);
        //}
        //}

        header('Location: ' . $temporarily_link);
        die();
    }

    function edit_entry() {
        /* Get file meta data */
        $cached_entry = $this->get_entry();

        if ($cached_entry === false) {
            die('-1');
        }

        $entry = $cached_entry->get_entry();
        if ($entry->get_can_edit_by_cloud() === false) {
            die('-1');
        }

        $edit_link = $this->get_shared_link($cached_entry, 'edit');

        if (empty($edit_link)) {
            error_log('[Share-one-Drive message]: ' . sprintf('Cannot create a editable ahared link %s', __LINE__));
            die();
        }

        header('Location: ' . $edit_link);
        die();
    }

    /*
     * Download file
     */

    function download_entry() {
        /* Check if file is cached and still valid */
        $cached = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if ($cached === false) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
        } else {
            $cachedentry = $cached;
        }

        if ($cachedentry === false) {
            die();
        }

        $entry = $cachedentry->get_entry();

        /* get the last-modified-date of this very file */
        $lastModified = $entry->get_last_edited();
        /* get a unique hash of this file (etag) */
        $etagFile = md5($lastModified);
        /* get the HTTP_IF_MODIFIED_SINCE header if set */
        $ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
        /* get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash) */
        $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

        header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
        header("Etag: $etagFile");
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 60 * 5) . ' GMT');
        header('Cache-Control: must-revalidate');

        /* check if page has changed. If not, send 304 and exit */
        if ($cached !== false) {
            if ($lastModified !== false && (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified || $etagHeader == $etagFile)) {

                /* Send email if needed */
                if ($this->get_processor()->get_shortcode_option('notificationdownload') === '1') {
                    $this->get_processor()->send_notification_email('download', array($cachedentry));
                }

                do_action('shareonedrive_download', $cachedentry);
                header("HTTP/1.1 304 Not Modified");
                exit;
            }
        }

        /* Check if entry is allowed */
        if (!$this->get_processor()->_is_entry_authorized($cachedentry)) {
            die();
        }

        /* Send email if needed */
        if ($this->get_processor()->get_shortcode_option('notificationdownload') === '1') {
            $this->get_processor()->send_notification_email('download', array($cachedentry));
        }


        /* Redirect if needed */
        if (isset($_REQUEST['redirect']) && ($this->get_processor()->get_shortcode_option('onclick') === 'redirect')) {
            $shared_link = $this->get_shared_link($cachedentry, 'view');
            header('Location: ' . $shared_link);
            die();
        }

        /* Get the complete file */
        $extension = (isset($_REQUEST['extension'])) ? $_REQUEST['extension'] : 'default';
        $this->download_content($cachedentry, $extension);

        die();
    }

    public function download_content(CacheNode $cachedentry, $extension = 'default') {

        $temporarily_link = $this->get_temporarily_link($cachedentry, $extension);

        /* Download Hook */
        do_action('shareonedrive_download', $cachedentry, $temporarily_link);


        $event_type = (isset($_REQUEST['action']) && $_REQUEST['action'] === 'shareonedrive-stream') ? 'shareonedrive_streamed_entry' : 'shareonedrive_downloaded_entry';
        do_action('shareonedrive_log_event', $event_type, $cachedentry);

        header('Location: ' . $temporarily_link);

        die();
    }

    public function stream_entry() {
        /* Check if file is cached and still valid */
        $cached = $this->get_cache()->is_cached($this->get_processor()->get_requested_entry());

        if ($cached === false) {
            $cachedentry = $this->get_entry($this->get_processor()->get_requested_entry());
        } else {
            $cachedentry = $cached;
        }

        if ($cachedentry === false) {
            die();
        }

        $entry = $cachedentry->get_entry();

        $extension = $entry->get_extension();
        $allowedextensions = array('mp4', 'm4v', 'ogg', 'ogv', 'webmv', 'mp3', 'm4a', 'ogg', 'oga');

        if (empty($extension) || !in_array($extension, $allowedextensions)) {
            die();
        }

        $this->download_entry();
    }

    /*
     * Create zipfile
     */

    public function download_entries_as_zip() {

        $cachedfolder = $this->get_folder();

        if ($cachedfolder === false || $cachedfolder['folder'] === false) {
            return new WP_Error('broke', __("Requested directory isn't allowed", 'shareonedrive'));
        }

        $folder = $cachedfolder['folder'];

        if (isset($_REQUEST['files'])) {
            $requested_ids = $_REQUEST['files'];
        } else {
            $requested_ids = array($folder->get_id());
        }

        /* Set Zip file name */
        $zip_filename = '_zip_' . basename($folder->get_name()) . '_' . uniqid() . '.zip';

        /* Load Zip Library */
        if (!function_exists('PHPZip\autoload')) {
            require_once "PHPZip/autoload.php";
        } else {
            error_log('[Share-one-Drive message]: ' . sprintf('No valid ZIP library found on line %s', __LINE__));
            die(-1);
        }

        /* Create Zip file */
        $zip = new \PHPZip\Zip\Stream\ZipStream(Helpers::filter_filename($zip_filename));


        /* Process all the files that need to be added to the zip file */
        $files_added_to_zip = array();
        foreach ($requested_ids as $requested_id) {

            $cached_entry = $this->get_entry($requested_id);

            if ($cached_entry === false) {
                continue;
            }

            $entry = $cached_entry->get_entry();
            $entries_to_add = array();

            if ($entry->is_dir()) {
                $new_entries_to_add = $this->_get_folder_recursive($cached_entry);
                $entries_to_add = array_merge($entries_to_add, $new_entries_to_add);
            } else {
                $entries_to_add[] = $cached_entry;
            }

            foreach ($entries_to_add as $cached_entry_to_add) {
                $zip = $this->_add_entry_to_zip($zip, $cached_entry_to_add);
                $files_added_to_zip[] = $cached_entry_to_add;

                do_action('shareonedrive_log_event', 'shareonedrive_downloaded_entry', $cached_entry_to_add, array('as_zip' => true));
            }
        }

        /* Close zip */
        $result = $zip->finalize();

        /* Send email if needed */
        if ($this->get_processor()->get_shortcode_option('notificationdownload') === '1') {
            $this->get_processor()->send_notification_email('download', $files_added_to_zip);
        }

        /* Download Zip Hook */
        do_action('shareonedrive_download_zip', $files_added_to_zip);

        die();
    }

    public function _add_entry_to_zip(\PHPZip\Zip\Stream\ZipStream $zip, CacheNode $cached_entry) {
        $relative_path = $cached_entry->get_path($this->get_processor()->get_last_folder());

        if ($cached_entry->get_entry()->is_dir()) {
            $zip->addDirectory(ltrim($relative_path, '/'));
        } else {

            /* Download the File */
            /* Update the time_limit as this can take a while */
            @set_time_limit(60);


            /* Get Download Url */
            $download_url = $this->get_temporarily_link($cached_entry);

            if ($download_url === false) {
                return $zip;
            }

            /* Get file */
            $request = new \SODOneDrive_Http_Request($download_url, 'GET');

            try {
                $httpRequest = $this->get_app()->get_client()->getAuth()->authenticatedRequest($request);
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return $zip;
            }

            if ($httpRequest->getResponseHttpCode() == 200) {
                ( ob_get_level() > 0 ) ? ob_flush() : flush();
                $stream = fopen('php://temp', 'r+');
                fwrite($stream, $httpRequest->getResponseBody());
                rewind($stream);

                /* Add file contents to zip */
                try {
                    $zip->addLargeFile($stream, ltrim($relative_path, '/'), $cached_entry->get_entry()->get_last_edited(), $cached_entry->get_entry()->get_description());
                } catch (\Exception $ex) {
                    error_log('[Share-one-Drive message]: ' . sprintf('ZIP Error on line %s: %s', __LINE__, $ex->getMessage()));
                    fclose($stream);
                    /* To Do Log */
                }

                fclose($stream);
            }
        }
        return $zip;
    }

    private function _get_folder_recursive(CacheNode $cached_entry, $list_of_cached_entries = array()) {

        if ($this->get_processor()->_is_entry_authorized($cached_entry) === false) {
            return $list_of_cached_entries;
        }

        if ($cached_entry->get_entry()->is_file()) {
            $list_of_cached_entries[$cached_entry->get_id()] = $cached_entry;
            return $list_of_cached_entries;
        }

        $result = $this->get_folder($cached_entry->get_id());
        if (empty($result)) {
            return $list_of_cached_entries;
        }

        $cached_folder = $result['folder'];

        if ($cached_folder->has_children() === false) {
            return $list_of_cached_entries;
        }

        foreach ($cached_folder->get_children() as $cached_child_entry) {
            $new_of_cached_entries = $this->_get_folder_recursive($cached_child_entry, $list_of_cached_entries);
            $list_of_cached_entries = array_merge($list_of_cached_entries, $new_of_cached_entries);
        }

        return $list_of_cached_entries;
    }

    public function has_temporarily_link($cached_entry, $extension = 'default') {
        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if ($cached_entry !== false) {
            if ($temporarily_link = $cached_entry->get_temporarily_link($extension)) {
                return true;
            }
        }

        return false;
    }

    public function get_temporarily_link($cached_entry, $extension = 'default') {
        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        /* 1: Get Temporarily link from cache */
        if ($cached_entry !== false) {
            if ($temporarily_link = $cached_entry->get_temporarily_link($extension)) {
                return $temporarily_link;
            }
        }

        /* 2: Get Temporarily link from entry itself */
        $direct_download_link = $cached_entry->get_entry()->get_direct_download_link();
        if (!empty($direct_download_link) && $extension === 'default') {
            $cached_entry->add_temporarily_link($direct_download_link, $extension);
            $this->get_cache()->set_updated();
            return $cached_entry->get_temporarily_link($extension);
        }


        /* 3: Get Temporarily link via API */
        try {
            /* Get a Download link via the Graph API */
            if ($extension === 'default') {
                $url = $this->get_app()->get_drive()->items->download($cached_entry->get_id());
            } else {
                $url = $this->get_app()->get_drive()->items->export($cached_entry->get_id(), array('format' => $extension));
            }

            if (!empty($url)) {
                $cached_entry->add_temporarily_link($url, $extension);
            } else {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__));
                return false;
            }
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
            return false;
        }

        $this->get_cache()->set_updated();
        return $cached_entry->get_temporarily_link($extension);
    }

    public function has_shared_link($cached_entry, $mode = 'view', $scope = false) {

        if ($scope === false) {
            $scope = ($this->get_app()->get_account_type() === 'personal') ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if ($cached_entry !== false) {
            if ($shared_link = $cached_entry->get_shared_link($mode, $scope)) {
                return true;
            }
        }

        return false;
    }

    public function get_shared_link($cached_entry, $type = 'view', $scope = false) {

        if ($scope === false) {
            $scope = ($this->get_app()->get_account_type() === 'personal') ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if ($cached_entry !== false) {
            if ($shared_link = $cached_entry->get_shared_link($type, $scope)) {
                do_action('shareonedrive_log_event', 'shareonedrive_created_link_to_entry', $cached_entry, array('url' => $shared_link));
                return $shared_link;
            }
        }

        $shared_link = $this->create_shared_link($cached_entry, $type, $scope);
        do_action('shareonedrive_log_event', 'shareonedrive_created_link_to_entry', $cached_entry, array('url' => $shared_link));

        return $shared_link;
    }

    public function get_shared_link_for_output($entry_id = false) {

        $cached_entry = $this->get_entry($entry_id);

        if ($cached_entry === false) {
            die(-1);
        }

        $entry = $cached_entry->get_entry();

        $shared_link = $this->get_shared_link($cached_entry, 'view');
        $embed_link = $this->get_embedded_link($cached_entry);

        $resultdata = array(
            'name' => $entry->get_name(),
            'extension' => $entry->get_extension(),
            'link' => $this->shorten_url($cached_entry, $shared_link),
            'embeddedlink' => $this->shorten_url($cached_entry, $embed_link),
            'size' => Helpers::bytes_to_size_1024($entry->get_size()),
            'error' => false
        );

        return $resultdata;
    }

    public function create_shared_link($cached_entry, $type = 'view', $scope = false) {

        if ($scope === false) {
            $scope = ($this->get_app()->get_account_type() === 'personal') ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        $params = array(
            'type' => $type,
            'scope' => $scope
        );

        try {
            $permission = $this->get_app()->get_drive()->items->createlink($cached_entry->get_id(), $params);
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
            return false;
        }

        $shared_link = $permission->getLink();
        $url = $shared_link->getWebUrl();

        /* Get Expire date from url */
        $url_array = parse_url($url);

        $expires = false;
        if (isset($url_array['query'])) {
            parse_str($url_array['query'], $url_attributes);
            $expires = isset($url_attributes['expiration']) ? $url_attributes['expiration'] : false;
        }

        $cached_entry->add_shared_link($url, $type, $scope, $expires);

        $this->get_cache()->set_updated();

        do_action('shareonedrive_log_event', 'shareonedrive_updated_metadata', $cached_entry, array('metadata_field' => 'Sharing Permissions'));

        return $url;
    }

    public function get_embedded_link($cached_entry, $scope = false) {

        if ($scope === false) {
            $scope = ($this->get_app()->get_account_type() === 'personal') ? 'anonymous' : $this->get_processor()->get_setting('link_scope');
        }

        if ($cached_entry instanceof Entry) {
            $cached_entry = $this->get_entry($cached_entry->get_id());
        }

        if ($cached_entry->get_entry()->get_can_preview_by_cloud() === false) {
            return false;
        }

        /* For images, just return the actual file
         * BUG in API: embedded url of image files don't work
         */
        if (in_array($cached_entry->get_entry()->get_extension(), array('jpg', 'jpeg', 'gif', 'png'))) {
            return SHAREONEDRIVE_ADMIN_URL . "?action=shareonedrive-embed-image&id=" . $cached_entry->get_id();
        }

        /* Only OneDrive personal Accounts can create embedded links to documents 
         * For the other accounts we need to add 'action=embedview' to the view url
         */

        if ($this->get_app()->get_account_type() === 'personal') {

            $embed_supported = array('csv', 'doc', 'docx', 'odp', 'ods', 'odt', 'pot', 'potm', 'potx', 'pps', 'ppsx', 'ppsxm', 'ppt', 'pptm', 'pptx', 'rtf', 'xls', 'xlsx', 'pdf');
            $embed = (in_array($cached_entry->get_entry()->get_extension(), $embed_supported));

            $embedded_link = $this->get_shared_link($cached_entry, 'embed', $scope);
            $embedded_link = str_replace('redir?', 'embed?', $embedded_link);

            if ($embed) {
                $embedded_link .= (strpos($embedded_link, '&em=2') === false) ? '&em=2' : ''; // Open embedded file directly
                $embedded_link .= '&wdHideHeaders=True';
                $embedded_link .= '&wdDownloadButton=False';
            }
            return $embedded_link;
        }

        /* Use the Google Doc viewer to preview pdf files in Business Accounts as embedding is not supported */
        if ($cached_entry->get_entry()->get_extension() === 'pdf') {
            $shared_link = $this->get_shared_link($cached_entry);
            if ($this->get_processor()->get_setting('allow_google_viewer') === 'Yes') {
                $embedded_link = 'https://docs.google.com/viewerng/viewer?embedded=true&url=' . urlencode($shared_link);
            } else {
                /* no embedded link */
                $embedded_link = '';
            }
            return $embedded_link;
        }

        $embedded_link = $this->get_shared_link($cached_entry, 'view', $scope);
        $embedded_link .= (strpos($embedded_link, '?')) ? '&action=embedview' : '?action=embedview';
        return $embedded_link;
    }

    public function shorten_url(CacheNode $cached_entry, $url) {
        try {
            switch ($this->get_processor()->get_setting('shortlinks')) {

                case 'Bit.ly';
                    require_once 'bitly/bitly.php';

                    $this->bitly = new \Bitly($this->get_processor()->get_setting('bitly_login'), $this->get_processor()->get_setting('bitly_apikey'));
                    $response = $this->bitly->shorten($url);
                    return $response['url'];

                case 'Shorte.st';
                    $request = new \SODOneDrive_Http_Request('https://api.shorte' . '.st/s/' . $this->get_processor()->get_setting('shortest_apikey') . '/' . $url, 'GET');
                    $httpRequest = $this->get_library()->execute($request);
                    return $httpRequest['shortenedUrl'];

                case 'Rebrandly';
                    $request = new \SODOneDrive_Http_Request('https://api.rebrandly.com/v1/links', 'POST', array('apikey' => $this->get_processor()->get_setting('rebrandly_apikey'), 'Content-Type' => 'application/json', 'workspace' => $this->get_processor()->get_setting('rebrandly_workspace')), json_encode(array('title' => $cached_entry->get_name(), 'destination' => $url, 'domain' => array('fullName' => $this->get_processor()->get_setting('rebrandly_domain')))));
                    $httpRequest = $this->get_library()->execute($request);
                    return 'https://' . $httpRequest['shortUrl'];

                case 'None':
                default:
                    break;
            }
        } catch (\Exception $ex) {
            error_log('[Share-one-Drive message]: ' . sprintf('API Error on line %s: %s', __LINE__, $ex->getMessage()));
            return $url;
        }

        return $url;
    }

    /* Pull for changes */

    public function get_changes($change_token = false) {
        $root_folder = $this->get_root_folder();

        if (empty($change_token)) {
            try {
                $result = $this->get_app()->get_drive()->changes->getlatest($root_folder->get_id());
                preg_match("/delta.token=\'?([a-zA-Z0-9;%\-=_]+)\'?/", $result['@odata.deltaLink'], $matches);
                $new_change_token = $result['@odata.deltaLink']; //$matches[1];
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return false;
            }
        }

        $changes = array();

        while ($change_token != null) {
            try {

                $expected_class = 'SODOneDrive_Service_Drive_Changes';

                $httpRequest = new \SODOneDrive_Http_Request($change_token, 'GET');
                $httpRequest = $this->get_app()->get_client()->getAuth()->sign($httpRequest);
                $httpRequest->setExpectedClass('SODOneDrive_Service_Drive_Changes');
                $result = $this->get_app()->get_client()->execute($httpRequest);

                if (isset($result['@odata.nextLink'])) {
                    $change_token = $result['@odata.nextLink'];
                } else {
                    $change_token = null;
                    $new_change_token = $result['@odata.deltaLink'];
                }

                $changes = array_merge($changes, $result->getValue());
            } catch (\Exception $ex) {
                error_log('[Share-one-Drive message]: ' . sprintf('Graph API Error on line %s: %s', __LINE__, $ex->getMessage()));
                return false;
            }
        }

        $list_of_update_entries = array();
        foreach ($changes as $change) {

            if ($change->getName() === 'root') {
                continue;
            }

            /* File is removed */
            if ($change->getDeleted() !== null) {
                $list_of_update_entries[$change->getId()] = 'deleted';
            } else {
                /* File is updated */
                $list_of_update_entries[$change->getId()] = new Entry($change);
            }
        }


        return array($new_change_token, $list_of_update_entries);
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Processor
     */
    public function get_processor() {
        return $this->_processor;
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\Cache
     */
    public function get_cache() {
        return $this->get_processor()->get_cache();
    }

    /**
     * 
     * @return \TheLion\ShareoneDrive\App
     */
    public function get_app() {
        return $this->_app;
    }

    /**
     * 
     * @return \Graph_Client
     */
    public function get_library() {
        return $this->_app->get_client();
    }

}
