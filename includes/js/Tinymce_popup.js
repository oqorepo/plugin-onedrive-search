jQuery(document).ready(function ($) {
  'use strict';

  /* Tabs*/
  $('ul.shareonedrive-nav-tabs li:not(".disabled")').click(function () {
    if ($(this).hasClass('disabled')) {
      return false;
    }
    var tab_id = $(this).attr('data-tab');

    $('ul.shareonedrive-nav-tabs  li').removeClass('current');
    $('.shareonedrive-tab-panel').removeClass('current');

    $(this).addClass('current');
    $("#" + tab_id).addClass('current');
    var hash = location.hash.replace('#', '');
    location.hash = tab_id;
    window.scrollTo(0, 0);
    window.parent.scroll(0, 0);
  });

  if (location.hash && location.hash.indexOf('TB_inline') < 0) {
    jQuery("ul.shareonedrive-nav-tabs " + location.hash + "_tab").trigger('click');
  }

  /* Fix for not scrolling popup*/
  if (/Android|webOS|iPhone|iPod|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    var parent = $(tinyMCEPopup.getWin().document);

    if (parent.find('#safari_fix').length === 0) {
      parent.find('.mceWrapper iframe').wrap(function () {
        return $('<div id="safari_fix"/>').css({
          'width': "100%",
          'height': "100%",
          'overflow': 'auto',
          '-webkit-overflow-scrolling': 'touch'
        });
      });
    }
  }

  /* Set mode */
  var mode = $('body').attr('data-mode');

  $("input[name=mode]:radio").change(function () {

    $('.forfilebrowser, .forgallery, .foraudio, .forvideo, .forsearch').hide();
    $("#ShareoneDrive_linkedfolders").trigger('change');

    $('#settings_upload_tab, #settings_advanced_tab, #settings_manipulation_tab, #settings_notifications_tab, #settings_mediafiles_tab, #settings_layout_tab, #settings_sorting_tab, #settings_exclusions_tab').removeClass('disabled');
    $('.download-options').show();

    mode = $(this).val();
    switch (mode) {
      case 'files':
        $('.forfilebrowser').not('.hidden').show();
        $('#settings_mediafiles_tab').addClass('disabled');
        break;

      case'upload':
        $('.foruploadbox').not('.hidden').show();
        $('#settings_upload_tab, #settings_notifications_tab').removeClass('disabled');
        $('#settings_mediafiles_tab, #settings_sorting_tab, #settings_advanced_tab, #settings_exclusions_tab, #settings_manipulation_tab').addClass('disabled');
        $('.download-options').hide();
        $('#ShareoneDrive_upload').prop("checked", true).trigger('change');
        $('#ShareoneDrive_notificationdownload, #ShareoneDrive_notificationdeletion').closest('.option').hide();
        break;

      case 'search':
        $('.forsearch').not('.hidden').show();
        $('#settings_mediafiles_tab').addClass('disabled');
        $('#settings_upload_tab').addClass('disabled');
        $('#ShareoneDrive_search_field').prop("checked", true).trigger('change');
        break;

      case 'gallery':
        $('.forgallery').show();
        $('#settings_mediafiles_tab').addClass('disabled');
        break;

      case 'audio':
        $('.foraudio').show();
        $('.root-folder').show();
        $('#settings_mediafiles_tab').removeClass('disabled');
        $('#settings_upload_tab, #settings_advanced_tab, #settings_manipulation_tab, #settings_notifications_tab').addClass('disabled');
        break;

      case 'video':
        $('.forvideo').show();
        $('.root-folder').show();
        $('#settings_mediafiles_tab').removeClass('disabled');
        $('#settings_upload_tab, #settings_advanced_tab, #settings_manipulation_tab, #settings_notifications_tab').addClass('disabled');
        break;
    }

    $("#ShareoneDrive_breadcrumb, #ShareoneDrive_showcolumnnames, #ShareoneDrive_mediapurchase, #ShareoneDrive_search, #ShareoneDrive_showfiles, #ShareoneDrive_slideshow, #ShareoneDrive_upload, #ShareoneDrive_upload_convert, #ShareoneDrive_rename, #ShareoneDrive_move, #ShareoneDrive_editdescription, #ShareoneDrive_delete, #ShareoneDrive_addfolder").trigger('change');
    $('input[name=ShareoneDrive_file_layout]:radio:checked').trigger('change').prop('checked', true);
    $('#ShareoneDrive_linkedfolders').trigger('change');
  });

  $("input[name=ShareoneDrive_file_layout]:radio").change(function () {
    switch ($(this).val()) {
      case 'grid':
        $('.forlistonly').fadeOut();
        break;
      case 'list':
        $('.forlistonly').fadeIn();
        break;
    }
  });

  $('[data-div-toggle]').change(function () {
    var toggleelement = '.' + $(this).attr('data-div-toggle');

    if ($(this).is(":checkbox")) {
      if ($(this).is(":checked")) {
        $(toggleelement).fadeIn().removeClass('hidden');
      } else {
        $(toggleelement).fadeOut().addClass('hidden');
      }
    } else if ($(this).is("select")) {
      if ($(this).val() === $(this).attr('data-div-toggle-value')) {
        $(toggleelement).fadeIn().removeClass('hidden');
      } else {
        $(toggleelement).fadeOut().addClass('hidden');
      }
    }
  });

  $("#ShareoneDrive_linkedfolders").change(function () {
    $('input[name=ShareoneDrive_userfolders_method]:radio:checked').trigger('change').prop('checked', true);
  });

  $("input[name=ShareoneDrive_userfolders_method]:radio").change(function () {
    var is_checked = $("#ShareoneDrive_linkedfolders").is(":checked");

    switch ($(this).val()) {
      case 'manual':
        if (is_checked) {
          $('.root-folder').hide();
        }
        $('.option-userfolders_auto').hide().addClass('hidden');
        break;
      case 'auto':
        $('.root-folder').show();
        $('.option-userfolders_auto').show().removeClass('hidden');
        break;
    }
  });

  $("input[name=sort_field]:radio").change(function () {
    switch ($(this).val()) {
      case 'shuffle':
        $('.option-sort-field').hide();
        break;
      default:
        $('.option-sort-field').show();
        break;
    }
  });

  $('.shareonedrive .get_shortcode').click(showRawShortcode);
  $(".shareonedrive .insert_links").click(createDirectLinks);
  $(".shareonedrive .insert_embedded").click(insertEmbedded);
  $('.shareonedrive .insert_shortcode').click(function (event) {
    insertShareoneDriveShortCode(event)
  });
  $('.shareonedrive .insert_shortcode_gf').click(function (event) {
    insertShareoneDriveShortCodeGF(event)
  });

  $('.shareonedrive  .insert_shortcode_cf').click(function (event) {
    insertShareoneDriveShortCodeCF(event)
  });

  $('.shareonedrive  .insert_shortcode_woocommerce').click(function (event) {
    insertShareoneDriveShortCodeWC(event)
  });

  $('.shareonedrive .list-container').on('click', '.entry_media_shortcode', function (event) {
    insertShareoneDriveShortCodeMedia($(this).closest('.entry'));
  });


  $(".ShareoneDrive img.preloading").unveil(200, $(".ShareoneDrive .ajax-filelist"), function () {
    $(this).load(function () {
      $(this).removeClass('preloading');
    });
  });

  /* Initialise from shortcode */
  $('input[name=mode]:radio:checked').trigger('change').prop('checked', true);

  function createShortcode() {
    var dir = $(".root-folder .ShareoneDrive.files").attr('data-id'),
            custom_class = $('#ShareoneDrive_class').val(),
            linkedfolders = $('#ShareoneDrive_linkedfolders').prop("checked"),
            show_files = $('#ShareoneDrive_showfiles').prop("checked"),
            max_files = $('#ShareoneDrive_maxfiles').val(),
            show_folders = $('#ShareoneDrive_showfolders').prop("checked"),
            show_filesize = $('#ShareoneDrive_filesize').prop("checked"),
            show_filedate = $('#ShareoneDrive_filedate').prop("checked"),
            filelayout = $("input[name=ShareoneDrive_file_layout]:radio:checked").val(),
            show_ext = $('#ShareoneDrive_showext').prop("checked"),
            show_columnnames = $('#ShareoneDrive_showcolumnnames').prop("checked"),
            candownloadzip = $('#ShareoneDrive_candownloadzip').prop("checked"),
            showsharelink = $('#ShareoneDrive_showsharelink').prop("checked"),
            showrefreshbutton = $('#ShareoneDrive_showrefreshbutton').prop("checked"),
            show_breadcrumb = $('#ShareoneDrive_breadcrumb').prop("checked"),
            breadcrumb_roottext = $('#ShareoneDrive_roottext').val(),
            search = $('#ShareoneDrive_search').prop("checked"),
            search_from = $('#ShareoneDrive_searchfrom').prop("checked"),
            previewinline = $('#ShareoneDrive_previewinline').prop("checked"),
            allow_preview = $('#ShareoneDrive_allow_preview').prop("checked"),
            onclick = $("input[name=ShareoneDrive_onclick]:radio:checked").val(),
            include_ext = $('#ShareoneDrive_include_ext').val(),
            include = $('#ShareoneDrive_include').val(),
            exclude_ext = $('#ShareoneDrive_exclude_ext').val(),
            exclude = $('#ShareoneDrive_exclude').val(),
            sort_field = $("input[name=sort_field]:radio:checked").val(),
            sort_order = $("input[name=sort_order]:radio:checked").val(),
            slideshow = $('#ShareoneDrive_slideshow').prop("checked"),
            pausetime = $('#ShareoneDrive_pausetime').val(),
            maximages = $('#ShareoneDrive_maximage').val(),
            show_filenames = $('#ShareoneDrive_showfilenames').prop("checked"),
            target_height = $('#ShareoneDrive_targetHeight').val(),
            max_width = $('#ShareoneDrive_max_width').val(),
            max_height = $('#ShareoneDrive_max_height').val(),
            upload = $('#ShareoneDrive_upload').prop("checked"),
            upload_folder = $('#ShareoneDrive_upload_folder').prop("checked"),
            overwrite = $('#ShareoneDrive_overwrite').prop("checked"),
            upload_ext = $('#ShareoneDrive_upload_ext').val(),
            maxfilesize = $('#ShareoneDrive_maxfilesize').val(),
            maxnumberofuploads = $('#ShareoneDrive_maxnumberofuploads').val(),
            edit = $('#ShareoneDrive_edit').prop("checked"),
            rename = $('#ShareoneDrive_rename').prop("checked"),
            move = $('#ShareoneDrive_move').prop("checked"),
            editdescription = $('#ShareoneDrive_editdescription').prop("checked"),
            can_delete = $('#ShareoneDrive_delete').prop("checked"),
            can_addfolder = $('#ShareoneDrive_addfolder').prop("checked"),
            notification_download = $('#ShareoneDrive_notificationdownload').prop("checked"),
            notification_upload = $('#ShareoneDrive_notificationupload').prop("checked"),
            notification_deletion = $('#ShareoneDrive_notificationdeletion').prop("checked"),
            notification_emailaddress = $('#ShareoneDrive_notification_email').val(),
            notification_skip_email_currentuser = $('#ShareoneDrive_notification_skip_email_currentuser').prop("checked"),
            user_folders = $('#ShareoneDrive_user_folders').prop("checked"),
            use_template_dir = $('#ShareoneDrive_userfolders_template').prop("checked"),
            template_dir = $(".template-folder .ShareoneDrive.files").attr('data-id'),
            maxuserfoldersize = $('#ShareoneDrive_maxuserfoldersize').val(),
            view_role = readCheckBoxes("input[name='ShareoneDrive_view_role[]']"),
            preview_role = readCheckBoxes("input[name='ShareoneDrive_preview_role[]']"),
            download_role = readCheckBoxes("input[name='ShareoneDrive_download_role[]']"),
            edit_role = readCheckBoxes("input[name='ShareoneDrive_edit_role[]']"),
            share_role = readCheckBoxes("input[name='ShareoneDrive_share_role[]']"),
            upload_role = readCheckBoxes("input[name='ShareoneDrive_upload_role[]']"),
            rename_role = readCheckBoxes("input[name='ShareoneDrive_rename_role[]']"),
            move_role = readCheckBoxes("input[name='ShareoneDrive_move_role[]']"),
            editdescription_role = readCheckBoxes("input[name='ShareoneDrive_editdescription_role[]']"),
            delete_role = readCheckBoxes("input[name='ShareoneDrive_delete_role[]']"),
            addfolder_role = readCheckBoxes("input[name='ShareoneDrive_addfolder_role[]']"),
            view_user_folders_role = readCheckBoxes("input[name='ShareoneDrive_view_user_folders_role[]']"),
            mediaextensions = readCheckBoxes("input[name='ShareoneDrive_mediaextensions[]']"),
            autoplay = $('#ShareoneDrive_autoplay').prop("checked"),
            showplaylist = $('#ShareoneDrive_showplaylist').prop("checked"),
            covers = $('#ShareoneDrive_covers').prop("checked"),
            linktomedia = $('#ShareoneDrive_linktomedia').prop("checked"),
            mediapurchase = $('#ShareoneDrive_mediapurchase').prop("checked"),
            linktoshop = $('#ShareoneDrive_linktoshop').val();

    var data = '';

    if (ShareoneDrive_vars.shortcodeRaw === '1') {
      data += '[raw]';
    }

    data += '[shareonedrive ';

    if (custom_class !== '') {
      data += 'class="' + custom_class + '" ';
    }

    if (typeof dir === 'undefined' && ($("input[name=ShareoneDrive_userfolders_method]:radio:checked").val() !== 'manual')) {
      $('#settings_folder_tab a').trigger('click');
      return false;
    }

    if (dir !== '') {
      if (linkedfolders) {
        if ($("input[name=ShareoneDrive_userfolders_method]:radio:checked").val() !== 'manual') {
          data += 'dir="' + dir + '" ';
        }
      } else {
        data += 'dir="' + dir + '" ';
      }
    }

    if (max_width !== '') {
      if (max_width.indexOf("px") !== -1 || max_width.indexOf("%") !== -1) {
        data += 'maxwidth="' + max_width + '" ';
      } else {
        data += 'maxwidth="' + parseInt(max_width) + '" ';
      }
    }

    if (max_height !== '') {
      if (max_height.indexOf("px") !== -1 || max_height.indexOf("%") !== -1) {
        data += 'maxheight="' + max_height + '" ';
      } else {
        data += 'maxheight="' + parseInt(max_height) + '" ';
      }
    }

    data += 'mode="' + $("input[name=mode]:radio:checked").val() + '" ';

    if (include_ext !== '') {
      data += 'includeext="' + include_ext + '" ';
    }

    if (include !== '') {
      data += 'include="' + include + '" ';
    }

    if (exclude_ext !== '') {
      data += 'excludeext="' + exclude_ext + '" ';
    }

    if (exclude !== '') {
      data += 'exclude="' + exclude + '" ';
    }

    if (view_role !== 'administrator|editor|author|contributor|subscriber|pending|guest') {
      data += 'viewrole="' + view_role + '" ';
    }

    if (sort_field !== 'name') {
      data += 'sortfield="' + sort_field + '" ';
    }

    if (sort_field !== 'shuffle' && sort_order !== 'asc') {
      data += 'sortorder="' + sort_order + '" ';
    }

    if (linkedfolders === true) {
      var method = $("input[name=ShareoneDrive_userfolders_method]:radio:checked").val();
      data += 'userfolders="' + method + '" ';

      if (method === 'auto' && use_template_dir === true && template_dir !== '') {
        data += 'usertemplatedir="' + template_dir + '" ';
      }

      if (view_user_folders_role !== 'administrator') {
        data += 'viewuserfoldersrole="' + view_user_folders_role + '" ';
      }

      if (maxuserfoldersize !== '-1' && maxuserfoldersize !== '') {
        data += 'maxuserfoldersize="' + maxuserfoldersize + '" ';
      }

    }

    if (mode === 'upload') {
      data += 'downloadrole="none" ';
    } else if (download_role !== 'administrator|editor|author|contributor|subscriber|guest') {
      data += 'downloadrole="' + download_role + '" ';
    }

    var mode = $("input[name=mode]:radio:checked").val();
    switch (mode) {
      case 'audio':
      case 'video':

        if (mediaextensions === 'none') {
          $('#settings_mediafiles_tab a').trigger('click');
          $('[name="ShareoneDrive_mediaextensions[]"]').next().css("color", "red");
          return false;
        }
        data += 'mediaextensions="' + mediaextensions + '" ';

        if (autoplay === true) {
          data += 'autoplay="1" ';
        }

        if (showplaylist === false) {
          data += 'hideplaylist="1" ';
        }

        if (covers === true) {
          data += 'covers="1" ';
        }

        if (linktomedia === true) {
          data += 'linktomedia="1" ';
        }

        if (mediapurchase === true && linktoshop !== '') {
          data += 'linktoshop="' + linktoshop + '" ';
        }

        break;

      case 'files':
      case 'gallery':
      case 'upload':
      case 'search':
        if (mode === 'gallery') {

          if (maximages !== '') {
            data += 'maximages="' + maximages + '" ';
          }

          if (show_filenames === true) {
            data += 'showfilenames="1" ';
          }

          if (target_height !== '') {
            data += 'targetheight="' + target_height + '" ';
          }

          if (slideshow === true) {
            data += 'slideshow="1" ';
            if (pausetime !== '') {
              data += 'pausetime="' + pausetime + '" ';
            }
          }
        }

        if (mode === 'files' || mode === 'search') {

          if (show_filesize === false) {
            data += 'filesize="0" ';
          }

          if (show_filedate === false) {
            data += 'filedate="0" ';
          }

          if (filelayout === 'list') {
            data += 'filelayout="list" ';
          }

          if (show_ext === false) {
            data += 'showext="0" ';
          }

          if (allow_preview === false) {
            data += 'allowpreview="0" ';
            if (onclick === 'preview') {
              onclick === 'download';
            }
          } else if (preview_role !== 'all') {
            data += 'previewrole="' + preview_role + '" ';
          }

          if (show_columnnames === false) {
            data += 'showcolumnnames="0" ';
          }

          if (edit === true) {
            data += 'edit="1" ';

            if (edit_role !== 'administrator|editor|author') {
              data += 'editrole="' + edit_role + '" ';
            }
          }

          if (onclick !== 'preview') {
            data += 'onclick="' + onclick + '" ';
          }
        }

        if (show_files === false) {
          data += 'showfiles="0" ';
        }
        if (show_folders === false) {
          data += 'showfolders="0" ';
        }

        if (max_files !== '-1' && max_files !== '') {
          data += 'maxfiles="' + max_files + '" ';
        }

        if (maxnumberofuploads !== '-1' && maxnumberofuploads !== '0' && maxnumberofuploads !== '') {
          data += 'maxnumberofuploads="' + maxnumberofuploads + '" ';
        }

        if (previewinline === false) {
          data += 'previewinline="0" ';
        }
        if (candownloadzip === true) {
          data += 'candownloadzip="1" ';
        }

        if (showsharelink === true) {
          data += 'showsharelink="1" ';

          if (share_role !== 'all') {
            data += 'sharerole="' + share_role + '" ';
          }

        }

        if (showrefreshbutton === false) {
          data += 'showrefreshbutton="0" ';
        }

        if (search === false && mode !== 'search') {
          data += 'search="0" ';
        } else {
          if (search_from === true) {
            data += 'searchfrom="selectedroot" ';
          }
        }

        if (show_breadcrumb === true) {
          if (breadcrumb_roottext !== '') {
            data += 'roottext="' + breadcrumb_roottext + '" ';
          }
        } else {
          data += 'showbreadcrumb="0" ';
        }

        if (notification_download === true || notification_upload === true || notification_deletion === true) {
          if (notification_emailaddress !== '') {
            data += 'notificationemail="' + notification_emailaddress + '" ';
          }

          if (notification_skip_email_currentuser === true) {
            data += 'notification_skipemailcurrentuser="1" ';
          }
        }

        if (notification_download === true) {
          data += 'notificationdownload="1" ';
        }

        if (upload === true) {
          data += 'upload="1" ';

          if (upload_folder === false) {
            data += 'upload_folder="0" ';
          }

          if (upload_role !== 'administrator|editor|author|contributor|subscriber') {
            data += 'uploadrole="' + upload_role + '" ';
          }
          if (maxfilesize !== '') {
            data += 'maxfilesize="' + maxfilesize + '" ';
          }

          if (overwrite === true) {
            data += 'overwrite="1" ';
          }

          if (upload_ext !== '') {
            data += 'uploadext="' + upload_ext + '" ';
          }

          if (notification_upload === true) {
            data += 'notificationupload="1" ';
          }

        }

        if (rename === true) {
          data += 'rename="1" ';

          if (rename_role !== 'administrator|editor') {
            data += 'renamerole="' + rename_role + '" ';
          }
        }

        if (move === true) {
          data += 'move="1" ';

          if (move_role !== 'administrator|editor') {
            data += 'moverole="' + move_role + '" ';
          }
        }

        if (editdescription === true) {
          data += 'editdescription="1" ';

          if (editdescription_role !== 'administrator|editor') {
            data += 'editdescriptionrole="' + editdescription_role + '" ';
          }
        }

        if (can_delete === true) {
          data += 'delete="1" ';

          if (delete_role !== 'administrator|editor') {
            data += 'deleterole="' + delete_role + '" ';
          }

          if (notification_deletion === true) {
            data += 'notificationdeletion="1" ';
          }

        }

        if (can_addfolder === true) {
          data += 'addfolder="1" ';

          if (addfolder_role !== 'administrator|editor') {
            data += 'addfolderrole="' + addfolder_role + '" ';
          }
        }

        break;
    }

    data += ']';

    if (ShareoneDrive_vars.shortcodeRaw === '1') {
      data += '[/raw]';
    }

    return data;
  }

  function insertShareoneDriveShortCode(event) {
    var data = createShortcode();
    event.preventDefault();


    if (data !== false) {
      tinyMCEPopup.execCommand('mceInsertContent', false, data);
      // Refocus in window
      if (tinyMCEPopup.isWindow)
        window.focus();
      tinyMCEPopup.editor.focus();
      tinyMCEPopup.close();
    }
  }

  function insertShareoneDriveShortCodeGF(event) {
    event.preventDefault();

    var data = createShortcode();
    if (data !== false) {
      $('#field_shareonedrive', window.parent.document).val(data);
      window.parent.SetFieldProperty('ShareoneDriveShortcode', data);
      window.parent.tb_remove();
    }
  }

  function insertShareoneDriveShortCodeCF(event) {
    event.preventDefault();

    var data = createShortcode();
    if (data !== false) {
      var encoded_data = window.btoa(unescape(encodeURIComponent(data)));

      $('.shareonedrive-shortcode-value', window.parent.document).val(encoded_data);
      window.parent.jQuery('.shareonedrive-shortcode-value').trigger('change');

      if (data.indexOf('userfolders="auto"') > -1) {
        $('.share-one-drive-upload-folder', window.parent.document).fadeIn();
      } else {
        $('.share-one-drive-upload-folder', window.parent.document).fadeOut();
      }

      window.parent.window.modal_action.close();
    }
  }

  function insertShareoneDriveShortCodeWC(event) {
    event.preventDefault();

    var data = createShortcode();
    if (data !== false) {
      $('#shareonedrive_upload_box_shortcode', window.parent.document).val(data);
      window.parent.tb_remove();
    }
  }

  function insertShareoneDriveShortCodeMedia($entry_element) {

    $("#ShareoneDrive_showplaylist").prop("checked", false);
    $("#ShareoneDrive_include").val($entry_element.data('id'));
    var file_name = $entry_element.find('.entry_link:first').data('filename');

    $.each($('.foraudio input[name="ShareoneDrive_mediaextensions[]"]'), function () {
      var extension = '.' + $(this).val();

      if (file_name.indexOf(extension) > -1) {
        $(this).prop("checked", true);
        $("input#audio").trigger('click');
        return false;
      }
    });

    $.each($('.forvideo input[name="ShareoneDrive_mediaextensions[]"]'), function () {
      var extension = '.' + $(this).val();

      if (file_name.indexOf(extension) > -1) {
        $(this).prop("checked", true);
        $("input#video").trigger('click');
        return false;
      }
    });

    var data = createShortcode();

    if (data !== false) {
      tinyMCEPopup.execCommand('mceInsertContent', false, data);
      // Refocus in window
      if (tinyMCEPopup.isWindow)
        window.focus();
      tinyMCEPopup.editor.focus();
      tinyMCEPopup.close();
    }
  }

  function showRawShortcode() {
    /* Close any open modal windows */
    $('#shareonedrive-modal-action').remove();
    var shortcode = createShortcode();


    if (shortcode === false) {
      return false;
    }

    /* Build the Shortcode Dialog */
    var modalbuttons = '';
    modalbuttons += '<button class="simple-button blue shareonedrive-modal-copy-btn" type="button" title="' + ShareoneDrive_vars.str_copy_to_clipboard_title + '" >' + ShareoneDrive_vars.str_copy_to_clipboard_title + '</button>';
    var modalheader = $('<a tabindex="0" class="close-button" title="' + ShareoneDrive_vars.str_close_title + '" onclick="modal_action.close();"><i class="fas fa-times fa-lg" aria-hidden="true"></i></a></div>');
    var modalbody = $('<div class="shareonedrive-modal-body" tabindex="0" ><strong>' + shortcode + '</strong></div>');
    var modalfooter = $('<div class="shareonedrive-modal-footer"><div class="shareonedrive-modal-buttons">' + modalbuttons + '</div></div>');
    var modaldialog = $('<div id="shareonedrive-modal-action" class="ShareoneDrive shareonedrive-modal"><div class="modal-dialog"><div class="modal-content"></div></div></div>');
    $('body').append(modaldialog);
    $('#shareonedrive-modal-action .modal-content').append(modalheader, modalbody, modalfooter);

    /* Set the button actions */
    $('#shareonedrive-modal-action .shareonedrive-modal-copy-btn').unbind('click');
    $('#shareonedrive-modal-action .shareonedrive-modal-copy-btn').click(function () {

      var $temp = $("<input>");
      $("body").append($temp);
      $temp.val(shortcode).select();
      document.execCommand("copy");
      $temp.remove();

    });

    /* Open the Dialog and load the images inside it */
    var modal_action = new RModal(document.getElementById('shareonedrive-modal-action'), {
      dialogOpenClass: 'animated slideInDown',
      dialogCloseClass: 'animated slideOutUp',
      escapeClose: true
    });
    document.addEventListener('keydown', function (ev) {
      modal_action.keydown(ev);
    }, false);
    modal_action.open();
    window.modal_action = modal_action;

    return false;
  }
  function createDirectLinks() {
    var listtoken = $(".ShareoneDrive.files").attr('data-token'),
            lastpath = $(".ShareoneDrive[data-token='" + listtoken + "']").attr('data-path'),
            entries = readOneDriveArrCheckBoxes(".ShareoneDrive[data-token='" + listtoken + "'] input[name='selected-files[]']");

    if (entries.length === 0) {
      if (tinyMCEPopup.isWindow)
        window.focus();
      tinyMCEPopup.editor.focus();
      tinyMCEPopup.close();
    }

    $.ajax({
      type: "POST",
      url: ShareoneDrive_vars.ajax_url,
      data: {
        action: 'shareonedrive-create-link',
        listtoken: listtoken,
        lastpath: lastpath,
        entries: entries,
        _ajax_nonce: ShareoneDrive_vars.createlink_nonce
      },
      beforeSend: function () {
        $(".ShareoneDrive .loading").height($(".ShareoneDrive .ajax-filelist").height());
        $(".ShareoneDrive .loading").fadeTo(400, 0.8);
        $(".ShareoneDrive .insert_links").attr('disabled', 'disabled');
      },
      complete: function () {
        $(".ShareoneDrive .loading").fadeOut(400);
        $(".ShareoneDrive .insert_links").removeAttr('disabled');
      },
      success: function (response) {
        if (response !== null) {
          if (response.links !== null && response.links.length > 0) {

            var data = '';

            $.each(response.links, function (key, linkresult) {
              data += '<a class="ShareoneDrive-directlink" href="' + linkresult.link.replace('?dl=1', '') + '" target="_blank">' + linkresult.name + '</a><br/>';
            });

            tinyMCEPopup.execCommand('mceInsertContent', false, data);
            // Refocus in window
            if (tinyMCEPopup.isWindow)
              window.focus();
            tinyMCEPopup.editor.focus();
            tinyMCEPopup.close();
          } else {
          }
        }
      },
      dataType: 'json'
    });
    return false;
  }

  function insertEmbedded() {
    var listtoken = $(".ShareoneDrive.files").attr('data-token'),
            lastpath = $(".ShareoneDrive[data-token='" + listtoken + "']").attr('data-path'),
            entries = readOneDriveArrCheckBoxes(".ShareoneDrive[data-token='" + listtoken + "'] input[name='selected-files[]']");

    if (entries.length === 0) {
      if (tinyMCEPopup.isWindow)
        window.focus();
      tinyMCEPopup.editor.focus();
      tinyMCEPopup.close();
    }

    $.ajax({
      type: "POST",
      url: ShareoneDrive_vars.ajax_url,
      data: {
        action: 'shareonedrive-embedded',
        listtoken: listtoken,
        lastpath: lastpath,
        entries: entries,
        _ajax_nonce: ShareoneDrive_vars.createlink_nonce
      },
      beforeSend: function () {
        $(".ShareoneDrive .loading").height($(".ShareoneDrive .ajax-filelist").height());
        $(".ShareoneDrive .loading").fadeTo(400, 0.8);
        $(".ShareoneDrive .insert_links").attr('disabled', 'disabled');
      },
      complete: function () {
        $(".ShareoneDrive .loading").fadeOut(400);
        $(".ShareoneDrive .insert_links").removeAttr('disabled');
      },
      success: function (response) {
        if (response !== null) {
          if (response.links !== null && response.links.length > 0) {

            var data = '';

            $.each(response.links, function (key, linkresult) {
              if ($.inArray(linkresult.extension, ['jpg', 'jpeg', 'png', 'gif']) > -1) {
                data += '<img src="' + linkresult.embeddedlink + '" />';
              } else {
                data += '<iframe src="' + linkresult.embeddedlink + '" height="480" style="width:100%;" frameborder="0" scrolling="no" class="sod-embedded"  allowfullscreen></iframe>';
              }
            });

            tinyMCEPopup.execCommand('mceInsertContent', false, data);
            // Refocus in window
            if (tinyMCEPopup.isWindow)
              window.focus();
            tinyMCEPopup.editor.focus();
            tinyMCEPopup.close();
          } else {
          }
        }
      },
      dataType: 'json'
    });
    return false;
  }

  function readCheckBoxes(element) {
    var values = readOneDriveArrCheckBoxes(element);

    if (values.length === 0) {
      return "none";
    }

    if (values.length === $(element).length) {
      return "all";
    }

    return values.join('|');
  }

  function readOneDriveArrCheckBoxes(element) {
    var values = $(element + ":checked").map(function () {
      return this.value;
    }).get();
    return values;
  }
});