<?php
$settings = (array) get_option('share_one_drive_settings');

if (
        !(\TheLion\ShareoneDrive\Helpers::check_user_role($this->settings['permissions_add_shortcodes'])) &&
        !(\TheLion\ShareoneDrive\Helpers::check_user_role($this->settings['permissions_add_links'])) &&
        !(\TheLion\ShareoneDrive\Helpers::check_user_role($this->settings['permissions_add_embedded']))
) {
    die();
}

$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'default';
$standalone = isset($_REQUEST['standaloneshortcodebuilder']);

function wp_roles_checkbox($name, $selected = array()) {
    global $wp_roles;
    if (!isset($wp_roles)) {
        $wp_roles = new WP_Roles();
    }

    $roles = $wp_roles->get_names();


    foreach ($roles as $role_value => $role_name) {
        if (in_array($role_value, $selected) || $selected[0] == 'all') {
            $checked = 'checked="checked"';
        } else {
            $checked = '';
        }
        echo '<div class="shareonedrive-option-checkbox">';
        echo '<input class="simple" type="checkbox" name="' . $name . '[]" value="' . $role_value . '" ' . $checked . '>';
        echo '<label for="userfolders_method_auto1" class="shareonedrive-option-checkbox-label">' . $role_name . '</label>';
        echo '</div>';
    }
    if (in_array('guest', $selected) || $selected[0] == 'all') {
        $checked = 'checked="checked"';
    } else {
        $checked = '';
    }
    echo '<div class="shareonedrive-option-checkbox">';
    echo '<input class="simple" type="checkbox" name="' . $name . '[]" value="guest" ' . $checked . '>';
    echo '<label for="userfolders_method_auto1" class="shareonedrive-option-checkbox-label">' . __('Guest', 'shareonedrive') . '</label>';
    echo '</div>';
}

$this->load_scripts();
$this->load_styles();
$this->load_custom_css();

function ShareoneDrive_remove_all_scripts() {
    global $wp_scripts;
    $wp_scripts->queue = array();

    wp_enqueue_script('jquery-effects-fade');
    wp_enqueue_script('jquery');
    wp_enqueue_script('ShareoneDrive');
    wp_enqueue_script('ShareoneDrive.tinymce');
}

function ShareoneDrive_remove_all_styles() {
    global $wp_styles;
    $wp_styles->queue = array();
    wp_enqueue_style('qtip');
    wp_enqueue_style('ShareoneDrive.tinymce');
    wp_enqueue_style('ShareoneDrive');
    wp_enqueue_style('Awesome-Font-5-css');
}

add_action('wp_print_scripts', 'ShareoneDrive_remove_all_scripts', 1000);
add_action('wp_print_styles', 'ShareoneDrive_remove_all_styles', 1000);

/* Count number of openings for rating dialog */
$counter = get_option('share_one_drive_shortcode_opened', 0) + 1;
update_option('share_one_drive_shortcode_opened', $counter);

/* Initialize shortcode vars */
$mode = (isset($_REQUEST['mode'])) ? $_REQUEST['mode'] : 'files';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
      <?php
      if ($type === 'default') {
          $title = __('Shortcode Builder', 'shareonedrive');
          $mcepopup = 'shortcode';
      } else if ($type === 'links') {
          $title = __('Insert direct Links', 'shareonedrive');
          $mcepopup = 'links';
      } else if ($type === 'embedded') {
          $title = __('Embed files', 'shareonedrive');
          $mcepopup = 'embedded';
      } else if ($type === 'gravityforms') {
          $title = __('Shortcode Builder', 'shareonedrive');
          $mcepopup = 'shortcode';
      } else if ($type === 'woocommerce') {
          $title = __('Shortcode Builder', 'shareonedrive');
          $mcepopup = 'shortcode';
      } else if ($type === 'contactforms7') {
          $title = __('Shortcode Builder', 'shareonedrive');
          $mcepopup = 'shortcode';
      }
      ?></title>
    <?php if ($type !== 'gravityforms' && $type !== 'contactforms7' && $standalone === false) { ?>
        <script type="text/javascript" src="<?php echo site_url(); ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
    <?php } ?>

    <?php wp_print_scripts(); ?>
    <?php wp_print_styles(); ?>
  </head>

  <body class="shareonedrive" data-mode="<?php echo $mode; ?>">
    <?php $this->ask_for_review(); ?>

    <form action="#">

      <div class="wrap">
        <div class="shareonedrive-header">
          <div class="shareonedrive-logo"><img src="<?php echo SHAREONEDRIVE_ROOTPATH; ?>/css/images/logo64x64.png" height="64" width="64"/></div>
          <div class="shareonedrive-form-buttons">
            <?php if ($type === 'default') { ?>
                <?php if ($standalone) { ?>
                    <div id="get_shortcode" class="simple-button default get_shortcode" name="get_shortcode" title="<?php _e("Get raw Shortcode", 'shareonedrive'); ?>"><?php _e("Create Shortcode", 'shareonedrive'); ?><i class="fas fa-code" aria-hidden="true"></i></div>
                <?php } else { ?>
                    <div id="get_shortcode" class="simple-button default get_shortcode" name="get_shortcode" title="<?php _e("Get raw Shortcode", 'shareonedrive'); ?>"><?php _e("Raw", 'shareonedrive'); ?><i class="fas fa-code" aria-hidden="true"></i></div>
                    <div id="doinsert"  class="simple-button default insert_shortcode" name="insert"><?php _e("Insert Shortcode", 'shareonedrive'); ?>&nbsp;<i class="fas fa-chevron-circle-right" aria-hidden="true"></i></div>
                <?php } ?>
            <?php } elseif ($type === 'links') { ?>
                <div id="doinsert" class="simple-button default insert_links" name="insert"  ><?php _e("Insert Links", 'shareonedrive'); ?>&nbsp;<i class="fas fa-chevron-circle-right" aria-hidden="true"></i></div>
            <?php } elseif ($type === 'embedded') { ?>
                <div id="doinsert" class="simple-button default insert_embedded" name="insert" ><?php _e("Embed Files", 'shareonedrive'); ?>&nbsp;<i class="fas fa-chevron-circle-right" aria-hidden="true"></i></div>
            <?php } elseif ($type === 'gravityforms') { ?>
                <div id="doinsert" class="simple-button default insert_shortcode_gf" name="insert"><?php _e("Insert Shortcode", 'shareonedrive'); ?>&nbsp;<i class="fas fa-chevron-circle-right" aria-hidden="true"></i></div>
            <?php } elseif ($type === 'woocommerce') { ?>
                <div id="doinsert" class="simple-button default insert_shortcode_woocommerce" name="insert"><?php _e("Insert Shortcode", 'shareonedrive'); ?>&nbsp;<i class="fas fa-chevron-circle-right" aria-hidden="true"></i></div>
            <?php } elseif ($type === 'contactforms7') { ?>
                <div id="doinsert" class="simple-button default insert_shortcode_cf" name="insert"><?php _e("Insert Shortcode", 'shareonedrive'); ?>&nbsp;<i class="fas fa-chevron-circle-right" aria-hidden="true"></i></div>
            <?php } ?>
          </div>

          <div class="shareonedrive-title"><?php echo $title; ?></div>

        </div>
        <?php
        if ($type === 'links' || $type === 'embedded') {

            echo '<div class="shareonedrive-panel shareonedrive-panel-full">';

            if ($type === 'embedded') {
                echo "<p>" . __('Please note that the embedded files need to be public (with link)', 'shareonedrive') . "</p>";
            }

            $allowed_extensions = 'csv|doc|docx|odp|ods|odt|pot|potm|potx|pps|ppsx|ppsxm|ppt|pptm|pptx|rtf|xls|xlsx|jpg|jpeg|gif|png|pdf';

            $atts = array(
                'mode' => 'files',
                'showfiles' => '1',
                'upload' => '0',
                'delete' => '0',
                'rename' => '0',
                'addfolder' => '0',
                'showcolumnnames' => '0',
                'viewrole' => 'all',
                'candownloadzip' => '0',
                'showsharelink' => '0',
                'previewinline' => '0',
                'mcepopup' => $mcepopup,
                'includeext' => ($type === 'embedded') ? $allowed_extensions : '*',
                '_random' => 'embed'
            );

            $user_folder_backend = apply_filters('shareonedrive_use_user_folder_backend', $this->settings['userfolder_backend']);

            if ($user_folder_backend !== 'No') {
                $atts['userfolders'] = $user_folder_backend;

                $private_root_folder = $this->settings['userfolder_backend_auto_root'];
                if ($user_folder_backend === 'auto' && !empty($private_root_folder) && isset($private_root_folder['id'])) {
                    $atts['dir'] = $private_root_folder['id'];

                    if (!isset($private_root_folder['view_roles'])) {
                        $private_root_folder['view_roles'] = array('none');
                    }
                    $atts['viewuserfoldersrole'] = implode('|', $private_root_folder['view_roles']);
                }
            }

            echo $this->create_template($atts);
            echo "</div>";
            ?>
            <?php
        } else {
            ?>

            <div id="" class="shareonedrive-panel shareonedrive-panel-left">
              <div class="shareonedrive-nav-header"><?php _e('Shortcode Settings', 'shareonedrive'); ?></div>
              <ul class="shareonedrive-nav-tabs">
                <li id="settings_general_tab" data-tab="settings_general" class="current"><a><span><?php _e('General', 'shareonedrive'); ?></span></a></li>
                <li id="settings_folder_tab" data-tab="settings_folders"><a><span><?php _e('Folders', 'shareonedrive'); ?></span></a></li>
                <li id="settings_mediafiles_tab" data-tab="settings_mediafiles"><a><span><?php _e('Media Files', 'shareonedrive'); ?></span></a></li>
                <li id="settings_layout_tab" data-tab="settings_layout"><a><span><?php _e('Layout', 'shareonedrive'); ?></span></a></li>
                <li id="settings_sorting_tab" data-tab="settings_sorting"><a><span><?php _e('Sorting', 'shareonedrive'); ?></span></a></li>
                <li id="settings_advanced_tab" data-tab="settings_advanced"><a><span><?php _e('Advanced', 'shareonedrive'); ?></span></a></li>
                <li id="settings_exclusions_tab" data-tab="settings_exclusions"><a><span><?php _e('Exclusions', 'shareonedrive'); ?></span></a></li>
                <li id="settings_upload_tab" data-tab="settings_upload"><a><span><?php _e('Upload Box', 'shareonedrive'); ?></span></a></li>
                <li id="settings_notifications_tab" data-tab="settings_notifications"><a><span><?php _e('Notifications', 'shareonedrive'); ?></span></a></li>
                <li id="settings_manipulation_tab" data-tab="settings_manipulation"><a><span><?php _e('File Manipulation', 'shareonedrive'); ?></span></a></li>
                <li id="settings_permissions_tab" data-tab="settings_permissions" class=""><a><span><?php _e('User Permissions', 'shareonedrive'); ?></span></a></li>
              </ul>
            </div>

            <div class="shareonedrive-panel shareonedrive-panel-right">

              <!-- General Tab -->
              <div id="settings_general" class="shareonedrive-tab-panel current">

                <div class="shareonedrive-tab-panel-header"><?php _e('General', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Plugin Mode', 'shareonedrive'); ?></div>
                <div class="shareonedrive-option-description"><?php _e('Select how you want to use Share-one-Drive in your post or page', 'shareonedrive'); ?>:</div>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="files" name="mode" <?php echo (($mode === 'files') ? 'checked="checked"' : ''); ?> value="files" class="mode"/>
                  <label for="files" class="shareonedrive-option-radio-label"><?php _e('File browser', 'shareonedrive'); ?></label>
                </div>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="upload" name="mode" <?php echo (($mode === 'upload') ? 'checked="checked"' : ''); ?> value="upload" class="mode"/>
                  <label for="upload" class="shareonedrive-option-radio-label"><?php _e('Upload Box', 'shareonedrive'); ?></label>
                </div>
                <?php if ($type !== 'gravityforms' && $type !== 'contactforms7') { ?>
                    <div class="shareonedrive-option-radio">
                      <input type="radio" id="gallery" name="mode" <?php echo (($mode === 'gallery') ? 'checked="checked"' : ''); ?> value="gallery" class="mode"/>
                      <label for="gallery" class="shareonedrive-option-radio-label"><?php _e('Photo gallery', 'shareonedrive'); ?></label>
                    </div>
                    <div class="shareonedrive-option-radio">
                      <input type="radio" id="audio" name="mode" <?php echo (($mode === 'audio') ? 'checked="checked"' : ''); ?> value="audio" class="mode"/>
                      <label for="audio" class="shareonedrive-option-radio-label"><?php _e('Audio player', 'shareonedrive'); ?></label>
                    </div>
                    <div class="shareonedrive-option-radio">
                      <input type="radio" id="video" name="mode" <?php echo (($mode === 'video') ? 'checked="checked"' : ''); ?> value="video" class="mode"/>
                      <label for="video" class="shareonedrive-option-radio-label"><?php _e('Video player', 'shareonedrive'); ?></label>
                    </div>
                    <div class="shareonedrive-option-radio">
                      <input type="radio" id="search" name="mode" <?php echo (($mode === 'search') ? 'checked="checked"' : ''); ?> value="search" class="mode"/>
                      <label for="search" class="shareonedrive-option-radio-label"><?php _e('Search Box', 'shareonedrive'); ?></label>
                    </div>
                <?php } else {
                    ?>
                    <br/>
                    <div class="sod-updated">
                      <i><strong>TIP</strong>: <?php _e("Don't forget to check the Upload Permissions on the User Permissions tab", 'shareonedrive'); ?>. <?php _e("By default, only logged-in users can upload files", 'shareonedrive'); ?>.</i>
                    </div>
                    <?php
                }
                ?>

              </div>
              <!-- End General Tab -->
              <!-- User Folders Tab -->
              <div id="settings_folders" class="shareonedrive-tab-panel">

                <div class="shareonedrive-tab-panel-header"><?php _e('Folders', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Select start Folder', 'shareonedrive'); ?></div>
                <div class="shareonedrive-option-description"><?php _e('Select which folder should be used as starting point, or in case the Smart Client Area is enabled should be used for the Private Folders ', 'shareonedrive'); ?>. <?php _e('Users will not be able to navigate outside this folder', 'shareonedrive'); ?>.</div>
                <div class="root-folder">
                  <?php
                  $atts = array(
                      'mode' => 'files',
                      'maxheight' => '300px',
                      'filelayout' => 'list',
                      'showfiles' => '1',
                      'filesize' => '0',
                      'filedate' => '0',
                      'upload' => '0',
                      'delete' => '0',
                      'rename' => '0',
                      'addfolder' => '0',
                      'showbreadcrumb' => '1',
                      'showcolumnnames' => '0',
                      'search' => '0',
                      'roottext' => '',
                      'viewrole' => 'all',
                      'downloadrole' => 'none',
                      'candownloadzip' => '0',
                      'showsharelink' => '0',
                      'previewinline' => '0',
                      'mcepopup' => $mcepopup
                  );

                  if (isset($_REQUEST['dir'])) {
                      $atts['startid'] = $_REQUEST['dir'];
                  }

                  $user_folder_backend = apply_filters('shareonedrive_use_user_folder_backend', $this->settings['userfolder_backend']);

                  if ($user_folder_backend !== 'No') {
                      $atts['userfolders'] = $user_folder_backend;

                      $private_root_folder = $this->settings['userfolder_backend_auto_root'];
                      if ($user_folder_backend === 'auto' && !empty($private_root_folder) && isset($private_root_folder['id'])) {
                          $atts['dir'] = $private_root_folder['id'];

                          if (!isset($private_root_folder['view_roles'])) {
                              $private_root_folder['view_roles'] = array('none');
                          }
                          $atts['viewuserfoldersrole'] = implode('|', $private_root_folder['view_roles']);
                      }
                  }

                  echo $this->create_template($atts);
                  ?>
                </div>

                <br/>
                <div class="shareonedrive-tab-panel-header"><?php _e('Smart Client Area', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Use Private Folders', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_linkedfolders" id="ShareoneDrive_linkedfolders" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['userfolders'])) ? 'checked="checked"' : ''; ?> data-div-toggle='option-userfolders'/>
                    <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_linkedfolders"></label>
                  </div>
                </div>

                <div class="shareonedrive-option-description">
                  <?php echo sprintf(__('The plugin can easily and securily share documents on your %s with your users/clients', 'shareonedrive'), 'OneDrive'); ?>. 
                  <?php _e('This allows your clients to preview, download and manage their documents in their own private folder', 'shareonedrive'); ?>.
                  <?php echo sprintf(__('Specific permissions can always be set via %s', 'shareonedrive'), '<a href="#" onclick="jQuery(\'li[data-tab=settings_permissions]\').trigger(\'click\')">' . __('User Permissions', 'shareonedrive') . '</a>'); ?>. 

                  <?php _e('The Smart Client Area can be useful in some situations, for example', 'shareonedrive'); ?>:
                  <ul>
                    <li><?php _e('You want to share documents with your clients privately', 'shareonedrive'); ?></li>
                    <li><?php _e('You want your clients, users or guests upload files to their own folder', 'shareonedrive'); ?></li>
                    <li><?php _e('You want to give your customers a private folder already filled with some files directly after they register', 'shareonedrive'); ?></li>
                  </ul>
                </div>

                <div class="option option-userfolders forfilebrowser foruploadbox forgallery <?php echo (isset($_REQUEST['userfolders'])) ? '' : 'hidden'; ?>">

                  <div class="shareonedrive-option-title"><?php _e('Mode', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-option-description"><?php _e('Do you want to link your users manually to their Private Folder or should the plugin handle this automatically for you', 'shareonedrive'); ?>.</div>

                  <?php
                  $userfolders = 'auto';
                  if (isset($_REQUEST['userfolders'])) {
                      $userfolders = $_REQUEST['userfolders'];
                  }
                  ?>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="userfolders_method_manual" name="ShareoneDrive_userfolders_method"<?php echo ($userfolders === 'manual') ? 'checked="checked"' : ''; ?> value="manual"/>
                    <label for="userfolders_method_manual" class="shareonedrive-option-radio-label"><?php echo sprintf(__('I will link the users manually via %sthis page%s', 'shareonedrive'), '<a href="' . admin_url('admin.php?page=ShareoneDrive_settings_linkusers') . '" target="_blank">', '</a>'); ?></label>
                  </div>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="userfolders_method_auto" name="ShareoneDrive_userfolders_method" <?php echo ($userfolders === 'auto') ? 'checked="checked"' : ''; ?> value="auto"/>
                    <label for="userfolders_method_auto" class="shareonedrive-option-radio-label"><?php _e('Let the plugin automatically manage the Private Folders for me in the folder I have selected above', 'shareonedrive'); ?></label>
                  </div>

                  <div class="option-userfolders_auto">
                    <div class="shareonedrive-option-title"><?php _e('Template Folder', 'shareonedrive'); ?>
                      <div class="shareonedrive-onoffswitch">
                        <input type="checkbox" name="ShareoneDrive_userfolders_template" id="ShareoneDrive_userfolders_template" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['usertemplatedir'])) ? 'checked="checked"' : ''; ?> data-div-toggle='userfolders-template-option'/>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_userfolders_template"></label>
                      </div>
                    </div>
                    <div class="shareonedrive-option-description">
                      <?php _e('Newly created Private Folders can be prefilled with files from a template', 'shareonedrive'); ?>. <?php _e('The content of the template folder selected will be copied to the user folder', 'shareonedrive'); ?>.
                    </div>

                    <div class="userfolders-template-option <?php echo (isset($_REQUEST['usertemplatedir'])) ? '' : 'hidden'; ?>">
                      <div class="template-folder">
                        <?php
                        $user_folders = (($user_folder_backend === 'No') ? '0' : $this->settings['userfolder_backend']);

                        $atts = array(
                            'mode' => 'files',
                            'filelayout' => 'list',
                            'maxheight' => '300px',
                            'showfiles' => '1',
                            'filesize' => '0',
                            'filedate' => '0',
                            'upload' => '0',
                            'delete' => '0',
                            'rename' => '0',
                            'addfolder' => '0',
                            'showbreadcrumb' => '1',
                            'showcolumnnames' => '0',
                            'viewrole' => 'all',
                            'downloadrole' => 'none',
                            'candownloadzip' => '0',
                            'showsharelink' => '0',
                            'userfolders' => $user_folders,
                            'mcepopup' => $mcepopup
                        );

                        if (isset($_REQUEST['usertemplatedir'])) {
                            $atts['startid'] = $_REQUEST['usertemplatedir'];
                        }

                        echo $this->create_template($atts);
                        ?>
                      </div>
                    </div>

                    <div class="shareonedrive-option-title"><?php _e('Full Access', 'shareonedrive'); ?></div>
                    <div class="shareonedrive-option-description"><?php _e('By default only Administrator users will be able to navigate through all Private Folders', 'shareonedrive'); ?>. <?php _e('When you want other User Roles to be able do browse to the Private Folders as well, please check them below', 'shareonedrive'); ?>.</div>

                    <?php
                    $selected = (isset($_REQUEST['viewuserfoldersrole'])) ? explode('|', $_REQUEST['viewuserfoldersrole']) : array('administrator');
                    wp_roles_checkbox('ShareoneDrive_view_user_folders_role', $selected);
                    ?>

                    <div class="shareonedrive-option-title"><?php _e('Quota', 'shareonedrive'); ?></div>
                    <div class="shareonedrive-option-description"><?php _e("Set maximum size of the User Folder (e.g. 10M, 100M, 1G). When the Upload function is enabled, the user will not be able to upload when the limit is reached", "shareonedrive"); ?>. <?php _e('Leave this field empty or set it to -1 for unlimited disk space', 'shareonedrive'); ?>.</div>
                    <input type="text" name="ShareoneDrive_maxuserfoldersize" id="ShareoneDrive_maxuserfoldersize" placeholder="e.g. 10M, 100M, 1G" value="<?php echo (isset($_REQUEST['maxuserfoldersize'])) ? $_REQUEST['maxuserfoldersize'] : ''; ?>"/>
                  </div>
                </div>

              </div>
              <!-- End User Folders Tab -->
              <!-- Media Files Tab -->
              <div id="settings_mediafiles"  class="shareonedrive-tab-panel">

                <div class="shareonedrive-tab-panel-header"><?php _e('Media Files', 'shareonedrive'); ?></div>

                <div class="foraudio">
                  <div class="shareonedrive-option-title"><?php _e('Provided formats', 'shareonedrive'); ?>*</div>
                  <div class="shareonedrive-option-description"><?php _e('Select which sort of media files you will provide', 'shareonedrive'); ?>. <?php _e('You may provide the same file with different extensions to increase cross-browser support', 'shareonedrive'); ?>. <?php _e('Do always supply a mp3 (audio) or m4v/mp4 (video)file to support all browsers', 'shareonedrive'); ?>.</div>
                  <?php
                  $mediaextensions = (!isset($_REQUEST['mediaextensions']) || ($mode !== 'audio')) ? array() : explode('|', $_REQUEST['mediaextensions']);
                  ?>

                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_mp3" type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('mp3', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='mp3' /><label for="mediaextensions_mp3" class="shareonedrive-option-checkbox-label">mp3</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_mp4"  type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('mp4', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='mp4' /><label for="mediaextensions_mp4" class="shareonedrive-option-checkbox-label">mp4</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_m4a" type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('m4a', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='m4a' /><label for="mediaextensions_m4a" class="shareonedrive-option-checkbox-label">m4a</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_ogg"  type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('ogg', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='ogg' /><label for="mediaextensions_ogg" class="shareonedrive-option-checkbox-label">ogg</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_oga" type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('oga', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='oga' /><label for="mediaextensions_oga" class="shareonedrive-option-checkbox-label">oga</label></div>
                </div>        

                <div class="forvideo">
                  <div class="shareonedrive-option-title"><?php _e('Provided formats', 'shareonedrive'); ?>*</div>
                  <div class="shareonedrive-option-description"><?php _e('Select which sort of media files you will provide', 'shareonedrive'); ?>. <?php _e('You may provide the same file with different extensions to increase cross-browser support', 'shareonedrive'); ?>. <?php _e('Do always supply a mp3 (audio) or m4v/mp4 (video)file to support all browsers', 'shareonedrive'); ?>.</div>
                  <?php
                  $mediaextensions = (!isset($_REQUEST['mediaextensions']) || ($mode !== 'video')) ? array() : explode('|', $_REQUEST['mediaextensions']);
                  ?>

                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_mp4" type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('mp4', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='mp4' /><label for="mediaextensions_mp4" class="shareonedrive-option-checkbox-label">mp4</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_m4v"  type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('m4v', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='m4v' /><label for="mediaextensions_m4v" class="shareonedrive-option-checkbox-label">m4v</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_ogg" type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('ogg', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='ogg' /><label for="mediaextensions_ogg" class="shareonedrive-option-checkbox-label">ogg</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_ogv"  type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('ogv', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='ogv' /><label for="mediaextensions_ogv" class="shareonedrive-option-checkbox-label">ogv</label></div>
                  <div class="shareonedrive-option-checkbox" style="display: inline-block;"><input id="mediaextensions_webmv" type="checkbox" name="ShareoneDrive_mediaextensions[]" <?php echo (in_array('webmv', $mediaextensions)) ? 'checked="checked"' : ''; ?> value='webmv' /><label for="mediaextensions_webmv" class="shareonedrive-option-checkbox-label">webmv</label></div>
                </div>  

                <div class="shareonedrive-option-title"><?php _e('Auto Play', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_autoplay" id="ShareoneDrive_autoplay" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['autoplay']) && $_REQUEST['autoplay'] === '1') ? 'checked="checked"' : ''; ?>>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_autoplay"></label>
                  </div>
                </div>

                <div class="shareonedrive-option-title"><?php _e('Show Playlist on start', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_showplaylist" id="ShareoneDrive_showplaylist" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['hideplaylist']) && $_REQUEST['hideplaylist'] === '1') ? '' : 'checked="checked"'; ?>>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showplaylist"></label>
                  </div>
                </div>   

                <div class="foraudio">
                  <div class="shareonedrive-option-title"><?php _e('Display covers', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_covers" id="ShareoneDrive_covers" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['covers']) && $_REQUEST['covers'] === '1') ? 'checked="checked"' : ''; ?>>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_covers"></label>
                    </div>
                  </div>   
                  <div class="shareonedrive-option-description"><?php _e('You can show covers of your audio files in the Audio Player', 'shareonedrive'); ?>. <?php _e('Add a *.png or *.jpg file with the same name as your audio file in the same folder as your audio files. You can also add a cover with the name of the folder to show the cover for all audio files in the album', 'shareonedrive'); ?>. <?php _e('If no cover is available, a placeholder will be used', 'shareonedrive'); ?>.</div>
                </div>


                <div class="shareonedrive-option-title"><?php _e('Download Button', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_linktomedia" id="ShareoneDrive_linktomedia" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['linktomedia']) && $_REQUEST['linktomedia'] === '1') ? 'checked="checked"' : ''; ?>>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_linktomedia"></label>
                  </div>
                </div>   

                <div class="shareonedrive-option-title"><?php _e('Purchase Button', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_mediapurchase" id="ShareoneDrive_mediapurchase" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['linktoshop']) && $_REQUEST['linktoshop'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle='webshop-options'>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_mediapurchase"></label>
                  </div>
                </div>  


                <div class="option webshop-options <?php echo (isset($_REQUEST['linktoshop'])) ? '' : 'hidden'; ?>">
                  <div class="shareonedrive-option-title"><?php _e('Link to webshop', 'shareonedrive'); ?></div>  
                  <input class="shareonedrive-option-input-large" type="text" name="ShareoneDrive_linktoshop" id="ShareoneDrive_linktoshop" placeholder="https://www.yourwebshop.com/" value="<?php echo (isset($_REQUEST['linktoshop'])) ? $_REQUEST['linktoshop'] : ''; ?>"/>
                </div>

              </div>
              <!-- End Media Files Tab -->

              <!-- Layout Tab -->
              <div id="settings_layout"  class="shareonedrive-tab-panel">

                <div class="shareonedrive-tab-panel-header"><?php _e('Layout', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Plugin container width', 'shareonedrive'); ?></div>
                <div class="shareonedrive-option-description"><?php _e("Set max width for the Share-one-Drive container", "shareonedrive"); ?>. <?php _e("You can use pixels or percentages, eg '360px', '480px', '70%'", "shareonedrive"); ?>. <?php _e('Leave empty for default value', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_max_width" id="ShareoneDrive_max_width" placeholder="100%" value="<?php echo (isset($_REQUEST['maxwidth'])) ? $_REQUEST['maxwidth'] : ''; ?>"/>


                <div class="forfilebrowser forgallery forsearch">
                  <div class="shareonedrive-option-title"><?php _e('Plugin container height', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-option-description"><?php _e("Set max height for the Share-one-Drive container", "shareonedrive"); ?>. <?php _e("You can use pixels or percentages, eg '360px', '480px', '70%'", "shareonedrive"); ?>. <?php _e('Leave empty for default value', 'shareonedrive'); ?>.</div>
                  <input type="text" name="ShareoneDrive_max_height" id="ShareoneDrive_max_height" placeholder="" value="<?php echo (isset($_REQUEST['maxheight'])) ? $_REQUEST['maxheight'] : ''; ?>"/>
                </div>

                <div class="shareonedrive-option-title"><?php _e('Custom CSS Class', 'shareonedrive'); ?></div>
                <div class="shareonedrive-option-description"><?php _e('Add your own custom classes to the plugin container. Multiple classes can be added seperated by a whitespace', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_class" id="ShareoneDrive_class" value="<?php echo (isset($_REQUEST['class'])) ? $_REQUEST['class'] : '' ?>" autocomplete="off"/>

                <div class="forfilebrowser forsearch">
                  <div class="shareonedrive-option-title"><?php _e('File Browser view', 'shareonedrive'); ?></div>
                  <?php
                  $filelayout = (!isset($_REQUEST['filelayout'])) ? 'grid' : $_REQUEST['filelayout'];
                  ?>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="file_layout_grid" name="ShareoneDrive_file_layout"  <?php echo ($filelayout === 'grid') ? 'checked="checked"' : ''; ?> value="grid" />
                    <label for="file_layout_grid" class="shareonedrive-option-radio-label"><?php _e('Grid/Thumbnail View', 'shareonedrive'); ?></label>
                  </div>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="file_layout_list" name="ShareoneDrive_file_layout"  <?php echo ($filelayout === 'list') ? 'checked="checked"' : ''; ?> value="list" />
                    <label for="file_layout_list" class="shareonedrive-option-radio-label"><?php _e('List View', 'shareonedrive'); ?></label>
                  </div>
                </div>

                <div class=" forfilebrowser forgallery">
                  <div class="shareonedrive-option-title"><?php _e('Show header', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_breadcrumb" id="ShareoneDrive_breadcrumb" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showbreadcrumb']) && $_REQUEST['showbreadcrumb'] === '0') ? '' : 'checked="checked"'; ?> data-div-toggle="header-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_breadcrumb"></label>
                    </div>
                  </div>  

                  <div class="option header-options <?php echo (isset($_REQUEST['showbreadcrumb']) && $_REQUEST['showbreadcrumb'] === '0') ? 'hidden' : ''; ?>">
                    <div class="shareonedrive-option-title"><?php _e('Show refresh button', 'shareonedrive'); ?>
                      <div class="shareonedrive-onoffswitch">
                        <input type="checkbox" name="ShareoneDrive_showrefreshbutton" id="ShareoneDrive_showrefreshbutton" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showrefreshbutton']) && $_REQUEST['showrefreshbutton'] === '0') ? '' : 'checked="checked"'; ?>/>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showrefreshbutton"></label>
                      </div>
                    </div>
                    <div class="shareonedrive-option-description"><?php _e('Add a refresh button in the header so users can refresh the file list and pull changes', 'shareonedrive'); ?></div>

                    <div class="shareonedrive-option-title"><?php _e('Breadcrumb text for top folder', 'shareonedrive'); ?></div>
                    <input type="text" name="ShareoneDrive_roottext" id="ShareoneDrive_roottext" placeholder="<?php _e('Start', 'shareonedrive'); ?>" value="<?php echo (isset($_REQUEST['roottext'])) ? $_REQUEST['roottext'] : ''; ?>"/>
                  </div>
                </div>
                <div class=" forfilebrowser forsearch forgallery">
                  <div class="option forfilebrowser forsearch forlistonly">
                    <div class="shareonedrive-option-title"><?php _e('Show columnnames', 'shareonedrive'); ?>
                      <div class="shareonedrive-onoffswitch">
                        <input type="checkbox" name="ShareoneDrive_showcolumnnames" id="ShareoneDrive_showcolumnnames" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showcolumnnames']) && $_REQUEST['showcolumnnames'] === '0') ? '' : 'checked="checked"'; ?> data-div-toggle="columnnames-options"/>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showcolumnnames"></label>
                      </div>
                    </div>
                    <div class="shareonedrive-option-description"><?php _e('Display the columnnames of the date and filesize in the List View of the File Browser', 'shareonedrive'); ?></div>

                    <div class="columnnames-options">
                      <div class="option-filesize">
                        <div class="shareonedrive-option-title"><?php _e('Show file size', 'shareonedrive'); ?>
                          <div class="shareonedrive-onoffswitch">
                            <input type="checkbox" name="ShareoneDrive_filesize" id="ShareoneDrive_filesize" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['filesize']) && $_REQUEST['filesize'] === '0') ? '' : 'checked="checked"'; ?>/>
                            <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_filesize"></label>
                          </div>
                        </div>
                        <div class="shareonedrive-option-description"><?php _e('Display or Hide column with file sizes in List view', 'shareonedrive'); ?></div>
                      </div>

                      <div class="option-filedate">
                        <div class="shareonedrive-option-title"><?php _e('Show last modified date', 'shareonedrive'); ?>
                          <div class="shareonedrive-onoffswitch">
                            <input type="checkbox" name="ShareoneDrive_filedate" id="ShareoneDrive_filedate" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['filedate']) && $_REQUEST['filedate'] === '0') ? '' : 'checked="checked"'; ?>/>
                            <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_filedate"></label>
                          </div>
                        </div>
                        <div class="shareonedrive-option-description"><?php _e('Display or Hide column with last modified date in List view', 'shareonedrive'); ?></div>
                      </div>
                    </div>
                  </div>

                  <div class="option forfilebrowser forsearch forgallery">
                    <div class="shareonedrive-option-title"><?php _e('Show file extension', 'shareonedrive'); ?>
                      <div class="shareonedrive-onoffswitch">
                        <input type="checkbox" name="ShareoneDrive_showext" id="ShareoneDrive_showext" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showext']) && $_REQUEST['showext'] === '0') ? '' : 'checked="checked"'; ?>/>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showext"></label>
                      </div>
                    </div>
                    <div class="shareonedrive-option-description"><?php _e('Display or Hide the file extensions', 'shareonedrive'); ?></div>

                    <div class="shareonedrive-option-title"><?php _e('Show files', 'shareonedrive'); ?>
                      <div class="shareonedrive-onoffswitch">
                        <input type="checkbox" name="ShareoneDrive_showfiles" id="ShareoneDrive_showfiles" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showfiles']) && $_REQUEST['showfiles'] === '0') ? '' : 'checked="checked"'; ?>/>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showfiles"></label>
                      </div>
                    </div>
                    <div class="shareonedrive-option-description"><?php _e('Display or Hide files', 'shareonedrive'); ?></div>
                  </div>

                  <div class="shareonedrive-option-title"><?php _e('Show folders', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_showfolders" id="ShareoneDrive_showfolders" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showfolders']) && $_REQUEST['showfolders'] === '0') ? '' : 'checked="checked"'; ?>/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showfolders"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Display or Hide child folders', 'shareonedrive'); ?></div>

                  <div class="showfiles-options">
                    <div class="shareonedrive-option-title"><?php _e('Amount of files', 'shareonedrive'); ?>
                    </div>
                    <div class="shareonedrive-option-description"><?php _e('Number of files to show', 'shareonedrive'); ?>. <?php _e('Can be used for instance to only show the last 5 updated documents', 'shareonedrive'); ?>. <?php _e("Leave this field empty or set it to -1 for no limit", 'shareonedrive'); ?></div>
                    <input type="text" name="ShareoneDrive_maxfiles" id="ShareoneDrive_maxfiles" placeholder="-1" value="<?php echo (isset($_REQUEST['maxfiles'])) ? $_REQUEST['maxfiles'] : ''; ?>"/>
                  </div>
                </div>

                <div class="option forgallery">
                  <div class="shareonedrive-option-title"><?php _e('Show file names', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_showfilenames" id="ShareoneDrive_showfilenames" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showfilenames']) && $_REQUEST['showfilenames'] === '1') ? 'checked="checked"' : ''; ?>/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showfilenames"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Display or Hide the file names in the gallery', 'shareonedrive'); ?></div>

                  <div class="shareonedrive-option-title"><?php _e('Gallery row height', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-option-description"><?php _e("The ideal height you want your grid rows to be", 'shareonedrive'); ?>. <?php _e("It won't set it exactly to this as plugin adjusts the row height to get the correct width", 'shareonedrive'); ?>. <?php _e('Leave empty for default value', 'shareonedrive'); ?> (200px).</div>
                  <input type="text" name="ShareoneDrive_targetHeight" id="ShareoneDrive_targetHeight" placeholder="200" value="<?php echo (isset($_REQUEST['targetheight'])) ? $_REQUEST['targetheight'] : ''; ?>"/>

                  <div class="shareonedrive-option-title"><?php _e('Number of images lazy loaded', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-option-description"><?php _e("Number of images to be loaded each time", 'shareonedrive'); ?>. <?php _e("Set to 0 to load all images at once", 'shareonedrive'); ?>.</div>
                  <input type="text" name="ShareoneDrive_maximage" id="ShareoneDrive_maximage" placeholder="25" value="<?php echo (isset($_REQUEST['maximages'])) ? $_REQUEST['maximages'] : ''; ?>"/>

                  <div class="shareonedrive-option-title"><?php _e('Slideshow', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_slideshow" id="ShareoneDrive_slideshow" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['slideshow']) && $_REQUEST['slideshow'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="slideshow-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_slideshow"></label>
                    </div>
                  </div>

                  <div class="slideshow-options">                  
                    <div class="shareonedrive-option-description"><?php _e('Enable or disable the Slideshow mode in the Lightbox', 'shareonedrive'); ?></div>                  
                    <div class="shareonedrive-option-title"><?php _e('Delay between cycles (ms)', 'shareonedrive'); ?></div>
                    <div class="shareonedrive-option-description"><?php _e('Delay between cycles in milliseconds, the default is 5000', 'shareonedrive'); ?>.</div>
                    <input type="text" name="ShareoneDrive_pausetime" id="ShareoneDrive_pausetime" placeholder="5000" value="<?php echo (isset($_REQUEST['pausetime'])) ? $_REQUEST['pausetime'] : ''; ?>"/>
                  </div>
                </div>
              </div>
              <!-- End Layout Tab -->

              <!-- Sorting Tab -->
              <div id="settings_sorting"  class="shareonedrive-tab-panel">

                <div class="shareonedrive-tab-panel-header"><?php _e('Sorting', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Sort field', 'shareonedrive'); ?></div>
                <?php
                $sortfield = (!isset($_REQUEST['sortfield'])) ? 'name' : $_REQUEST['sortfield'];
                ?>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="name" name="sort_field" <?php echo ($sortfield === 'name') ? 'checked="checked"' : ''; ?> value="name"/>
                  <label for="name" class="shareonedrive-option-radio-label"><?php _e('Name', 'shareonedrive'); ?></label>
                </div>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="size" name="sort_field" <?php echo ($sortfield === 'size') ? 'checked="checked"' : ''; ?> value="size" />
                  <label for="size" class="shareonedrive-option-radio-label"><?php _e('Size', 'shareonedrive'); ?></label>
                </div>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="modified" name="sort_field" <?php echo ($sortfield === 'modified') ? 'checked="checked"' : ''; ?> value="modified" />
                  <label for="modified" class="shareonedrive-option-radio-label"><?php _e('Date modified', 'shareonedrive'); ?></label>
                </div>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="datetaken" name="sort_field" <?php echo ($sortfield === 'datetaken') ? 'checked="checked"' : ''; ?> value="datetaken" />
                  <label for="datetaken" class="shareonedrive-option-radio-label"><?php _e('Date taken', 'shareonedrive'); ?></label>
                </div>
                <div class="shareonedrive-option-radio">
                  <input type="radio" id="shuffle" name="sort_field" <?php echo ($sortfield === 'shuffle') ? 'checked="checked"' : ''; ?> value="shuffle" />
                  <label for="shuffle" class="shareonedrive-option-radio-label"><?php _e('Shuffle/Random', 'shareonedrive'); ?></label>
                </div>

                <div class="option-sort-field">
                  <div class="shareonedrive-option-title"><?php _e('Sort order', 'shareonedrive'); ?></div>

                  <?php
                  $sortorder = (isset($_REQUEST['sortorder']) && $_REQUEST['sortorder'] === 'desc') ? 'desc' : 'asc';
                  ?>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="asc" name="sort_order" <?php echo ($sortorder === 'asc') ? 'checked="checked"' : ''; ?> value="asc"/>
                    <label for="asc" class="shareonedrive-option-radio-label"><?php _e('Ascending', 'shareonedrive'); ?></label>
                  </div>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="desc" name="sort_order" <?php echo ($sortorder === 'desc') ? 'checked="checked"' : ''; ?> value="desc"/>
                    <label for="desc" class="shareonedrive-option-radio-label"><?php _e('Descending', 'shareonedrive'); ?></label>
                  </div>
                </div>
              </div>
              <!-- End Sorting Tab -->
              <!-- Advanced Tab -->
              <div id="settings_advanced"  class="shareonedrive-tab-panel">
                <div class="shareonedrive-tab-panel-header"><?php _e('Advanced', 'shareonedrive'); ?></div>

                <div class="option forfilebrowser forgallery forsearch">
                  <div class="shareonedrive-option-title"><?php _e('Allow Preview', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_allow_preview" id="ShareoneDrive_allow_preview" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['allowpreview']) && $_REQUEST['allowpreview'] === '0') ? '' : 'checked="checked"'; ?> data-div-toggle="preview-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_allow_preview"></label>
                    </div>
                  </div>
                </div>

                <div class="option preview-options <?php echo (isset($_REQUEST['allowpreview']) && $_REQUEST['allowpreview'] === '0') ? 'hidden' : ''; ?>">
                  <div class="shareonedrive-option-title"><?php _e('Inline Preview', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_previewinline" id="ShareoneDrive_previewinline" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['previewinline']) && $_REQUEST['previewinline'] === '0') ? '' : 'checked="checked"'; ?>/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_previewinline"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Open preview inside a lightbox or open in a new window', 'shareonedrive'); ?></div>
                </div>

                <div class="option forfilebrowser forsearch">
                  <div class="shareonedrive-option-title"><?php _e('Default on click event', 'shareonedrive'); ?></div>
                  <?php
                  $onclick = (!isset($_REQUEST['onclick'])) ? 'preview' : $_REQUEST['onclick'];
                  ?>
                  <div class="option preview-options <?php echo (isset($_REQUEST['allowpreview']) && $_REQUEST['allowpreview'] === '0') ? 'hidden' : ''; ?>">
                    <div class="shareonedrive-option-radio">
                      <input type="radio" id="onclick_preview" name="ShareoneDrive_onclick" <?php echo ($onclick === 'preview') ? 'checked="checked"' : ''; ?> value="preview"/>
                      <label for="onclick_preview" class="shareonedrive-option-radio-label"><?php _e('Preview File', 'shareonedrive'); ?></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="onclick_download" name="ShareoneDrive_onclick" <?php echo ($onclick === 'download') ? 'checked="checked"' : ''; ?> value="download"/>
                    <label for="onclick_download" class="shareonedrive-option-radio-label"><?php _e('Download File', 'shareonedrive'); ?></label>
                  </div>
                  <div class="shareonedrive-option-radio">
                    <input type="radio" id="onclick_redirect" name="ShareoneDrive_onclick" <?php echo ($onclick === 'redirect') ? 'checked="checked"' : ''; ?> value="redirect"/>
                    <label for="onclick_redirect" class="shareonedrive-option-radio-label"><?php _e('Open File in Onedrive', 'shareonedrive'); ?></label>
                  </div>

                </div>            

                <div class="option forfilebrowser foruploadbox forgallery">
                  <div class="shareonedrive-option-title"><?php _e('Allow searching', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_search" id="ShareoneDrive_search" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['search']) && $_REQUEST['search'] === '0') ? '' : 'checked="checked"'; ?> data-div-toggle="search-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_search"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('The search function allows your users to find files by filename and content (when files are indexed)', 'shareonedrive'); ?></div>
                </div>

                <?php
                if (class_exists('ZipArchive')) {
                    ?>
                    <div class="shareonedrive-option-title"><?php _e('Allow ZIP download', 'shareonedrive'); ?>
                      <div class="shareonedrive-onoffswitch">
                        <input type="checkbox" name="ShareoneDrive_candownloadzip" id="ShareoneDrive_candownloadzip" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['candownloadzip']) && $_REQUEST['candownloadzip'] === '1') ? 'checked="checked"' : ''; ?>/>
                        <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_candownloadzip"></label>
                      </div>
                    </div>
                    <div class="shareonedrive-option-description"><?php _e('Allow users to download multiple files at once', 'shareonedrive'); ?></div>
                <?php } ?>

              </div>
              <!-- End Advanced Tab -->
              <!-- Exclusions Tab -->
              <div id="settings_exclusions"  class="shareonedrive-tab-panel">
                <div class="shareonedrive-tab-panel-header"><?php _e('Exclusions', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Only show files with those extensions', 'shareonedrive'); ?>:</div>
                <div class="shareonedrive-option-description"><?php echo __('Add extensions separated with | e.g. (jpg|png|gif)', 'shareonedrive') . '. ' . __('Leave empty to show all files', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_include_ext" id="ShareoneDrive_include_ext" class="shareonedrive-option-input-large" value="<?php echo (isset($_REQUEST['includeext'])) ? $_REQUEST['includeext'] : ''; ?>"/>

                <div class="shareonedrive-option-title"><?php _e('Only show the following files or folders', 'shareonedrive'); ?>:</div>
                <div class="shareonedrive-option-description"><?php echo __('Add files or folders by name or OneDrive ID separated with | e.g. (file1.jpg|long folder name)', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_include" id="ShareoneDrive_include" class="shareonedrive-option-input-large" value="<?php echo (isset($_REQUEST['include'])) ? $_REQUEST['include'] : ''; ?>"/>

                <div class="shareonedrive-option-title"><?php _e('Hide files with those extensions', 'shareonedrive'); ?>:</div>
                <div class="shareonedrive-option-description"><?php echo __('Add extensions separated with | e.g. (jpg|png|gif)', 'shareonedrive') . '. ' . __('Leave empty to show all files', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_exclude_ext" id="ShareoneDrive_exclude_ext" class="shareonedrive-option-input-large" value="<?php echo (isset($_REQUEST['excludeext'])) ? $_REQUEST['excludeext'] : ''; ?>"/>

                <div class="shareonedrive-option-title"><?php _e('Hide the following files or folders', 'shareonedrive'); ?>:</div>
                <div class="shareonedrive-option-description"><?php echo __('Add files or folders by name or OneDrive ID separated with | e.g. (file1.jpg|long folder name)', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_exclude" id="ShareoneDrive_exclude"  class="shareonedrive-option-input-large" value="<?php echo (isset($_REQUEST['exclude'])) ? $_REQUEST['exclude'] : ''; ?>"/>

              </div>
              <!-- End Exclusions Tab -->

              <!-- Upload Tab -->
              <div id="settings_upload"  class="shareonedrive-tab-panel">

                <div class="shareonedrive-tab-panel-header"><?php _e('Upload Box', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Allow Upload', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_upload" id="ShareoneDrive_upload" data-div-toggle="upload-options" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['upload']) && $_REQUEST['upload'] === '1') ? 'checked="checked"' : ''; ?>/>
                    <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_upload"></label>
                  </div>
                </div>
                <div class="shareonedrive-option-description"><?php _e('Allow users to upload files', 'shareonedrive'); ?>. <?php echo sprintf(__('You can select which Users Roles should be able to upload via %s', 'shareonedrive'), '<a href="#" onclick="jQuery(\'li[data-tab=settings_permissions]\').trigger(\'click\')">' . __('User Permissions', 'shareonedrive') . '</a>'); ?>.</div>

                <div class="option upload-options <?php echo (isset($_REQUEST['upload']) && $_REQUEST['upload'] === '1' && in_array($mode, array('files', 'upload', 'gallery'))) ? '' : 'hidden'; ?>">

                  <div class="shareonedrive-option-title"><?php _e('Allow folder upload', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_upload_folder" id="ShareoneDrive_upload_folder"  class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['upload_folder']) && $_REQUEST['upload_folder'] === '0') ? '' : 'checked="checked"'; ?>/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_upload_folder"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Adds an Add Folder button to the upload form if the browser supports it', 'shareonedrive'); ?>. </div>


                  <div class="shareonedrive-option-title"><?php _e('Overwrite existing files', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_overwrite" id="ShareoneDrive_overwrite"  class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['overwrite']) && $_REQUEST['overwrite'] === '1') ? 'checked="checked"' : ''; ?>/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_overwrite"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Overwrite already existing files or auto-rename the new uploaded files', 'shareonedrive'); ?>. </div>

                  <div class="shareonedrive-option-title"><?php _e('Restrict file extensions', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-option-description"><?php echo __('Add extensions separated with | e.g. (jpg|png|gif)', 'shareonedrive') . ' ' . __('Leave empty for no restricion', 'shareonedrive', 'shareonedrive'); ?>.</div>
                  <input type="text" name="ShareoneDrive_upload_ext" id="ShareoneDrive_upload_ext" value="<?php echo (isset($_REQUEST['uploadext'])) ? $_REQUEST['uploadext'] : ''; ?>"/>

                  <div class="shareonedrive-option-title"><?php _e('Max uploads per session', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-option-description"><?php echo __('Number of maximum uploads per upload session', 'shareonedrive') . ' ' . __('Leave empty for no restricion', 'shareonedrive'); ?>.</div>
                  <input type="text" name="ShareoneDrive_maxnumberofuploads" id="ShareoneDrive_maxnumberofuploads" placeholder="-1" value="<?php echo (isset($_REQUEST['maxnumberofuploads'])) ? $_REQUEST['maxnumberofuploads'] : ''; ?>"/>

                  <div class="shareonedrive-option-title"><?php _e('Maximum file size', 'shareonedrive'); ?></div>
                  <?php
                  $max_size_bytes = min(\TheLion\ShareoneDrive\Helpers::return_bytes(ini_get('post_max_size')), \TheLion\ShareoneDrive\Helpers::return_bytes(ini_get('upload_max_filesize')));
                  $max_size_string = \TheLion\ShareoneDrive\Helpers::bytes_to_size_1024($max_size_bytes)
                  ?>
                  <div class="shareonedrive-option-description"><?php _e('Max filesize for uploading in bytes', 'shareonedrive'); ?>. <?php echo __('Leave empty for server maximum ', 'shareonedrive'); ?> (<?php echo $max_size_string; ?>).</div>
                  <input type="text" name="ShareoneDrive_maxfilesize" id="ShareoneDrive_maxfilesize" placeholder="<?php echo $max_size_string; ?>" value="<?php echo (isset($_REQUEST['maxfilesize'])) ? $_REQUEST['maxfilesize'] : ''; ?>"/>

                </div>

              </div>
              <!-- End Upload Tab -->

              <!-- Notifications Tab -->
              <div id="settings_notifications"  class="shareonedrive-tab-panel">

                <div class="shareonedrive-tab-panel-header"><?php _e('Notifications', 'shareonedrive'); ?></div>

                <div class="shareonedrive-option-title"><?php _e('Download email notification', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_notificationdownload" id="ShareoneDrive_notificationdownload" class="shareonedrive-onoffswitch-checkbox"  <?php echo (isset($_REQUEST['notificationdownload']) && $_REQUEST['notificationdownload'] === '1') ? 'checked="checked"' : ''; ?>/>
                    <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_notificationdownload"></label>
                  </div>
                </div>

                <div class="shareonedrive-option-title"><?php _e('Upload email notification', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_notificationupload" id="ShareoneDrive_notificationupload" class="shareonedrive-onoffswitch-checkbox"  <?php echo (isset($_REQUEST['notificationupload']) && $_REQUEST['notificationupload'] === '1') ? 'checked="checked"' : ''; ?>/>
                    <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_notificationupload"></label>
                  </div>
                </div>
                <div class="shareonedrive-option-title"><?php _e('Delete email notification', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_notificationdeletion" id="ShareoneDrive_notificationdeletion" class="shareonedrive-onoffswitch-checkbox"  <?php echo (isset($_REQUEST['notificationdeletion']) && $_REQUEST['notificationdeletion'] === '1') ? 'checked="checked"' : ''; ?>/>
                    <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_notificationdeletion"></label>
                  </div>
                </div>

                <div class="shareonedrive-option-title"><?php _e('Receiver', 'shareonedrive'); ?></div>
                <div class="shareonedrive-option-description"><?php _e('On which email address would you like to receive the notification? You can use <code>%admin_email%</code>, <code>%user_email%</code> (user that executes the action) and <code>%linked_user_email%</code> (Manually linked Private Folders)', 'shareonedrive'); ?>.</div>
                <input type="text" name="ShareoneDrive_notification_email" id="ShareoneDrive_notification_email" class="shareonedrive-option-input-large" placeholder="<?php echo get_site_option('admin_email'); ?>" value="<?php echo (isset($_REQUEST['notificationemail'])) ? $_REQUEST['notificationemail'] : ''; ?>" />

                <div class="shareonedrive-option-title"><?php _e('Skip notification of the user that executes the action', 'shareonedrive'); ?>
                  <div class="shareonedrive-onoffswitch">
                    <input type="checkbox" name="ShareoneDrive_notification_skip_email_currentuser" id="ShareoneDrive_notification_skip_email_currentuser" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['notification_skipemailcurrentuser']) && $_REQUEST['notification_skipemailcurrentuser'] === '1') ? 'checked="checked"' : ''; ?>/>
                    <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_notification_skip_email_currentuser"></label>
                  </div>
                </div>

              </div>
              <!-- End Notifications Tab -->

              <!-- Manipulation Tab -->
              <div id="settings_manipulation"  class="shareonedrive-tab-panel">
                <div class="shareonedrive-tab-panel-header"><?php _e('File Manipulation', 'shareonedrive'); ?></div>

                <div class="option forfilebrowser forgallery forsearch">
                  <div class="shareonedrive-option-title"><?php _e('Allow sharing', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_showsharelink" id="ShareoneDrive_showsharelink" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['showsharelink']) && $_REQUEST['showsharelink'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="sharing-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_showsharelink"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Allow users to generate permanent shared links to the files', 'shareonedrive'); ?></div>


                  <div class="shareonedrive-option-title"><?php _e('Edit descriptions', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_editdescription" id="ShareoneDrive_editdescription" class="shareonedrive-onoffswitch-checkbox"  <?php echo (isset($_REQUEST['editdescription']) && $_REQUEST['editdescription'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="editdescription-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_editdescription"></label>
                    </div>
                  </div>
                  <div class="shareonedrive-option-description"><?php _e('Only supported for OneDrive Personal Accounts', 'shareonedrive'); ?>.</div>

                  <div class="shareonedrive-option-title"><?php _e('Rename files and folders', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_rename" id="ShareoneDrive_rename" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['rename']) && $_REQUEST['rename'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="rename-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_rename"></label>
                    </div>
                  </div>

                  <div class="shareonedrive-option-title"><?php _e('Move files and folders', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_move" id="ShareoneDrive_move" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['move']) && $_REQUEST['move'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="move-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_move"></label>
                    </div>
                  </div>

                  <div class="shareonedrive-option-title"><?php _e('Delete files and folders', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_delete" id="ShareoneDrive_delete" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['delete']) && $_REQUEST['delete'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="delete-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_delete"></label>
                    </div>
                  </div>
                </div>

                <div class="option forfilebrowser forgallery">
                  <div class="shareonedrive-option-title"><?php _e('Create new folders', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_addfolder" id="ShareoneDrive_addfolder" class="shareonedrive-onoffswitch-checkbox" <?php echo (isset($_REQUEST['addfolder']) && $_REQUEST['addfolder'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="addfolder-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_addfolder"></label>
                    </div>
                  </div>
                </div>

                <div class="option forfilebrowser">
                  <div class="shareonedrive-option-title"><?php _e('Edit Microsoft Office files', 'shareonedrive'); ?>
                    <div class="shareonedrive-onoffswitch">
                      <input type="checkbox" name="ShareoneDrive_edit" id="ShareoneDrive_edit" class="shareonedrive-onoffswitch-checkbox"  <?php echo (isset($_REQUEST['edit']) && $_REQUEST['edit'] === '1') ? 'checked="checked"' : ''; ?> data-div-toggle="edit-options"/>
                      <label class="shareonedrive-onoffswitch-label" for="ShareoneDrive_edit"></label>
                    </div>
                  </div>
                </div>

                <br/><br/>

                <div class="shareonedrive-option-description">
                  <?php echo sprintf(__('Select via %s which User Roles are able to perform the actions', 'shareonedrive'), '<a href="#" onclick="jQuery(\'li[data-tab=settings_permissions]\').trigger(\'click\')">' . __('User Permissions', 'shareonedrive') . '</a>'); ?>.
                </div>

              </div>
              <!-- End Manipulation Tab -->
              <!-- Permissions Tab -->
              <div id="settings_permissions"  class="shareonedrive-tab-panel">
                <div class="shareonedrive-tab-panel-header"><?php _e('User Permissions', 'shareonedrive'); ?></div>

                <div class="option forfilebrowser foruploadbox forupload forgallery foraudio forvideo forsearch shareonedrive-permissions-box">
                  <div class="shareonedrive-option-title"><?php _e('Who can see the plugin', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['viewrole'])) ? explode('|', $_REQUEST['viewrole']) : array('administrator', 'author', 'contributor', 'editor', 'subscriber', 'pending', 'guest');
                  wp_roles_checkbox('ShareoneDrive_view_role', $selected);
                  ?>


                  <div class="option forfilebrowser shareonedrive-permissions-box preview-options">
                    <div class="shareonedrive-option-title"><?php _e('Who can preview', 'shareonedrive'); ?></div>
                    <?php
                    $selected = (isset($_REQUEST['previewrole'])) ? explode('|', $_REQUEST['previewrole']) : array('all');
                    wp_roles_checkbox('ShareoneDrive_preview_role', $selected);
                    ?>
                  </div>

                  <div class="shareonedrive-option-title"><?php _e('Who can download', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['downloadrole'])) ? explode('|', $_REQUEST['downloadrole']) : array('all');
                  wp_roles_checkbox('ShareoneDrive_download_role', $selected);
                  ?>

                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forgallery forsearch sharing-options ">
                  <div class="shareonedrive-option-title"><?php _e('Who can share content', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['sharerole'])) ? explode('|', $_REQUEST['sharerole']) : array('all');
                  wp_roles_checkbox('ShareoneDrive_share_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser edit-options">
                  <div class="shareonedrive-option-title"><?php _e('Who can edit', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['editrole'])) ? explode('|', $_REQUEST['editrole']) : array('administrator', 'author', 'editor');
                  wp_roles_checkbox('ShareoneDrive_edit_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forgallery foruploadbox forupload upload-options">
                  <div class="shareonedrive-option-title"><?php _e('Who can upload', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['uploadrole'])) ? explode('|', $_REQUEST['uploadrole']) : array('administrator', 'author', 'contributor', 'editor', 'subscriber');
                  wp_roles_checkbox('ShareoneDrive_upload_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forgallery forsearch editdescription-options ">
                  <div class="shareonedrive-option-title"><?php _e('Who can edit descriptions', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['editdescriptionrole'])) ? explode('|', $_REQUEST['editdescriptionrole']) : array('administrator', 'editor');
                  wp_roles_checkbox('ShareoneDrive_editdescription_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forgallery forsearch rename-options ">
                  <div class="shareonedrive-option-title"><?php _e('Who can rename', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['renamerole'])) ? explode('|', $_REQUEST['renamerole']) : array('administrator', 'author', 'contributor', 'editor');
                  wp_roles_checkbox('ShareoneDrive_rename_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forsearch forgallery move-options">
                  <div class="shareonedrive-option-title"><?php _e('Who can move files and folders', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['moverole'])) ? explode('|', $_REQUEST['moverole']) : array('administrator', 'editor');
                  wp_roles_checkbox('ShareoneDrive_move_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forsearch forgallery delete-options ">
                  <div class="shareonedrive-option-title"><?php _e('Who can delete', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['deleterole'])) ? explode('|', $_REQUEST['deleterole']) : array('administrator', 'author', 'contributor', 'editor');
                  wp_roles_checkbox('ShareoneDrive_delete_role', $selected);
                  ?>
                </div>

                <div class="option shareonedrive-permissions-box forfilebrowser forgallery addfolder-options ">
                  <div class="shareonedrive-option-title"><?php _e('Who can create new folders', 'shareonedrive'); ?></div>
                  <?php
                  $selected = (isset($_REQUEST['addfolderrole'])) ? explode('|', $_REQUEST['addfolderrole']) : array('administrator', 'author', 'contributor', 'editor');
                  wp_roles_checkbox('ShareoneDrive_addfolder_role', $selected);
                  ?>
                </div>
              </div>
              <!-- End Permissions Tab -->

            </div>
            <?php
        }
        ?>

        <div class="footer">

        </div>
      </div>
    </form>
  </body>
</html>