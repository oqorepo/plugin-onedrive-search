<div id="ShareoneDrive" class="ShareoneDriveDashboard">
  <div class="shareonedrive admin-settings">

    <div class="wrap">
      <div class="shareonedrive-header">
        <div class="shareonedrive-logo"><img src="<?php echo SHAREONEDRIVE_ROOTPATH; ?>/css/images/logo64x64.png" height="64" width="64"/></div>
        <div class="shareonedrive-title"><?php _e('Reports', 'shareonedrive'); ?></div>
      </div>

      <div class="shareonedrive-panel">
        <div id="shareonedrive-totals">
          <div class="shareonedrive-box shareonedrive-box25">
            <div class="shareonedrive-box-inner ">
              <div class="shareonedrive-option-title nopadding">
                <div class="shareonedrive-counter-text"><?php echo __('Total Previews', 'shareonedrive'); ?> </div>
                <div class="shareonedrive-counter" data-type="shareonedrive_previewed_entry">
                  <span>
                    <div class="loading"><div class='loader-beat'></div></div>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <div class="shareonedrive-box shareonedrive-box25">
            <div class="shareonedrive-box-inner">
              <div class="shareonedrive-option-title nopadding">
                <div class="shareonedrive-counter-text"><?php echo __('Total Downloads', 'shareonedrive'); ?></div>
                <div class="shareonedrive-counter" data-type="shareonedrive_downloaded_entry">
                  <span>
                    <div class="loading"><div class='loader-beat'></div></div>
                  </span>
                </div></div>
            </div>
          </div>

          <div class="shareonedrive-box shareonedrive-box25">
            <div class="shareonedrive-box-inner">
              <div class="shareonedrive-option-title nopadding">
                <div class="shareonedrive-counter-text"><?php echo __('Items Shared', 'shareonedrive'); ?></div>
                <div class="shareonedrive-counter" data-type="shareonedrive_created_link_to_entry">
                  <span>
                    <div class="loading"><div class='loader-beat'></div></div>
                  </span>
                </div></div>
            </div>
          </div>

          <div class="shareonedrive-box shareonedrive-box25">
            <div class="shareonedrive-box-inner">
              <div class="shareonedrive-option-title nopadding">
                <div class="shareonedrive-counter-text"><?php echo __('Documents Uploaded', 'shareonedrive'); ?></div>
                <div class="shareonedrive-counter" data-type="shareonedrive_uploaded_entry">
                  <span>
                    <div class="loading"><div class='loader-beat'></div></div>
                  </span>
                </div></div>
            </div>
          </div>
        </div>

        <div class="shareonedrive-box">
          <div class="shareonedrive-box-inner">
            <div class="shareonedrive-event-date-selector">
              <label for="chart_datepicker_from"><?php echo __('From', 'shareonedrive'); ?></label>
              <input type="text" id="chart_datepicker_from" name="chart_datepicker_from">
              <label for="chart_datepicker_to"><?php echo __('to', 'shareonedrive'); ?></label>
              <input type="text" id="chart_datepicker_to" name="chart_datepicker_to">
            </div>
            <div class="shareonedrive-option-title"><?php echo __('Events per Day', 'shareonedrive'); ?></div>
            <div class="shareonedrive-events-chart-container" style="height:500px !important; position:relative;">
              <div class="loading"><div class='loader-beat'></div></div>
              <canvas id="shareonedrive-events-chart"></canvas>
            </div>
          </div>
        </div>

        <div class="shareonedrive-box shareonedrive-box50">
          <div class="shareonedrive-box-inner">
            <div class="shareonedrive-option-title"><?php echo __('Top 25 Downloads', 'shareonedrive'); ?></div>
            <table id="top-downloads" class="stripe hover order-column" style="width:100%">
              <thead>
                <tr>
                  <th></th>
                  <th><?php echo __('Document', 'shareonedrive'); ?></th>
                  <th><?php echo __('Total', 'shareonedrive'); ?></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>

        <div class="shareonedrive-box shareonedrive-box50">
          <div class="shareonedrive-box-inner">
            <div class="shareonedrive-option-title"><?php echo __('Top 25 Users with most Downloads', 'shareonedrive'); ?></div>
            <table id="top-users" class="display" style="width:100%">
              <thead>
                <tr>
                  <th></th>
                  <th><?php echo __('User', 'shareonedrive'); ?></th>
                  <th><?php echo __('Downloads', 'shareonedrive'); ?></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>

        <div class="shareonedrive-box">
          <div class="shareonedrive-box-inner">
            <div class="shareonedrive-option-title"><?php echo __('All Events', 'shareonedrive'); ?></div>
            <table id="full-log" class="display" style="width:100%">
              <thead>
                <tr>
                  <th></th>
                  <th class="all"><?php echo __('Description', 'shareonedrive'); ?></th>
                  <th><?php echo __('Date', 'shareonedrive'); ?></th>
                  <th><?php echo __('Event', 'shareonedrive'); ?></th>
                  <th><?php echo __('User', 'shareonedrive'); ?></th>
                  <th><?php echo __('Name', 'shareonedrive'); ?></th>
                  <th><?php echo __('Location', 'shareonedrive'); ?></th>
                  <th><?php echo __('Page', 'shareonedrive'); ?></th>
                  <th><?php echo __('Extra', 'shareonedrive'); ?></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>

        <div class="event-details-template" style="display:none;">
          <div class="event-details-name"></div>

          <div class="shareonedrive-box shareonedrive-box25">
            <div class="shareonedrive-box-inner">
              <div class="event-details-user-template" style="display:none;">
                <div class="event-details-entry-img"></div>
                <a target="_blank" class="event-visit-profile event-button simple-button blue"><i class="fas fa-external-link-square-alt"></i>&nbsp;<?php echo __('Visit Profile'); ?></a>

                <div class="loading"><div class="loader-beat"></div></div>
              </div>

              <div class="event-details-entry-template" style="display:none;">
                <div class="event-details-entry-img"></div>
                <p class="event-details-description"></p>
                <a target="_blank" class="event-download-entry event-button simple-button blue" download><i class="fas fa-download"></i>&nbsp;<?php echo __('Download'); ?></a>

                <div class="loading"><div class="loader-beat"></div></div>
              </div>

              <br/>

              <div class="event-details-totals-template">
                <div class="shareonedrive-option-title tbpadding10 ">
                  <div class="shareonedrive-counter-text"><?php echo __('Previews', 'shareonedrive'); ?> </div>
                  <div class="shareonedrive-counter" data-type="shareonedrive_previewed_entry">
                    <span>
                      <div class="loading"><div class='loader-beat'></div></div>
                    </span>
                  </div>
                </div>

                <div class="shareonedrive-option-title tbpadding10">
                  <div class="shareonedrive-counter-text"><?php echo __('Downloads', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-counter" data-type="shareonedrive_downloaded_entry">
                    <span>
                      <div class="loading"><div class='loader-beat'></div></div>
                    </span>
                  </div>
                </div>

                <div class="shareonedrive-option-title tbpadding10">
                  <div class="shareonedrive-counter-text"><?php echo __('Shared', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-counter" data-type="shareonedrive_created_link_to_entry">
                    <span>
                      <div class="loading"><div class='loader-beat'></div></div>
                    </span>
                  </div>
                </div>

                <div class="shareonedrive-option-title tbpadding10">
                  <div class="shareonedrive-counter-text"><?php echo __('Uploads', 'shareonedrive'); ?></div>
                  <div class="shareonedrive-counter" data-type="shareonedrive_uploaded_entry">
                    <span>
                      <div class="loading"><div class='loader-beat'></div></div>
                    </span>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div class="shareonedrive-box shareonedrive-box75 event-details-table-template">
            <div class="shareonedrive-box-inner">
              <div class="shareonedrive-option-title"><?php echo __('Logged Events', 'shareonedrive'); ?></div>
              <table id="full-detail-log" class="display" style="width:100%">
                <thead>
                  <tr>
                    <th></th>
                    <th class="all"><?php echo __('Description', 'shareonedrive'); ?></th>
                    <th><?php echo __('Date', 'shareonedrive'); ?></th>
                    <th><?php echo __('Event', 'shareonedrive'); ?></th>
                    <th><?php echo __('User', 'shareonedrive'); ?></th>
                    <th><?php echo __('Name', 'shareonedrive'); ?></th>
                    <th><?php echo __('Location', 'shareonedrive'); ?></th>
                    <th><?php echo __('Page', 'shareonedrive'); ?></th>
                    <th><?php echo __('Extra', 'shareonedrive'); ?></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
